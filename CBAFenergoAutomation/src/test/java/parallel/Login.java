package parallel;

import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertTrue;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberoptions.TestRunner;
import cucumber.api.java.Before;
import dataProvider.ConfigFileReader;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import cucumber.api.java.After;

public class Login extends TestRunner {

	public WebDriver driver;

	public static JavascriptExecutor jsExecutor;
	public static String AuthToken;
	JavaScriptControls js;
	ScenarioContext scenariocontext = new ScenarioContext();
	ConfigFileReader configFileReader = new ConfigFileReader();
	public static Scenario message;

	@Before
	public void setUpScenario(Scenario scenario) {
		Login.message = scenario;

	}

	@When("^I \"([^\"]*)\" by right clicking$")
	public void i_by_right_clicking(String arg1) throws Throwable {
		Thread.sleep(7000);
		Actions a = new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath("//*[@fill='#000000']"))).contextClick().build().perform();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[text()='" + arg1 + "']")).click();
		Thread.sleep(7000);
	}

	@When("^I refer \"([^\"]*)\" task$")
	public void i_refer_task(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");

			switch (Type) {
			case "button":
				if (name.equals("NewRequestButton")) {
					Thread.sleep(2000);
					WebElement element = driver.findElement(By.xpath(locator));
					Actions actions = new Actions(driver);
					actions.moveToElement(element).click().build().perform();
					Thread.sleep(3000);

				} else if ((name.equals("Create Entity"))) {
					WebElement element = driver.findElement(By.xpath(locator));
					Actions actions = new Actions(driver);
					actions.moveToElement(element).click().build().perform();
					Thread.sleep(15000);

				} else {

					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);

					break;
				}
			case "Dropdown":
				if (name.equals("Legal Entity Role")) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");

				}

				else if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
				}
				Thread.sleep(2000);
				break;
			case "TextBox":
				if (value != null) {

					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);

				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
				}

			case "MultiSelectDropdown":
				if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
					Thread.sleep(2000);
				}

			}
		}
	}

	@And("^I validate \"([^\"]*)\" field is visible in LHN$")
	public void i_validate_something_field_is_visible_in_lhn(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean visible = driver.findElements(By.xpath(locator)).size() > 0;
			Assert.assertEquals(true, visible);
		}

	}

	@And("^I take a screenshot$")
	public void i_take_a_screenshot() throws Throwable {
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='50%';");
		final byte[] scrShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		message.embed(scrShot, "image/png");
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='100%';");
	}

	@After
	public void takeScreenShotAfterEveryStep() {
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='50%';");
		final byte[] scrShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		message.embed(scrShot, "image/png");
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='100%';");
		// driver.close();
	}

	@Given("^I login to Fenergo Application with \"([^\"]*)\"$")
	public void login_with_user(String username) {


		try {
			// System.out.println("Threadnumber=" + Thread.currentThread().getId());

			System.setProperty("webdriver.chrome.driver", configFileReader.getDriverPath());

			String headless = configFileReader.getHeadlessValue();
			String runInAlreadyOpenBrowser = configFileReader.runInAlreadyOpenBrowser();
			if (headless.equalsIgnoreCase("Yes") || headless.equalsIgnoreCase("True")) {
				ChromeOptions options = new ChromeOptions();
				options.setHeadless(true);
				options.addArguments("--window-size=1920,1080");
				options.addArguments("--disable-gpu");
				options.addArguments("--disable-extensions");
				options.setExperimentalOption("useAutomationExtension", false);
				options.addArguments("--proxy-server='direct://'");
				options.addArguments("--proxy-bypass-list=*");
				options.addArguments("--start-maximized");
				options.addArguments("--headless");

				driver = new ChromeDriver(options);
			} else if(runInAlreadyOpenBrowser.equalsIgnoreCase("Yes")){
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("debuggerAddress", "localhost:9222");
				options.addArguments("--disable-gpu");
				options.addArguments("--disable-extensions");
				driver = new ChromeDriver(options);
			}
			
			else {
				ChromeOptions options = new ChromeOptions();

				options.addArguments("--disable-gpu");
				options.addArguments("--disable-extensions");
				options.addArguments("enable-automation");
				options.addArguments("--start-maximized");

				driver = new ChromeDriver(options);
			}
//			driverFactory  = new DriverFactory();
//			driver = driverFactory.init_driver();
			jsExecutor = (JavascriptExecutor) driver;
			String login = configFileReader.getloginValue();
			if(login.equalsIgnoreCase("FAB")){
				//driver.get("https://clmsit.bankfab.com/FenergoFABSIT.Web");
				//driver.get("https://clmuat.bankfab.com/FenergoFABUAT.Web");
				driver.get("https://clmsit1.bankfab.com/FenergoFABSIT1.Web");
				driver.manage().window().maximize();
				System.out.println("Logged in Succesfully");
				driver.findElement(By.id("details-button")).click();
				//driver.findElement(By.linkText("Proceed to clmsit.bankfab.com (unsafe)")).click();
				//driver.findElement(By.linkText("Proceed to clmuat.bankfab.com (unsafe)")).click();
				driver.findElement(By.linkText("Proceed to clmsit1.bankfab.com (unsafe)")).click();
			}
			if((login.equalsIgnoreCase("LUXOFT") && (runInAlreadyOpenBrowser.equalsIgnoreCase("No")))){


			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			ResultSet rst = sql.getUsernameandPassword("Users", username);
			while (rst.next()) {
				String Username = rst.getString("Username");
				String password = rst.getString("Password");
				driver.get(configFileReader.getApplicationUrl());
				String url = driver.getCurrentUrl();
				String url2 = url.replace("http://", "");
				String url3 = "http://" + Username + ":" + password + "@" + url2;
				// System.out.println(url3);
				driver.get(url3);
				Thread.sleep(7000);
				boolean isAcceptPresent = driver.findElements(By.xpath("//span[text()='Accept']")).size() == 1;
				if (isAcceptPresent == true) {
					driver.findElement(By.xpath("//span[text()='Accept']")).click();
				}
				// driver.manage().window().maximize();
				System.out.println("Logged in Succesfully");
				// Thread.sleep(10000);
			ResultSet rst1 = sql.getControlsDataByName("SearchIcon");
				String locator = rst1.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
				 By waitElement = By.xpath("//span[@class='icon fen-icon-search undefined']");
				Thread.sleep(3000);
				}
			}
		} catch (Exception e) {
			System.out.println("Unable to login with user " + username);
			assertTrue(false, e.getLocalizedMessage());
			driver.close();

		}

	}

	@Then("^I compare the list of documents should be same as \"([^\"]*)\" for ClientType \"([^\"]*)\"$")
	public void i_compare_the_list_of_documents_should_be_same_as_something(String strArg1, String strArg2)
			throws Throwable {
		ArrayList<String> ActualDocument = new ArrayList<String>();
		ArrayList<String> ExpectedDocument = new ArrayList<String>();

		for (int i = 1; i <= 40; i++) {
			if (i == 26) {
				driver.findElement(By.xpath("//span[text()='Next']")).click();
				Thread.sleep(5000);
				for (int j = 1; j <= 15; j++) {
					String locator = "(//a[@class='fen-grid-link fen-grid-link'])[" + j + "]";
					String DocumentName = driver.findElement(By.xpath(locator)).getText();
					ActualDocument.add(DocumentName);
//					System.out.println(DocumentName);
				}

			}

			else if (i <= 25) {

				String locator = "(//a[@class='fen-grid-link fen-grid-link'])[" + i + "]";
				String DocumentName = driver.findElement(By.xpath(locator)).getText();
				ActualDocument.add(DocumentName);
//				System.out.println(DocumentName);

			}

		}
		System.out.println(ActualDocument);
		ExpectedDocument = Expectedvalue(strArg1, strArg2);
		Assert.assertEquals(ExpectedDocument, ActualDocument);
		driver.findElement(By.xpath("//span[text()='Previous']")).click();
		Thread.sleep(5000);

	}

	@And("^I assert that \"([^\"]*)\" subflow is visible in \"([^\"]*)\" screen$")
	public void i_assert_that_subflow_is_visible_in__screen(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean present = driver.findElement(By.xpath(locator)).isDisplayed();
			Assert.assertEquals(true, present);

		}
	}

	@And("^I assert that \"([^\"]*)\" is non-mondatory $")
	public void i_assert_that_something_is_nonmondatory(String strArg1) throws Throwable {

	}

	@When("^I add a \"([^\"]*)\" in KYCDocument$")
	public void i_add_a_document_in_something(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		int j = 1;
		// Thread.sleep(5000);
		ResultSet rst11 = sql.getControlsDataByName("SortDocuments");

		String locatorForSortButton = rst11.getString("Locator");
		waitExplicityForVisibility(By.xpath(locatorForSortButton));
		driver.findElement(By.xpath(locatorForSortButton)).click();

		// waitExplicityForVisibility(By.xpath("//div[@class='fen-data-grid-sort__value']"));
		// System.out.println("Clicking on sort by button");
		// driver.findElement(By.xpath("//div[@class='fen-data-grid-sort__value']")).click();
		// System.out.println("Clicked on sort by button");
		// //Thread.sleep(2000);
		// waitExplicityForVisibility(By.xpath("//div[@data-value='IsWaivable,ASC']"));
		// driver.findElement(By.xpath("//div[@data-value='IsWaivable,ASC']")).click();
		ResultSet rst2 = sql.getControlsDataByName("SortByMandatory");
		String locatorForMandatory = rst2.getString("Locator");
		waitExplicityForVisibility(By.xpath(locatorForMandatory));
		driver.findElement(By.xpath(locatorForMandatory)).click();
		Thread.sleep(5000);
		String getDataQuery = sql.queryForGetDataFromControlsByObjectKeyName("DocumentUpload", "ActionButton");
        ResultSet rst = sql.GetDataSet(getDataQuery);
         while (rst.next()) {
			Thread.sleep(7000);
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			int Count = driver.findElements(By.xpath(locator)).size();
			System.out.println("Count of the number of element is" + Count);
			if (Count == 1) {

				Count = 2;

			}
			for (int i = 4; i <= Count * 2; i = i + 4) {
				Thread.sleep(7000);
				String login = configFileReader.getloginValue();
				if(login.equalsIgnoreCase("FAB")){
					Thread.sleep(2000);
				}
				String Locator = "(//span[@class='fen-dataGrid-cell '])[" + i + "]";
				System.out.println("Value of locator is" + Locator);
				String Mandatory = driver.findElement(By.xpath(Locator)).getText();
				System.out.println("value of mandatory is" + Mandatory);
				
				if(login.equalsIgnoreCase("FAB")){
					Thread.sleep(3000);
				}
				if (Mandatory.equals("Yes")) {

					String Action = "(//button[@class='f-context-menu__button  icon-button '])[" + j + "]";
					System.out.println("(//button[@class='f-context-menu__button  icon-button '])[" + j + "]");
					if(login.equalsIgnoreCase("FAB")){
						Thread.sleep(3000);
					}
					waitExplicityForVisibility(By.xpath(Action));
					driver.findElement(By.xpath(Action)).click();
					ResultSet rst1 = sql.buildQuery("Document", "C1");
					FillInDataWithoutValidation(rst1);
					j = j + 1;

				}
				else {
					break;
				}

			}

		}

	}

	@Then("^I see there is no validation message$")
	public void i_see_there_is_no_validation_message() throws Throwable {

	}

	@When("^I click on \"([^\"]*)\" sign to create new request for \"([^\"]*)\"$")
    public void i_click_on__sign_to_create_new_request(String strArg1, String strArg2) throws Throwable {
        Thread.sleep(5000);
        SqliteConnection sql = new SqliteConnection();
        // String query = "select * from Controls1 where ObjectKey='CompleteRequestFAB'
        // and Screen='CaptureRequestDetails'";
        String query = sql.queryForGetDataFromControlsByObjectkeyAndScreen("CompleteRequestFAB",
                "CaptureRequestDetails");
        System.out.println(query);

 

        ResultSet rst = sql.GetDataSet(query);
        System.out.println(sql);
        System.out.println(query);

 

        System.out.println(rst);

 

        while (rst.next()) {
            Thread.sleep(4000);
            String locator = rst.getString("Locator");
            waitExplicityForVisibility(By.xpath(locator));
            WebElement element = driver.findElement(By.xpath(locator));
            System.out.println(locator);
            jsExecutor.executeScript("arguments[0].click()", element);
            Thread.sleep(3000);
        }

 

    }
	
	@Then("^I see \"([^\"]*)\" task is not generated$")
	@And("^I see \"([^\"]*)\" task is not generated under Risk Assessment stage$")
	public void i_see_something_task_is_not_generated_under_risk_assessment_stage(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Thread.sleep(3000);
			Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 1;
			Assert.assertEquals(true, isPresent);

		}

	}

	@Then("^I can see \"([^\"]*)\" label is renamed as \"([^\"]*)\" on EnterEntitydetails screen$")
	public void i_can_see_label_is_renamed_on_enterentitydetails_screen(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		String query = "select * from Controls1 where ObjectKey= '" + strArg2 + "'";
		String query1 = "select * from Controls1 where ObjectKey= '" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		waitExplicityForVisibility(By.xpath(locator));
		// WebElement element1 = driver.findElement(By.xpath(locator1));
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
		Boolean FabEntityTypePresent = driver.findElements(By.xpath(locator1)).size() < 1;
		Thread.sleep(3000);
		Assert.assertEquals(true, isPresent);
		Assert.assertEquals(true, FabEntityTypePresent);

	}

	@Then("^I can see \"([^\"]*)\" label is renamed as \"([^\"]*)\" on \"([^\"]*)\" screen$")
	public void i_can_see_label_is_renamed_on_enterentitydetails_screen(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		String query = "select * from Controls1 where ObjectKey= '" + strArg2 + "'";
		String query1 = "select * from Controls1 where ObjectKey= '" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		waitExplicityForVisibility(By.xpath(locator));
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
		Boolean FabEntityTypePresent = driver.findElements(By.xpath(locator1)).size() < 1;
		Thread.sleep(3000);
		Assert.assertEquals(true, isPresent);
		Assert.assertEquals(true, FabEntityTypePresent);

	}

	@And("^I assert that \"([^\"]*)\" accepts less than or equal to \"([^\"]*)\" characters$")
	public void i_assert_that_something_accepts_less_than_or_equal_to_255_characters(String strArg1, int strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(strArg2));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("true", Enable);

		}

	}

	@And("^I assert that \"([^\"]*)\" accepts less than or equal to \"([^\"]*)\" numbers$")
	public void i_assert_that_something_accepts_less_than_or_equal_to_255_numbers(String strArg1, int strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(strArg2));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("true", Enable);

		}

	}

	@And("^I assert that \"([^\"]*)\" accept less than or equal to \"([^\"]*)\" characters$")
	public void i_assert_that_something_accept_less_than_or_equal_to_255_characters(String strArg1, int strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(strArg2));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("true", Enable);

		}

	}

	@And("^I assert that \"([^\"]*)\" does not accept alphanumerals$")
	public void i_assert_that_something_does_not_accept_alphanumerals(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(23));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Enabled is" + Enable);
			Assert.assertEquals("false", Enable);
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(13));
		}
	}

	@And("^I assert that \"([^\"]*)\" does not accept more than \"([^\"]*)\" numbers$")
	public void i_assert_that_something_does_not_accept_more_than_numbers(String strArg1, String strArg2)
			throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(15));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Disabled is" + Enable);
			Assert.assertEquals("false", Enable);
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getNumericString(14));
		}
	}

	@And("^I assert that \"([^\"]*)\" does not accept more than 255 characters$")
	public void i_assert_that_something_does_not_accept_more_than_255_characters(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(257));
			Thread.sleep(5000);
			ResultSet rst1 = sql.getControlsData("Controls1", "SaveButton");
			String locator1 = rst1.getString("Locator");
			Boolean enable = driver.findElement(By.xpath(locator1)).isEnabled();
			String Enable = enable.toString();
			System.out.println("Disabled is" + Enable);
			Assert.assertEquals("false", Enable);
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(255));
		}

	}

	public void FillInDataWithLocators(String type, String xpath, String value, String labelName) {

		try {
			switch (type) {

			case "Textbox":
				TextBox.fillInLegacyPageTextBox(xpath, value);
				break;
			case "TextArea":
				TextArea.fillInLegacyPageTextArea(xpath, value);

			}

		} catch (Exception e) {

		}

	}

	@And("^I assert that the CaseStatus is \"([^\"]*)\"$")
    public void i_assert_that_the_casestatus_is_closed(String strArg1) throws Throwable {
        
        driver.navigate().refresh();
        Thread.sleep(5000);
       waitExplicityForVisibility(By.xpath("//span[text()='"+strArg1+"']"));
        Thread.sleep(2000);
        WebElement casestatus = driver.findElement(By.xpath("(//div[@class='fen-form-field-wrapper'])[1]"));
        
        String status = casestatus.getText();
        System.out.println("Case status is " + status);

        if (casestatus.getText().equals("Closed")) {
             System.out.println("Case status is Closed");

        } else {
             System.out.println("Match Not found");
             
             Assert.assertEquals("Closed", casestatus.getText());
             // driver.close();
        }


	}

	@Then("^I refer back the case to \"([^\"]*)\" stage$")
	public void i_refer_back_the_case_to_stag(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "Refer");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String name = rst.getString("Name");
			String Type = rst.getString("Type");
			String value = rst.getString("Value");
			switch (Type) {
			case "button":
				// Thread.sleep(2000);
				// System.out.println("Name is" + name);

				Thread.sleep(3000);
				waitExplicityForVisibility(By.xpath(locator));
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(5000);

				break;

			case "Dropdown":
				if (name.equals("Refer to Stage")) {
					Thread.sleep(2000);

					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					Thread.sleep(5000);
					break;

				}

			case "TextBox": {
				Thread.sleep(3000);
				waitExplicityForVisibility(By.xpath(locator));
				driver.findElement(By.xpath(locator)).sendKeys(value);
				Thread.sleep(2000);

			}

			}
		}

		Thread.sleep(10000);

	}

	public void waitExplicityForVisibility(By by) {
        try{
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
       // System.out.println("----Used Wait for visibility-----=> " + by.toString());
        } catch (Exception e) {
               
        }

	}

	public void FillInDataWithoutValidation(ResultSet dataSet) {

		try {

			while (dataSet.next()) {
			    String name = dataSet.getString("label");
				String value = dataSet.getString("FieldValue");
				String Type = dataSet.getString("type");
				String locator = dataSet.getString("locator");
				switch (Type) {

				case "button":
					// Thread.sleep(2000);
					// System.out.println("Name is" + name);
					if (name.equals("Continue")) {
						waitExplicityForVisibility(By.xpath(locator));
						Thread.sleep(1000);
						driver.findElement(By.xpath(locator)).click();
						waitExplicityForVisibility(By.xpath("//label[text()='Legal Entity Name']"));
						Thread.sleep(4000);
						break;
					} else if (name.equalsIgnoreCase("Create Entity")) {
						waitExplicityForVisibility(By.xpath(locator));
						Thread.sleep(1000);
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(10000);
                          break;
					}

					else
						
					waitExplicityForVisibility(By.xpath(locator));
					Thread.sleep(1000);
					waitIfEnvIsFAB(3);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);
					break;
				case "Checkbox":
					Thread.sleep(2000);
					if(value.equals("true")) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValue(" + value + ")");
					}
					//megha
					else
						if(value.equals("false")) {
							break;		
						}
                    //megha
					else {
					
					 System.out.println("Name is" + name);
					waitExplicityForVisibility(By.xpath(locator));
					Thread.sleep(1000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
					}
					break;
				case "Radiobutton":
					Thread.sleep(3000);
					// System.out.println("Name is" + name);
					waitExplicityForVisibility(By.xpath(locator));
					Thread.sleep(1000);
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);

					break;
				case "Dropdown":
					if (value != null) {
//						Thread.sleep(2000);
						
						if (name.equals("Can the corporation issue bearer shares?")) {
							waitForDropDown(name);
							System.out.println("Dropdown field name :'"+name+"' and value is '"+value);
							jsExecutor.executeScript(
									"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
							Thread.sleep(5000);
						}

						else if (value.equals("NULL")) {
							break;

						} else if (name.equals("Legal Entity Type")) {
						System.out.println("Dropdown field name :'"+name+"' and value is '"+value);
							waitForDropDown(name);
							Thread.sleep(2000);
							jsExecutor.executeScript(
									"PageObjects.find({dataKey:'LegalEntity_SubtypeId'}).setValueByLookupText('" + value
											+ "')");
							Thread.sleep(2000);
						}

						else {
//							Thread.sleep(1000);
							waitForDropDown(name);
							System.out.println("Dropdown field name :'"+name+"' and value is '"+value);
//							System.out.println(jsExecutor.executeScript("PageObjects.find({caption:'" + name
//									+ "'}).setValueByLookupText('" + value + "')"));
//							Thread.sleep(2000);
							jsExecutor.executeScript(
									"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
							 Thread.sleep(1000);
						}

					}
					break;
                 case "MultiSelectDropdown":
                	 Thread.sleep(1000);
					if (value != null) {
						System.out.println(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						Thread.sleep(2000);
					}
					break;
				case "TextBox":
					if ((value != null) && !(name.equals("Upload"))) {
						// jsExecutor.executeScript("return PageObjects.find({caption:'" + name +
						// "'}).clear()");
						waitExplicityForVisibility(By.xpath(locator));
						Thread.sleep(1000);
						driver.findElement(By.xpath(locator)).clear();
						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(1000);
					} else if (name.equals("IdentificationNumber")) {
						waitExplicityForVisibility(By.xpath(locator));
						driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(7));
						Thread.sleep(3000);
						putExpirationDateIfDocTypeIsConstitutive();
                    } else if (name.equals("Upload")) {
                    	System.out.println("Inside upload");       	
						Thread.sleep(3000);
						String documentPath = System.getProperty("user.dir") + "\\" + value;
						driver.findElement(By.xpath(locator)).sendKeys(documentPath);
//						driver.findElement(By.xpath(locator)).sendKeys(value);

						Thread.sleep(1000);
						// Megha- To upload 40 documents
					}else if (name.equals("DocumentName")) {
						boolean isPresent = driver.findElements(By.xpath("//span[@class=\"read-only\"]")).size() > 0;
						if (isPresent == false) {
							waitExplicityForVisibility(By.xpath(locator));
							Thread.sleep(1000);
							driver.findElement(By.xpath(locator)).sendKeys("others");
							Thread.sleep(3000);
						}
					
					}
					// Megha- To upload 40 documents

					break;
				case "DatePicker":
					if (value != null) {
                        Thread.sleep(3000);
						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
						break;
					}

				}

			}

		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}

	}

	@And("^I assert that \"([^\"]*)\" is the first panel$")
	public void i_assert_that_something_is_the_first_panel(String strArg1) throws Throwable {
		Thread.sleep(5000);
		String Section = driver.findElement(By.xpath("(//h3)[1]")).getText();
		System.out.println("value is" + Section);
		Assert.assertEquals(Section, strArg1);
	}

	@And("^I assert that \"([^\"]*)\" is the second panel$")
	public void i_assert_that_something_is_the_second_panel(String strArg1) throws Throwable {
		Thread.sleep(5000);
		String Section = driver.findElement(By.xpath("(//h3)31]")).getText();
		System.out.println("value is" + Section);
		Assert.assertEquals(Section, strArg1);
	}
	@And("^I assert only the following document requirements are listed for LiteKYC WF$")
    public void i_assert_only_the_following_document_requirements_are_listed_for_litekyc_wf(DataTable table) throws Throwable {
     String docReqName, defaultDocType, defaultDocCategory, mandatory;
        List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
        for (Map<String, String> data : inputList) {
            Thread.sleep(3000);
            docReqName = data.get("KYC Document Requirement");
            //defaultDocType = data.get("Default Document Type");
            //defaultDocCategory = data.get("Default Document Category");
            //mandatory = data.get("Mandatory");
            if(docReqName.length()>35){
                docReqName = docReqName.substring(0, 31);
            }
            String docNameXpath = "//*[contains(text(),'" + docReqName + "')]";
            boolean isPresent = driver.findElements(By.xpath(docNameXpath)).size() > 0;
            Assert.assertTrue(isPresent);
            System.out.println("Document found : "+docReqName);
           
    }
 }
    @And("^I assert \"([^\"]*)\" does not accept future date$")
	public void i_assert_something_does_not_accept_future_date(String strArg1) throws Throwable {
		Thread.sleep(5000);
		jsExecutor.executeScript("PageObjects.find({caption:'" + strArg1 + "'}).setValue('2021-12-31')");
//		String message = driver.findElement(By.xpath("//span[@class='f-inline-message__text']")).getText();
		Thread.sleep(3000);
//		Assert.assertEquals("Please enter a date before 29-12-2020.", message);
		
	}

	@And("^I assert \"([^\"]*)\" is populated as \"([^\"]*)\"$")
	public void i_assert_something_is_populated_as_something(String strArg1, String strArg2) throws Throwable {
		Thread.sleep(4000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String locator = rst.getString("Locator");
		WebElement element = driver.findElement(By.xpath(locator));
		String riskValue = element.getText();
		Assert.assertEquals(strArg2, riskValue);
	}

	@Then("^I Validate the CaseName doesnot contain \"([^\"]*)\" in it$")
	public void i_validate_the_casename_doesnot_contain_something_in_it(String strArg1) throws Throwable {
		Thread.sleep(4000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "CaseName");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			String CaseName = driver.findElement(By.xpath(locator)).getText();
			System.out.println("Case Name is " + CaseName);
			Assert.assertFalse(CaseName.contains(strArg1));
		}

	}

	@Then("^I complete \"([^\"]*)\" screen with key \"([^\"]*)\"$")
	@And("^I fill the data for \"([^\"]*)\" with key \"([^\"]*)\"$")
	public void user_fill_in_datakey(String tablename, String Datakey) {
		try {
			Thread.sleep(5000);
			SqliteConnection sql = SqliteConnection.getInstance();
			ResultSet rst = sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);

		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}
	}

	@When("^I add an associatedparty from \"([^\"]*)\" with key \"([^\"]*)\"$")
	public void add_associated_party(String tablename, String Datakey) {
		try {
			SqliteConnection sql = SqliteConnection.getInstance();
			ResultSet rst = sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);

		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}
	}

	@Then("^I validate the specific LOVs for \"([^\"]*)\"$")
	public void i_validate_the_specific_lovs_for_something(String strArg1) throws Throwable {

	}

	@Test
	@When("^I create new request with LegalEntityrole as \"([^\"]*)\"$")
	public void i_create_new_request_with_customerid(String strArg1) throws Throwable {

		try {

			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();

			ResultSet rst = sql.getControlsData("Controls1", "CompleteRequestFAB");

			while (rst.next()) {
				String name = rst.getString("Name");
				String value = rst.getString("Value");
				String Type = rst.getString("Type");
				String locator = rst.getString("Locator");

				switch (Type) {
				case "button":
					if (name.equals("NewRequestButton")) {
						Thread.sleep(2000);
						WebElement element = driver.findElement(By.xpath(locator));
						Actions actions = new Actions(driver);
						actions.moveToElement(element).click().build().perform();
						Thread.sleep(3000);

					} else if ((name.equals("Create Entity"))) {
						WebElement element = driver.findElement(By.xpath(locator));
						Actions actions = new Actions(driver);
						actions.moveToElement(element).click().build().perform();
						Thread.sleep(15000);

					} else {

						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);

						break;
					}
				case "Dropdown":
					if (name.equals("Legal Entity Role")) {

						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");

					}

					else if (value != null) {
						System.out.println(jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')"));
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					}
					Thread.sleep(2000);
					break;
				case "TextBox":
					if (value != null) {

						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(2000);

					}
					break;
				case "DatePicker":
					if (value != null) {

						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
					}

				case "MultiSelectDropdown":
					if (value != null) {
						Thread.sleep(2000);
						System.out.println(jsExecutor.executeScript("PageObjects.find({caption:'" + name
								+ "'}).setValueByLookupText(['\" + strArg1 + \"'])"));
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
						Thread.sleep(2000);
					}

				}
			}
		}

		catch (Exception e) {
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(src, new File("Fenergo\\src\\test\\resources\\Screenshot.png"));
			driver.close();
			assertTrue(false, e.getLocalizedMessage());
			Thread.sleep(2000);
		}
	}

	@When("^I navigate to \"([^\"]*)\" screen$")
	public void i_navigate_to_something_screen(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("RiskAssessment")) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
					// driver.close();

				} else {
					Thread.sleep(2000);
					System.out.println("name is " + name);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}
				break;

			}

		}
	}

	@Then("^Verify if \"([^\"]*)\" button of \"([^\"]*)\" is disabled for \"([^\"]*)\"$")
	public void verify_if_something_button_of_something_is_disabled(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'and Screen='"
				+ strArg3 + "'";
		ResultSet rst = sql.GetDataSet(query);

		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean disabled = driver.findElement(By.xpath(locator)).isEnabled();
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor ex = (JavascriptExecutor) driver;
			ex.executeScript("arguments[0].click()", element);
			// Assert.assertEquals(disabled, false);

		}

	}

	@Then("^I can see \"([^\"]*)\" subflow is hidden$")
	public void i_can_see__subflow_is_hidden(String strArg1) throws Throwable {

		Thread.sleep(3000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		// ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String query = "select * from Controls1 where ObjectKey='LE360' and Name='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		String locator = rst.getString("Locator");
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 1;
		Assert.assertEquals(true, isPresent);

	}

	@When("^I complete \"([^\"]*)\" task for MaintenanceRequest$")
	public void i_complete_document_requirement_task_for_maintenancerequest(String strArg1) throws Throwable {

		try {
			Boolean isPresent = driver.findElements(By.xpath("//div[text()='Cases']")).size() > 0;
			if (isPresent == true) {
				WebElement element = driver.findElement(By.xpath("//div[text()='Cases']"));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().build().perform();
				WebElement element2 = driver.findElement(By.xpath("//a[@title='Capture Request Details']"));
				Thread.sleep(3000);
				jsExecutor.executeScript("arguments[0].click()", element2);
				Thread.sleep(2000);
			}
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			ResultSet rst = sql.getControlsData("Controls1", strArg1);
			while (rst.next()) {
				String name = rst.getString("Name");
				String value = rst.getString("Value");
				String Type = rst.getString("Type");
				String locator = rst.getString("Locator");
				switch (Type) {
				case "button":
					if (name.equals("RiskAssessment")) {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
						driver.close();

					} else {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(2000);
					}
					break;
				case "Dropdown":
					if (value != null) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
						Thread.sleep(2000);
					}
					break;

				case "MultiSelectDropdown":
					if (value != null) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
						Thread.sleep(2000);
					}
					break;
				case "TextBox":
					if (value != null) {

						driver.findElement(By.xpath(locator)).sendKeys(value);
						Thread.sleep(2000);
					}
					break;
				case "DatePicker":
					if (value != null) {

						jsExecutor
								.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
						Thread.sleep(2000);
						break;
					}

				default:

				}

			}
		} catch (Exception e) {

		}
		driver.close();

	}

	@And("^I check that below data is mandatory$")
	public void i_check_that_below_data_is_mandatory(DataTable table) throws Throwable {
		String labelname;
		String newvalue;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");
			Thread.sleep(5000);
			if ((labelname.contains("Grid"))&&(!labelname.contains("ProductsGrid"))&&(!labelname.contains("TradingEntitiesGrid"))&&(!labelname.contains("AddressesGrid"))) {
				System.out.println("Inside Grid");
				System.out.println("PageObjects.find({gridName:'" + labelname + "'}).mandatory");
				String a = jsExecutor.executeScript("return PageObjects.find({gridName:'" + labelname + "'}).mandatory").toString();
				if (a== null) {
					Assert.assertEquals(null, a);

				}
				
				else if(labelname.contains("ProductsGrid")||(labelname.contains("TradingEntitiesGrid"))||(labelname.contains("AddressesGrid"))) {
					continue;
					
				}
				else {
				Assert.assertEquals("true", a);}
			}
			
			else if(!labelname.contains("Grid")) {
			
			System.out.println("Value of data field is" + labelname);
			System.out.println(
					"PageObjects.find({caption:'" + data + "'}).mandatory");

			String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + labelname + "'}).mandatory")
					.toString();
			if(a==null) {
				System.out.println("Value is not defined");
			}
			{
				System.out.println("Value of mandatory is" + a);
				assertTrue(true, "value is not mandatory");
				Thread.sleep(2000);
			}
		}}

	}

	@When("^I Initiate \"([^\"]*)\" by rightclicking$")
	public void i_initiate_something_by_rightclicking(String strArg1) throws Throwable {
        waitExplicityForVisibility(By.xpath("//*[@fill='#39807D']"));
      Thread.sleep(1000);
         
         Actions a = new Actions(driver);
         
        a.moveToElement(driver.findElement(By.xpath("//*[@fill='#39807D']"))).contextClick().build().perform();
         Thread.sleep(2000);
         String login = configFileReader.getloginValue();
         if(login.equalsIgnoreCase("FAB")){
              Thread.sleep(3000);
         }
        waitExplicityForVisibility(By.xpath("//span[text()='Add " + strArg1 + " Screening']"));
         driver.findElement(By.xpath("//span[text()='Add " + strArg1 + " Screening']")).click();
         Thread.sleep(5000);
         

		
	}

	@When("^I Initiate \"([^\"]*)\" by rightclicking for \"([^\"]*)\" associated party$")
	public void i_initiate_by_rightclicking(String strArg1,int strArg2) throws Throwable {
		Actions a = new Actions(driver);
		for (int i=1;i<=strArg2;i++) {
			waitExplicityForVisibility(By.xpath("(//*[@fill='#000000'])["+i+"]"));
			Thread.sleep(1000);
			a.moveToElement(driver.findElement(By.xpath("(//*[@fill='#000000'])["+i+"]"))).contextClick().build().perform();
			Thread.sleep(2000);
			waitExplicityForVisibility(By.xpath("//span[text()='Add " + strArg1 + " Screening']"));
			Thread.sleep(1000);
			driver.findElement(By.xpath("//span[text()='Add " + strArg1 + " Screening']")).click();
		}
	}
	
	@When("^I add AssociatedParty by right clicking$")
	public void i_add_associatedparty_by_right_clicking() throws Throwable {
		Thread.sleep(2000);
		String login = configFileReader.getloginValue();
		if(login.equalsIgnoreCase("FAB")){
			Thread.sleep(15000);
		}
		Actions a = new Actions(driver);
		waitExplicityForVisibility(By.xpath("//*[@fill='#39807D']"));
		Thread.sleep(1000);
		a.moveToElement(driver.findElement(By.xpath("//*[@fill='#39807D']"))).contextClick().build().perform();
		waitExplicityForVisibility(By.xpath("//div[text()='Add Associated Party']"));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[text()='Add Associated Party']")).click();
		Thread.sleep(6000);
	}
	 @And("^I navigate to \"([^\"]*)\" screen of added AssociatedParty$")
     public void i_navigate_to_le_screen_of_added_associatedparty(String strArg1) throws Throwable {
         Thread.sleep(3000);
              Actions a = new Actions(driver);
              waitIfEnvIsFAB(5);
              waitExplicityForVisibility(By.xpath("//*[@fill='#000000']"));
              Thread.sleep(1000);
              a.moveToElement(driver.findElement(By.xpath("//*[@fill='#000000']"))).contextClick().build().perform();
//              Thread.sleep(2000);
                waitIfEnvIsFAB(4); //megha                  
                waitExplicityForVisibility(By.xpath("//div[text()='Navigate to Legal Entity']"));
            Thread.sleep(1000);
              driver.findElement(By.xpath("//div[text()='Navigate to Legal Entity']")).click();
              Thread.sleep(6000);

	    }
	 
//	 @And("^I navigate to \"([^\"]*)\" screen of added \"([^\"]*)\" AssociatedParty$")
//	    public void i_navigate_to_le_screen(String strArg1,String strArg2) throws Throwable {
//		    Thread.sleep(3000);
//			Actions a = new Actions(driver);
//			if (strArg2.equals("NonIndividual")) {
//			a.moveToElement(driver.findElement(By.xpath("(//*[@fill='#000000'])["+1+"]"))).contextClick().build().perform();
//			Thread.sleep(2000);
//			driver.findElement(By.xpath("//div[text()='Navigate to Legal Entity']")).click();
//			Thread.sleep(3000);
//			}
//			else {
//				a.moveToElement(driver.findElement(By.xpath("(//*[@fill='#000000'])["+2+"]"))).contextClick().build().perform();
//				Thread.sleep(2000);
//				driver.findElement(By.xpath("//div[text()='Navigate to Legal Entity']")).click();
//				Thread.sleep(3000);
//			}
//	    }

	 @When("^I navigate to \"([^\"]*)\" screen of the added \"([^\"]*)\" AssociatedParty$")
	 public void i_navigate_AssociatedParty(String arg1,String arg2) throws Throwable {
	        Thread.sleep(3000);
	        //waitIfEnvIsFAB(7);//megha
			Actions a = new Actions(driver);
			if (arg2.equals("NonIndividual")) {
			waitIfEnvIsFAB(4);//megha
			waitExplicityForDropdownVisibility(By.xpath("(//*[@fill='#000000'])["+1+"]"));
			Thread.sleep(1000);
			a.moveToElement(driver.findElement(By.xpath("(//*[@fill='#000000'])["+1+"]"))).contextClick().build().perform();
			waitExplicityForVisibility(By.xpath("//div[text()='Navigate to Legal Entity']"));
            Thread.sleep(1000);
            //waitIfEnvIsFAB(2);//megha
			driver.findElement(By.xpath("//div[text()='Navigate to Legal Entity']")).click();
			Thread.sleep(3000);
			}
			else {
				waitExplicityForDropdownVisibility(By.xpath("(//*[@fill='#000000'])["+2+"]"));
				Thread.sleep(1000);
				a.moveToElement(driver.findElement(By.xpath("(//*[@fill='#000000'])["+2+"]"))).contextClick().build().perform();
				Thread.sleep(2000);
				waitExplicityForVisibility(By.xpath("//div[text()='Navigate to Legal Entity']"));
                Thread.sleep(1000);
                waitIfEnvIsFAB(5);
				driver.findElement(By.xpath("//div[text()='Navigate to Legal Entity']")).click();
				Thread.sleep(3000);
	 }
	 }
	@And("^I complete \"([^\"]*)\" from assessment grid$")
	public void i_complete_something_from_assessment_grid(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "Assessment");
		while (rst.next()) {
			Thread.sleep(2000);
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(3000);
			// SqliteConnection sql1 = SqliteConnection.getInstance();

		}
		ResultSet rst1 = sql.buildQuery("Screening", "A1");
		FillInDataWithoutValidation(rst1);

	}

	  
	   
	@When("^I enter data in \"([^\"]*)\" as \"([^\"]*)\"$")
	public void i_enter_data_in_something_as_something(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			if (strArg2.equals("ALPHABETICAL") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("A");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("ALPHABETICAL") && (name.equals("T24 CIF ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("A");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("NumericalMoreThenSix") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("12312312");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("NumericalLessThenSix") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("1231231");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else if (strArg2.equals("AllZeroes") && (name.equals("GLCMS ID"))) {
				driver.findElement(By.xpath(locator)).sendKeys("000000");
				Actions act = new Actions(driver);
				act.moveByOffset(100, 100).click().build().perform();

			} else {
				driver.findElement(By.xpath(locator)).sendKeys(value);
			}

		}
	}

	@Then("^I validate the error messgage for \"([^\"]*)\" as \"([^\"]*)\"$")
	public void i_validate_the_error_messgage_for_something_as_something(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ErrorMessage'and Name='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(5000);
		while (rst.next()) {
			String name = rst.getString("Name");
			String locator = rst.getString("Locator");
			WebElement casestatus = driver.findElement(By.xpath(locator));
			
			String value = casestatus.getText();
		System.out.println("error message is"+value);
//			Actions act = new Actions(driver);
//			act.moveByOffset(450, 450).click().build().perform();
//			System.out.println("label name is " + casestatus);
//			// Assert.assertEquals(casestatus, strArg2);
//			Assert.assertTrue(strArg2, value.contains(strArg2));
		Thread.sleep(3000);
			
		}

	}

	@Then("^I refresh the page$")
	public void i_refresh_the_page() throws Throwable {
		driver.navigate().refresh();
		Thread.sleep(5000);
	}
	
	
	@And("^I check that below data is non mandatory$")
	public void i_check_that_below_data_is_non_mandatory(DataTable table) throws Throwable {
		String labelname;

		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
		labelname = data.get("FieldLabel");
			Thread.sleep(3000);
try {
				if ((labelname.contains("Grid"))&&(!labelname.contains("ProductsGrid"))&&(!labelname.contains("TradingEntitiesGrid"))&&(!labelname.contains("TaxIdentifierGrid"))) {
					System.out.println("Inside Grid");
					String a = jsExecutor.executeScript("return PageObjects.find({gridName:'" + labelname + "'}).mandatory")
							.toString();
					if (a== null) {
						Assert.assertEquals(null, a);

					}
					
					else if(labelname.contains("ProductsGrid")||(labelname.contains("TradingEntitiesGrid"))||(labelname.contains("TaxIdentifierGrid"))) {
						continue;
						
					}
					else {
					Assert.assertEquals("false", a);}
				} else if (!labelname.contains("Grid")){
					System.out.println("Value is " + labelname);
					Thread.sleep(2000);
					String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + labelname + "'}).mandatory")
							.toString();
					System.out.println("Value of a is " + a);
					Assert.assertEquals("false", a);
				}
			} catch (Exception e) {
				assertTrue(false, e.getLocalizedMessage());
			}
		}

	}
	@And("^I assert that \"([^\"]*)\" subflow is read only$")
	public void i_assert_that_something_subflow_is_read_only(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean ispresent = driver.findElements(By.xpath(locator)).size() > 0;
			Assert.assertEquals(false, ispresent);
		}
	}

	@When("^I expand the \"([^\"]*)\" button$")
	public void i_expand_the_button(String strArg1) throws Throwable {
		Thread.sleep(3000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			WebElement element = driver.findElement(By.xpath(locator));
			jsExecutor.executeScript("arguments[0].click()", element);
			Thread.sleep(3000);

		}

	}

	@When("^I expand the \"([^\"]*)\" button for \"([^\"]*)\"$")
	public void i_expand_the_buttonfor(String strArg1, String strArg2) throws Throwable {
		Thread.sleep(3000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "' and Screen='" + strArg2 + "'";
		System.out.println("select * from Controls1 where ObjectKey='" + strArg1 + "' and Screen='" + strArg2 + "'");
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			WebElement element = driver.findElement(By.xpath(locator));
			jsExecutor.executeScript("arguments[0].click()", element);
			Thread.sleep(3000);

		}

	}

	@When("^I complete \"([^\"]*)\" with TaxType as \"([^\"]*)\" and Country as \"([^\"]*)\"$")
	public void i_complete_something_with_taxtype_as_something_and_country_as_something(String strArg1, String strArg2,
			String strArg3) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		Thread.sleep(3000);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
					}
                   else {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}
				break;
			case "Dropdown":
				if (name.equals("Country")) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					Thread.sleep(5000);
				}

				else if ((name.equals("Tax Type"))) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");
					} else {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");

				}
				break;

			}

		}
	}

	@When("^I complete \"([^\"]*)\" with TaxType as \"([^\"]*)\" and Country as \"([^\"]*)\" for \"([^\"]*)\"$")
	public void i_complete_something_with_taxtype_as__and_country_as(String strArg1, String strArg2, String strArg3,
			String strArg4) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		System.out.println("select * from Controls1 where ObjectKey='" + strArg1 + "' and Screen='" + strArg4 + "'");
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "' and Screen='" + strArg4 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(3000);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}

				else {
					waitExplicityForVisibility(By.xpath(locator));
					WebElement element = driver.findElement(By.xpath(locator));
					jsExecutor.executeScript("arguments[0].click()", element);

					Thread.sleep(7000);
				}
				break;
			case "Dropdown":
				Thread.sleep(2000);
				if (name.equals("Country")) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg3 + "')");
					Thread.sleep(1000);
				}

				else if ((name.equals("Tax Type"))) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");
					Thread.sleep(5000);

				} else {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");

				}
				Thread.sleep(2000);
				break;

			case "TextBox":

				Thread.sleep(3000);
				waitExplicityForVisibility(By.xpath(locator));
				driver.findElement(By.xpath(locator)).sendKeys(value);
				Thread.sleep(3000);

				break;

			}

		}
	}

	@Then("^I assert that the warning message appears as \"([^\"]*)\"$")
	public void i_assert_that_the_warning_message_appears_as(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "TaxIdentifierValidationMessage");
		String locator = rst.getString("Locator");

		WebElement casestatus = driver.findElement(By.xpath(locator));
		casestatus.getText();
		Assert.assertEquals(strArg1, casestatus.getText());

	}

	@When("^I enter \"([^\"]*)\" as \"([^\"]*)\"$")
	public void i_enter_taxidentifiervalue(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(3000);

		String locator = rst.getString("Locator");
		String Type = rst.getString("Type");
		String value = rst.getString("Value");
		switch (Type) {
		case "TextBox":
			if (value != null) {
				driver.findElement(By.xpath(locator)).sendKeys(value);
				Thread.sleep(2000);

			}

		}

		Actions act = new Actions(driver);
		act.moveByOffset(30, 30).click().build().perform();

	}

	@And("^I edit a Product with Product Status as \"([^\"]*)\" for \"([^\"]*)\"$")
	public void i_edit_a_product_with_product_status_as_something_for_something(String strArg1, String strArg2)
			throws Throwable {
		Thread.sleep(3000);

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ProductEdit'and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					waitExplicityForVisibility(By.xpath("//span[@class='read-only']"));

				} else if (name.equals("ExpandButton")) {
					Boolean Ispresent = driver.findElements(By.xpath(locator)).size() < 1;
					System.out.println("Expand button is" + Ispresent);
					if (Ispresent.equals(true)) {
						System.out.println("Expand button is" + Ispresent);

					} else {
						waitExplicityForVisibility(By.xpath(locator));
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
					}

				} else {
					// Thread.sleep(3000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					if (name.equalsIgnoreCase("Save")) {
						waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
					}
					Thread.sleep(2000);

				}

				break;
			case "Dropdown":
				if (name.equals("Status")) {
					Thread.sleep(3000);
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					Thread.sleep(3000);
				}
				break;

			}

		}
		Thread.sleep(5000);

	}

	@And("^I edit a Product with Product Status as \"([^\"]*)\"$")
	public void i_edit_a_product_with_product_status_as_something(String strArg1) throws Throwable {
		Thread.sleep(3000);

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "ProductEdit");
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("EditButton")) {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					waitExplicityForVisibility(By.xpath("//*[text()='Status']"));
					Thread.sleep(3000);

				} else if (name.equals("ExpandButton")) {
					waitExplicityForVisibility(By.xpath(locator));
					Boolean Ispresent = driver.findElements(By.xpath(locator)).size() < 1;
					System.out.println("Expand button is" + Ispresent);
					if (Ispresent.equals(true)) {
						System.out.println("Expand button is" + Ispresent);

					} else {
						waitExplicityForVisibility(By.xpath(locator));
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
					}

				} else {
					Thread.sleep(3000);
					waitExplicityForVisibility(By.xpath(locator));
					if (name.equalsIgnoreCase("Save")) {
						driver.findElement(By.xpath(locator)).click();
						waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
					} else {
						driver.findElement(By.xpath(locator)).click();
					}
					Thread.sleep(3000);

				}

				break;
			case "Dropdown":
				if (name.equals("Status")) {
					Thread.sleep(2000);
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
					Thread.sleep(5000);
				}
				break;

			}

		}

	}

	@When("^I delete the Product from \"([^\"]*)\"$")
	public void i_delete_the_product(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		// ResultSet rst = sql.getControlsDataAlongWithScreenName("Controls1",
		// "ProductDelete");
		ResultSet rst = sql.getControlsDataAlongWithScreenName("Controls1", "ProductDelete", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("ExpandButton")) {
					Boolean Ispresent = driver.findElements(By.xpath(locator)).size() < 1;
					System.out.println("Expand button is " + Ispresent);
					if (Ispresent.equals(true)) {
						System.out.println("Expand button is " + Ispresent);

					} else {
						Thread.sleep(2000);
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(3000);
					}

				}

				break;

			}

		}

	}

	@When("^I view the existing Product from \"([^\"]*)\"$")
	public void i_view_the_existing_product_from_something(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ProductView'and Screen='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				Thread.sleep(2000);
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);

				break;

			}

		}

	}

	@And("^I assert ProductSection is empty in \"([^\"]*)\"$")
	public void i_assert__is_empty(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='ProductExpand'and Screen='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {

			String locator = rst.getString("Locator");

			Boolean ProductGridNotAvailable = driver.findElements(By.xpath(locator)).size() < 1;
			Assert.assertEquals(true, ProductGridNotAvailable);

		}

	}

	@Then("^I \"([^\"]*)\" one of the Document and check that the document is unlinked$")
	public void i_unlink_one_of_the_document(String strArg1) throws Throwable {
		driver.findElement(By.xpath("(//span[@class='icon fen-icon-ellipsis undefined'])[2]")).click();
		driver.findElement(By.xpath("(//span[@class='fen-caption'])[1]")).click();
		Thread.sleep(3000);
		String message = driver.findElement(By.xpath("//div[@class='fen-no-data-component']")).getText();
		Assert.assertEquals(message, "There are no documents linked to this requirement");

	}

	@And("^I check that the Document is \"([^\"]*)\"$")
	public void i_check_that_the_document_is(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(2000);
		}
		String value = driver.findElement(By.xpath("(//span[@class='fen-dataGrid-cell '])[2]")).getText();
		Assert.assertEquals("Received", value);

	}

	@And("^I close the browser$")
	public void i_close_the_browser() throws Throwable {
		// driver.close();
	}

	@And("^I add a \"([^\"]*)\" from \"([^\"]*)\"$")
	public void i_add_a_subflow(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();

		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("RiskAssessment")) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
					driver.close();

				} else {
					System.out.println("Name is " + name);
					waitExplicityForVisibility(By.xpath(locator));
					if (name.equalsIgnoreCase("SaveButton")) {
						driver.findElement(By.xpath(locator)).click();
						waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
					} else {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(5000);
					}

				}
				break;
			case "Dropdown":
				if (value != null) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					Thread.sleep(2000);
				}
				break;

			case "MultiSelectDropdown":
				if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					Thread.sleep(2000);
				}
				break;
			case "TextBox":
				if (value != null) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
					break;
				}
			case "Link":

				System.out.println("Name is : " + name);

				if (name.equals("AddProduct")) {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(1000);
					waitExplicityForVisibility(By.xpath("//*[text()='Product Category']"));

				} else {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}
				break;

			default:

			}

		}

	}

	@And("^I add a Product from \"([^\"]*)\"$")
	public void i_add_a_product(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();

		String query = "select * from Controls1 where ObjectKey='Product'and Screen='" + strArg1 + "'";

		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				if (name.equals("RiskAssessment")) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(3000);
					driver.close();

				} else {
					System.out.println("Name is " + name);
					waitExplicityForVisibility(By.xpath(locator));
					if (name.equalsIgnoreCase("SaveButton")) {
						driver.findElement(By.xpath(locator)).click();
						waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
					} else {
						driver.findElement(By.xpath(locator)).click();
						Thread.sleep(5000);
					}

				}
				break;
			case "Dropdown":
				if (value != null) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					Thread.sleep(2000);
				}
				break;

			case "MultiSelectDropdown":
				if (value != null) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					Thread.sleep(2000);
				}
				break;
			case "TextBox":
				if (value != null) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
					break;
				}
			case "Link":

				System.out.println("Name is : " + name);

				if (name.equals("AddProduct")) {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(1000);
					waitExplicityForVisibility(By.xpath("//*[text()='Product Category']"));

				} else {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}
				break;

			default:

			}

		}
	}

	@When("^I expand \"([^\"]*)\" and zoom out so that grid gets fully visible$")
	public void i_expand_something_and_zoom_out_to_75(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= 'ProductExpand'and Screen='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String screen = rst.getString("Screen");
			System.out.println("Screen name is" + screen);
			if (screen.equals("ReviewRequest")) {
				Thread.sleep(7000);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
			} else {
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(3000);
			}
			((JavascriptExecutor) driver).executeScript("document.body.style.zoom='75%';");
		}
	}

	@And("^I expand \"([^\"]*)\" and zoom out so that grid gets fully visible for \"([^\"]*)\"$")
	public void expand(String strArg1, String strArg2) throws Throwable {
		Thread.sleep(5000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= 'ProductGridExpand'and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String screen = rst.getString("Screen");
			System.out.println("Screen name is" + screen);
			if (screen.equals("ReviewRequest")) {
				Thread.sleep(3000);
				WebElement element = driver.findElement(By.xpath(locator));
				waitExplicityForVisibility(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
			} else {
				Boolean Ispresent = driver.findElements(By.xpath(locator)).size() > 0;
				if (Ispresent == true) {
					Thread.sleep(2000);
					WebElement element = driver.findElement(By.xpath(locator));
					waitExplicityForVisibility(By.xpath(locator));
					jsExecutor.executeScript("arguments[0].click()", element);
					Thread.sleep(3000);
				} else {
					continue;
				}
			}

		}

		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='75%';");

	}

	@And("^I assert \"([^\"]*)\" column is not present as a field below the grid for \"([^\"]*)\"$")
	public void i_assert_column_is_not_present_as_a_field_below_the_grid(String strArg1, String strArg2)
			throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query1 = "select * from Controls1 where ObjectKey= 'ProductExpand'and Name='" + strArg2 + "'";
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator1 = rst1.getString("Locator");
		Thread.sleep(5000);
		waitExplicityForVisibility(By.xpath(locator1));
		WebElement element1 = driver.findElement(By.xpath(locator1));
		jsExecutor.executeScript("arguments[0].click()", element1);
		Thread.sleep(2000);

		String locator = rst.getString("Locator");
		Thread.sleep(3000);
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 2;
		Assert.assertEquals(true, isPresent);
		Thread.sleep(5000);
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='100%';");

	}

	@And("^I assert that \"([^\"]*)\" is non-mondatory$")
	public void i_assert_that_is_not_mandatory(String strArg1) throws Throwable {
		try {
			System.out.println("PageObjects.find({name:'" + strArg1 + "'}).mandatory");
			String mandatory = null;
			mandatory = jsExecutor.executeScript("return PageObjects.find({name:'" + strArg1 + "'}).mandatory")
					.toString();
			System.out.println("Value of mandatory is" + mandatory);
			Assert.assertEquals(null, mandatory);
		} catch (NullPointerException npe) {

		}

	}

	@And("^I assert that \"([^\"]*)\" grid is mandatory$")
	public void i_assert_thatgrid_is_not_mandatory(String strArg1) throws Throwable {
		try {
			Thread.sleep(2000);
			System.out.println("PageObjects.find({gridName:'" + strArg1 + "'}).mandatory");
			String mandatory = null;
			mandatory = jsExecutor.executeScript("return PageObjects.find({gridName:'" + strArg1 + "'}).mandatory")
					.toString();
			System.out.println("Value of mandatory is" + mandatory);
			Assert.assertEquals(null, mandatory);
		} catch (NullPointerException npe) {

		}

	}

	@Then("^I can see \"([^\"]*)\" section is not visible$")
	public void i_can_see_section_is_not_visible(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String locator = rst.getString("Locator");
		Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 1;
		Assert.assertEquals(true, isPresent);

	}

	@When("^I navigate to product screen again by clicking on \"([^\"]*)\" button$")
	public void i_navigate_to_product_screen_again_by_clicking_on_button(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Name= 'Product' and ObjectKey='" + strArg1 + "'";
		System.out.println("select * from Controls1 where Name= 'Product' and ObjectKey='" + strArg1 + "'");
		ResultSet rst = sql.GetDataSet(query);

		while (rst.next()) {
			String locator = rst.getString("Locator");
			String Type = rst.getString("Type");
			String Label = rst.getString("Label");
			switch (Type) {
			case "button":
				Thread.sleep(3000);
				System.out.println("Label is" + Label);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(3000);

			}
		}
	}

	@When("^I navigate to \"([^\"]*)\" screen by clicking on \"([^\"]*)\" button for \"([^\"]*)\"$")
	public void i_navigate_to_screen_again_by_clicking_on_button(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Name= '" + strArg1 + "'and ObjectKey='" + strArg2
				+ "'and Screen='" + strArg3 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String Type = rst.getString("Type");
			switch (Type) {
			case "button":
				Thread.sleep(3000);
				WebElement element = driver.findElement(By.xpath(locator));
				jsExecutor.executeScript("arguments[0].click()", element);
				Thread.sleep(2000);

			}
		}
	}

	@When("^I create a new request with FABEntityType as \"([^\"]*)\" and LegalEntityRole as \"([^\"]*)\"$")
    public void i_create_a_new_request_with_fabentitytype_and_legalentityrole_as_something(String strArg1,
            String strArg2) throws Throwable
{


     SqliteConnection sql = new SqliteConnection();
     SqliteConnection.getInstance();

     ResultSet rst = sql.getControlsData("Controls1", "CompleteRequestFAB");
     while (rst.next()) {
            String name = rst.getString("Name");
            String value = rst.getString("Value");
            String Type = rst.getString("Type");
            String locator = rst.getString("Locator");

            switch (Type) {
            case "button":
                   if (name.equals("NewRequestButton")) {
                         Thread.sleep(1000);
                         WebElement element = driver.findElement(By.xpath(locator));
                          waitExplicityForVisibility(By.xpath(locator));
                         Actions actions = new Actions(driver);
                          actions.moveToElement(element).click().build().perform();
                          waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
                         Thread.sleep(1000);

                   } else if ((name.equals("Create Entity"))) {
                         WebElement element = driver.findElement(By.xpath(locator));
                          waitExplicityForVisibility(By.xpath(locator));
                         Actions actions = new Actions(driver);
                          actions.moveToElement(element).click().build().perform();
                         Thread.sleep(2000);
                          waitExplicityForVisibility(By.xpath("//*[text()='ID']"));
                         Thread.sleep(2000);

                   } else if ((name.equals("ContinueButton"))) {
                         Thread.sleep(1000);
                          waitExplicityForVisibility(By.xpath(locator));
                          driver.findElement(By.xpath(locator)).click();
                         Thread.sleep(2000);
                          waitExplicityForVisibility(By.xpath("//*[text()='Entity of Onboarding']"));
                   }

                   else {
                         Thread.sleep(1000);
                          waitExplicityForVisibility(By.xpath(locator));
                          driver.findElement(By.xpath(locator)).click();
                         Thread.sleep(3000);
                   }
                   break;

            case "Dropdown":

                   Thread.sleep(2000);
                   if (name.equals("Legal Entity Role")) {
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");

                   } else if (name.equals("Client Type")) {
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
                   }

                   else if (value != null) {
                         if (strArg1.equals("Financial Institution (FI)") && name.equalsIgnoreCase("Legal Entity Type")) {
                                value = "Branch of Foreign Entity";
                         } else if (strArg1.equalsIgnoreCase("Non-Bank Financial Institution (NBFI)")
                                       && name.equalsIgnoreCase("Legal Entity Type")) {
                                value = "Bond Issuer & Custodian";
                         } else if (strArg1.equalsIgnoreCase("PCG-Entity") && name.equalsIgnoreCase("Legal Entity Type")) {
                                value = "Regulatory and Statutory Body";
                         } else if (strArg1.equalsIgnoreCase("Business Banking Group (BBG)")
                                       && name.equalsIgnoreCase("Legal Entity Type")) {
                                value = "Regulatory and Statutory Body";
                         } /*else if (strArg1.equalsIgnoreCase("Non-Bank Financial Institution (NBFI)")
                                       && name.equalsIgnoreCase("Legal Entity Category")) {
                                value = "Custodians";
                         } else if (strArg1.equalsIgnoreCase("PCG-Entity")
                                       && name.equalsIgnoreCase("Legal Entity Category")) {
                                value = "Branch of a Foreign Company";
                         } else if (strArg1.equalsIgnoreCase("Business Banking Group (BBG)")
                                       && name.equalsIgnoreCase("Legal Entity Category")) {
                                value = "Branch of a Foreign Company";
                         } else if (strArg1.equals("Financial Institution (FI)")
                                       && name.equalsIgnoreCase("Legal Entity Category")) {
                                value = "Custodians";
                         } */
                         System.out.println(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                   }
                   Thread.sleep(2000);
                   break;
            case "TextBox":
                   if (value != null) {
                          waitExplicityForVisibility(By.xpath(locator));
                          driver.findElement(By.xpath(locator)).sendKeys(value);
                         Thread.sleep(3000);

                   }
                   break;
            case "DatePicker":
                   if (value != null) {

                          jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
                         Thread.sleep(2000);
                   }

            case "MultiSelectDropdown":
                   if (value != null) {
                         Thread.sleep(2000);
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
                         Thread.sleep(2000);
                   }

            }
     }

	}

	@When("^I navigate to \"([^\"]*)\" screen on \"([^\"]*)\" task$")
	public void i_navigate_to_addproduct_screen_on_something_task(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Name= '" + strArg1 + "' and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Thread.sleep(3000);
			// WebElement element = driver.findElement(By.xpath(locator));
			// jsExecutor.executeScript("arguments[0].click()", element);
			waitExplicityForVisibility(By.xpath(locator));
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(5000);
		}
	}

	@When("^I search user from \"([^\"]*)\" on \"([^\"]*)\"$")
	public void i_search_user_from_something_based(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey= 'RelationshipSearch' and Screen='" + strArg2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String Type = rst.getString("Type");
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			Thread.sleep(3000);
			switch (Type) {
			case "button":
				Thread.sleep(3000);
				waitExplicityForVisibility(By.xpath(locator));
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
				break;
			case "Dropdown":
				if (value != null) {
					Thread.sleep(2000);
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
				}
				Thread.sleep(2000);
			}

		}
	}

	@And("^I check that the \"([^\"]*)\" button is disabled$")
	public void i_check_that_the_something_button_is_disabled(String strArg1) throws Throwable {
		Thread.sleep(4000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String locator = rst.getString("Locator");
		WebElement element = driver.findElement(By.xpath(locator));
		boolean disabled = true;
		if (strArg1.equalsIgnoreCase("RiskAssessmentLiteKYC")) {
			String className = element.getAttribute("class");
			if (className.equals("read-only")) {
				Assert.assertEquals(true, disabled);
			}
		} else {
			disabled = element.isEnabled();
			Assert.assertEquals(false, disabled);
		}

	}

	@And("^I assert \"([^\"]*)\" column is visible in the Product Grid for \"([^\"]*)\"$")
	public void i_assert_column_is_visible_in_the_product_grid(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'";
		System.out.println("select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='" + strArg2 + "'");
		ResultSet rst = sql.GetDataSet(query);

		String locator = rst.getString("Locator");
		Thread.sleep(5000);
		waitExplicityForVisibility(By.xpath(locator));
		WebElement casestatus = driver.findElement(By.xpath(locator));

		// WebElement casestatus =
		// driver.findElement(By.xpath("(//div[@class='fen-datagrid-expandablerow__cell'])[12]"));
		casestatus.getText();
		Assert.assertEquals(strArg1, casestatus.getText());
		Thread.sleep(3000);

	}

	@Test
	@When("^I navigate to \"([^\"]*)\" task$")
	public void i_navigate_to_task(String strArg1) throws SQLException {
        try {
             //Thread.sleep(1000);
             SqliteConnection sql = new SqliteConnection();
             SqliteConnection.getInstance();
             String query = "select * from Controls1 where ObjectKey='" + strArg1 + "'and Name='Task'";

             ResultSet rst = sql.GetDataSet(query);
             // String name = rst.getString("Name");
             String key = rst.getString("ObjectKey");
             String locator = rst.getString("Locator");
             if (key.equals("EnrichKYCProfileGrid") || (key.equals("ValidateKYCandRegulatoryGrid"))
                        || (key.equals("ReviewRequestGrid"))) {
                   // Thread.sleep(10000);
                   
                  if(strArg1.equals("ReviewRequestGrid")||(strArg1.equals("CaptureProposedChangesGrid"))){
                        waitExplicityForVisibility(By.xpath("(//span[@class='read-only'])[3]"));
                        Thread.sleep(5000);
//                      System.out.println("task name : "+strArg1 +" waiting for 10 secs");
                        }
                  waitExplicityForVisibility(By.xpath(locator));
                   Thread.sleep(1000);
                   System.out.println("Inside" + strArg1 + " task" );
                   Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
                   // System.out.println(isPresent);
                   WebElement element = driver.findElement(By.xpath(locator));
                  jsExecutor.executeScript("arguments[0].click()", element);
                   Thread.sleep(3000);
                  waitExplicityForVisibility(By.xpath("//label[text()='Legal Entity Type']"));
                   Thread.sleep(1000);

             } else {
                   //Thread.sleep(1000);
                   Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
                   // System.out.println(isPresent);
                  waitExplicityForVisibility(By.xpath(locator));
                   Thread.sleep(1000);
                   WebElement element = driver.findElement(By.xpath(locator));
                  jsExecutor.executeScript("arguments[0].click()", element);
                   System.out.println("Inside" + strArg1 + " task" );
                   Thread.sleep(5000);
             }

        } catch (Exception e) {
             // assertTrue(false, e.getLocalizedMessage());
             // TODO Auto-generated catch block
             // driver.close();
             e.printStackTrace();

        }

	}

	@Test
	@When("^I navigate to \"([^\"]*)\" from LHN section$")
	public void i_navigate_to_from_lhn_section(String strArg1) throws Throwable {

		try {
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			String query = "Select * from Controls1 where Name='Cases'";
			ResultSet rst = sql.GetDataSet(query);
			while (rst.next()) {
				String locator = rst.getString("Locator");
				WebElement element = driver.findElement(By.xpath(locator));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().build().perform();
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(5000);

			}

		} catch (Exception e) {
			driver.close();
			e.printStackTrace();

		}

		// Thread.sleep(3000);
	}

	@When("^I navigate to the \"([^\"]*)\" from LHN section$")
	public void i_navigate_from_lhn_section(String strArg1) throws Throwable {

		try {
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			String query = "Select * from Controls1 where ObjectKey='LHN' and Name='" + strArg1 + "'";
			ResultSet rst = sql.GetDataSet(query);
			while (rst.next()) {
				String locator = rst.getString("Locator");
				WebElement element = driver.findElement(By.xpath(locator));
				Actions actions = new Actions(driver);
				actions.moveToElement(element).click().build().perform();
				// driver.findElement(By.xpath(locator)).click();
				Thread.sleep(5000);

			}

		} catch (Exception e) {
			driver.close();
			e.printStackTrace();

		}

		// Thread.sleep(3000);
	}

	@Then("^I assert that the status of the documents is \"([^\"]*)\"$")
	public void i_assert_that_the_status_of_the_documents(String strArg1) throws Throwable {

	}

	@Then("^I Validate the CaseName Contains \"([^\"]*)\" in it$")
	public void i_validate_the_casename_contains_something_in_it(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "CaseName");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String CaseName = driver.findElement(By.xpath(locator)).getText();
			System.out.println("Case Name is " + CaseName);
			Assert.assertTrue(CaseName.contains(strArg1));
		}
	}

	@Test
	@When("^I store the \"([^\"]*)\" from LE360$")
	public void i_store_the_caseid_from_le360(String strArg1) throws Throwable {
		try {
			SqliteConnection sql = new SqliteConnection();
			SqliteConnection.getInstance();
			String query = "select * from Controls1 where Name='CaseId'";
			ResultSet rst = sql.GetDataSet(query);
			while (rst.next()) {
				String locator = rst.getString("Locator");
				String CaseId = driver.findElement(By.xpath(locator)).getText();
				Thread.sleep(2000);
				System.out.println("CaseId being stored is : " + CaseId);
				scenariocontext.setValue("Key", CaseId);

			}

		} catch (Exception e) {
			driver.close();
			e.printStackTrace();

		}

	}

	@And("^I verify below Documents are Mandatory$")
	public void i_verify_below_documents_are_mandatory(DataTable table) throws Throwable {
		int j = 1;
		String labelname = null;
		driver.findElement(By.xpath("//div[@class='fen-data-grid-sort__value']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//div[@class='fen-data-grid-sort__option'])[8]")).click();
		Thread.sleep(5000);
		ArrayList<String> arlist = new ArrayList<String>();
		ArrayList<String> Expected = new ArrayList<String>();

		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("Documents");

			Expected.add(labelname);

			System.out.println("Value of data field is" + labelname);

		}
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "DocumentMandatory");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			for (int i = 4; i < 30; i = i + 4) {
				String Locator = "(//span[@class='fen-dataGrid-cell '])[" + i + "]";
				System.out.println("(//span[@class='fen-dataGrid-cell '])[" + i + "]");
				String value = driver.findElement(By.xpath(Locator)).getText();
				System.out.println("Value of the mandatory is" + value);
				if (value.equals("Yes")) {

					String Document = "(//a[@class='fen-grid-link fen-grid-link'])[" + j + "]";
					String DocumentName = driver.findElement(By.xpath(Document)).getText();
					System.out.println("Name of the document is " + DocumentName);

					arlist.add(DocumentName);
					j = j + 1;

				}
			}

		}

		Assert.assertEquals(Expected, arlist);
	}

	@Test
	@When("^I search for the \"([^\"]*)\"$")
	public void i_search_for_the_caseid(String strArg1) throws Throwable {
        try {
             SqliteConnection sql = new SqliteConnection();
             SqliteConnection.getInstance();
             // String query="Select * from Controls1 where
             // Name='"+scenariocontext.getValue("Key")+"'";
             // String query2="Select * from Controls1 where Name='"+strArg1+"'";

             // System.out.println(query);
             // ResultSet rst=sql.GetDataSet(query);
             String CaseID = "";
             try {
                   CaseID = scenariocontext.getValue("Key").toString();
             } catch (Exception e) {

             }
             ResultSet rst1 = sql.getControlsDataByName("SearchIcon");
             String locatorForSearchIcon = rst1.getString("Locator");
             waitExplicityForVisibility(By.xpath(locatorForSearchIcon));
             Thread.sleep(2000);
             driver.findElement(By.xpath(locatorForSearchIcon)).click();
             Thread.sleep(1000);
             ResultSet rst2 = sql.getControlsDataByName("SearchByCaseID");
             String locatorForCaseSearch = rst2.getString("Locator");
             Thread.sleep(2000);
             waitExplicityForVisibility(By.xpath(locatorForCaseSearch));
             driver.findElement(By.xpath(locatorForCaseSearch)).click();

             ResultSet rst3 = sql.getControlsDataByName("CaseIDSearchTextBox");
             String locatorForCaseIDSearchTextBox = rst3.getString("Locator");
             waitExplicityForVisibility(By.xpath(locatorForCaseIDSearchTextBox));
             if (strArg1.equalsIgnoreCase("CaseId")) {
                   if (CaseID != null) {
                        System.out.println("Searching for case Id :" + CaseID);
                        driver.findElement(By.xpath(locatorForCaseIDSearchTextBox)).clear();
                        driver.findElement(By.xpath(locatorForCaseIDSearchTextBox)).sendKeys(CaseID);
                        message.write("Case Id Searched is "+CaseID);
                   } else {
                        System.out.println("Case ID was not retreived, hence closing the execution");
                        driver.close();
                   }
             } else {
                   System.out.println("Searching for manually entered case Id :" + strArg1);
                  driver.findElement(By.xpath(locatorForCaseIDSearchTextBox)).clear();
                  driver.findElement(By.xpath(locatorForCaseIDSearchTextBox)).sendKeys(strArg1);
                   System.out.println("Now Storing Manually entered Case ID : " + strArg1);
                   scenariocontext.setValue("Key", strArg1);
                   message.write("Case Id Searched is "+strArg1);
                   
             }
             Thread.sleep(2000);
             ResultSet rst4 = sql.getControlsDataByName("SearchResultForCaseId");
             String locatorForSearchResultForCaseId = rst4.getString("Locator");
             waitExplicityForVisibility(By.xpath(locatorForSearchResultForCaseId));
             driver.findElement(By.xpath(locatorForSearchResultForCaseId)).click();

             ResultSet rst5 = sql.getControlsDataByName("CaseStatusUponSearch");
             String locatorForCaseStatus = rst5.getString("Locator");
             waitExplicityForVisibility(By.xpath(locatorForCaseStatus));
             Thread.sleep(2000);

        } catch (Exception e) {
//           driver.close();
             e.printStackTrace();

        }

	}

	@When("^validate if confirmed matches number populates correctly in Complete AML screen according to below table$")
	public void validate_if_confirmed_matches_number_populates_correctly_in_Complete_AML_screen_according_to_below_table(
			DataTable table) throws Throwable {
		// String labelname;
		List<Map<String, String>> list = table.asMaps(String.class, String.class);
		for (Map<String, String> data : list) {
			String labelname = data.get("FieldLabel");
			String label = "//label[text()='" + labelname + "']";
			System.out.println(label);
			String screenValueXpath = label + "//following-sibling::div//div//div";
			String screenvalue = driver.findElement(By.xpath(screenValueXpath)).getText();
			String inputValue = data.get("Number");
			Assert.assertEquals(inputValue, screenvalue);

		}
	}

	@When("^I complete \"([^\"]*)\" from assessment grid with Key \"([^\"]*)\"$")
	public void i_complete_from_assessment_grid_with_Key(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		Thread.sleep(5000);
		ResultSet rst1 = sql.getControlsData("Controls1", "ActionsInAssessment");
//		int count=driver.findElements(By.xpath("//div[@class='f-context-menu']")).size();
	   String locator1=rst1.getString("Locator");
			int count=driver.findElements(By.xpath(locator1)).size();
			
		
		System.out.println("Number of assessment is " +count);
		for (int i=1;i<=count;i++) {
		
		driver.findElement(By.xpath("(//div[@class='f-context-menu'])["+i+"]")).click();
		Thread.sleep(3000);
		ResultSet rst = sql.getControlsData("Controls1", "Assessment");
		while (rst.next()) {
			Thread.sleep(2000);
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(3000);
			// SqliteConnection sql1 = SqliteConnection.getInstance();

		}
		ResultSet rst2 = sql.buildQuery("Screening", strArg2);
		FillInDataWithoutValidation(rst2);
		
		}

	}

	@Then("^I Verify the list of Documents with mandatory and non mandatory field$")
	public void i_verify_the_list_of_documents_with_mandatory_and_non_mandatory_field() throws Throwable {

	}

	@When("^I navigate to \"([^\"]*)\" screen of the associated party$")
	public void i_navigate_to_something_screen_of_the_associated_party(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			Thread.sleep(2000);
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(3000);
		}
	}

	@When("^I add associatedparties in ID&V$")
	public void i_add_associatedparties_in_idv() throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "ID&V");
		while (rst.next()) {
			Thread.sleep(2000);
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(3000);
		}
	}

	@And("^I check that below data is not visible$")
	public void i_check_that_below_data_is_not_visible(DataTable table) throws Throwable {
		String labelname;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");
			//String value = "//label[text()='" + labelname + "']";
			String value = "//label[text()="+"\"" + labelname +"\"" +"]";
			System.out.println(value);
			Boolean element = driver.findElements(By.xpath(value)).size() < 1;
			if (element == false) {
				String valueParentName = value + "//parent::div";
				WebElement element1 = driver.findElement(By.xpath(valueParentName));
				Thread.sleep(2000);
				String getClassAttribute = element1.getAttribute("class");
				if (getClassAttribute.contains("row hidden")) {
					element = true;
					Assert.assertEquals(true, element);
				}

			} else {
				Assert.assertEquals(true, element);
			}

		}

	}
		

	@And("^I clear \"([^\"]*)\" field$")
	public void i_clear_field(String strArg1) throws Throwable {
		jsExecutor.executeScript("return PageObjects.find({caption:'" + strArg1 + "'}).clear()");
	}

	@And("^I check that below data is visible$")
	public void i_check_that_below_data_is_visible(DataTable table) throws Throwable {
		String labelname;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			labelname = data.get("FieldLabel");
			String value = "//label[text()='" + labelname + "']";
			System.out.println(value);
			waitExplicityForVisibility(By.xpath(value));
			Boolean element = driver.findElements(By.xpath(value)).size() > 0;
			if (element == true) {
				String valueParentName = value + "//parent::div";
				WebElement element1 = driver.findElement(By.xpath(valueParentName));
				Thread.sleep(2000);
				String getClassAttribute = element1.getAttribute("class");
				if (getClassAttribute.contains("row hidden")) {
					element = false;
					Assert.assertEquals(false, element);
				}

			} else {
				Assert.assertEquals(false, element);
			}
			
			Thread.sleep(2000);

		}

	}
	 @Then("^I verify \"([^\"]*)\" type is \"([^\"]*)\"$")
	    public void i_verify_type(String strArg1, String strArg2) throws Throwable {
		 String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + strArg1 + "'}).type")
					.toString();
			Assert.assertEquals(strArg2, a);
	 
	    }

	@Then("^I select area of change as \"([^\"]*)\" and complete Maintenance request$")
	public void i_select_area_of_change_as_something_and_complete_maintenance_request(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Controls1 where ObjectKey='MaintenanceRequest'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {
			case "button":
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(2000);
				break;
			case "Dropdown":
				Thread.sleep(2000);
				// System.out.println("PageObjects.find({caption:'"+ name
				// +"'}).setValueByLookupText('"+ value +"')");
				jsExecutor.executeScript(
						"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
				break;
			case "TextBox":
				if (value != null) {
					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;

			default:
				if (name == "Create Entity Button") {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(6000);
				}
			}
		}

	}

	@Then("^I assert that \"([^\"]*)\" is not visible$")
	public void i_assert_that_something_is_not_visible(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean isPresent = driver.findElements(By.xpath(locator)).size() < 0;
			Assert.assertEquals(false, isPresent);

		}

	}

	@And("^I add \"([^\"]*)\" via express addition$")
	public void i_add_association_task(String strArg1) throws Throwable {

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			String datakey = rst.getString("Datakey");
			switch (Type) {

			case "button":
				Thread.sleep(5000);
				// System.out.println("Name is" + name);
				if (name.equals("Continue")) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(10000);
				} else
					Thread.sleep(2000);
				waitExplicityForVisibility(By.xpath(locator));
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(5000);
				break;
			case "Dropdown":
				if (value != null) {
					Thread.sleep(2000);

					if (name.equals("Legal Entity Type")) {
						// System.out.println("Datakey is " + datakey);
						System.out.println(
								"PageObjects.find({dataKey:'" + datakey + "'}).setValueByLookupText('" + value + "')");
						jsExecutor.executeScript(
								"PageObjects.find({dataKey:'" + datakey + "'}).setValueByLookupText('" + value + "')");
						Thread.sleep(5000);
					} else
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					Thread.sleep(2000);

				}
				break;

			case "MultiSelectDropdown":
				if (value != null) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					Thread.sleep(2000);
				}
				break;
			case "TextBox":
				if (value != null) {

					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
					break;
				}

			}

		}
		Thread.sleep(4000);
	}
	@Then("^I remove the existing \"([^\"]*)\" from AssociatedPartiesGrid$")
	@When("^I complete \"([^\"]*)\" task$")
	public void i_complete_task(String strArg1) throws Throwable {
//
//		try {
//			Boolean isPresent = driver.findElements(By.xpath("//div[text()='Cases']")).size() > 0;
//			if (isPresent == true) {
//				Thread.sleep(6000);
//				WebElement element = driver.findElement(By.xpath("//div[text()='Cases']"));
//				waitExplicityForVisibility(By.xpath("//div[text()='Cases']"));
//				Actions actions = new Actions(driver);
//				actions.moveToElement(element).click().build().perform();
//				Thread.sleep(2000);
//				WebElement element2 = driver.findElement(By.xpath("//a[@title='Capture Request Details']"));
//				waitExplicityForVisibility(By.xpath("//a[@title='Capture Request Details']"));
//				jsExecutor.executeScript("arguments[0].click()", element2);
//				Thread.sleep(2000);
//			}
		Thread.sleep(5000);

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			String Type = rst.getString("Type");
			String locator = rst.getString("Locator");
			switch (Type) {

			case "button":
				Thread.sleep(5000);
				// System.out.println("Name is" + name);
				if (name.equals("Continue")) {
//						waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(10000);
				} else if (name.equals("savebutton")) {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(10000);
				} else {
					Thread.sleep(2000);
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).click();
					
					if(strArg1.equals("CaptureFABReferences") && (name.equals("Continuebutton") )){
		                    waitIfEnvIsFAB(25);//megha
		                    
		                    }
		                    //megha
		                    Thread.sleep(5000);

		                    break;
		                    }
				
				
		
			case "Dropdown":
				if (value != null) {
					Thread.sleep(2000);

					if (name.equals("Can the corporation issue bearer shares?")) {
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
						Thread.sleep(5000);
					} else
						jsExecutor.executeScript(
								"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
					Thread.sleep(2000);

				}
				break;

			case "MultiSelectDropdown":
				if (value != null) {
					System.out.println(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
					Thread.sleep(2000);
				}
				break;
			case "TextBox":
				if(name.equals("External Reference")) {
						String NumericData = GenericStepDefinition.getNumericString(5);
//						String AlphabetData=GenericStepDefinition.getAlphabetString(2);
						waitExplicityForVisibility(By.xpath(locator));
                        String dataToSend = NumericData;
                        driver.findElement(By.xpath(locator)).sendKeys(dataToSend);
                        scenariocontext.setValue("GLCMSUID", dataToSend);

//						driver.findElement(By.xpath(locator)).sendKeys(NumericData);
						Thread.sleep(2000);
					}
					else if (value != null) {
					waitExplicityForVisibility(By.xpath(locator));
					driver.findElement(By.xpath(locator)).sendKeys(value);
					Thread.sleep(2000);
				}
				break;
			case "CheckBox":
				Thread.sleep(2000);
				if((value != null)) {
					jsExecutor.executeScript(
							"PageObjects.find({caption:'" + name + "'}).setValue(" + value + ")");
				}
				else {
				
//				 System.out.println("Name is" + name);
//				waitExplicityForVisibility(By.xpath(locator));
//				Thread.sleep(1000);
//				driver.findElement(By.xpath(locator)).click();
//				Thread.sleep(3000);
				}
				break;
				
				
			case "DatePicker":
				if (value != null) {

					jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
					Thread.sleep(2000);
					break;
				}

			}

		}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			//			assertTrue(falsee.getLocalizedMessage(),);
//			//    			File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//			//        		FileUtils.copyFile(src,new File("C:\\Users\\sansingh\\eclipse-workspace\\CBAFenergoAutomation\\src\\test\\resources\\Screenshot.png"));
//			//			driver.close();
//		}

	}

	@And("^I click on \"([^\"]*)\" button$")
	public void i_click_on_something_button(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String key = rst.getString("ObjectKey");

			if (key.equals("SaveandCompleteforValidateKYC")) {
				Thread.sleep(3000);
				waitExplicityForVisibility(By.xpath(locator));
				WebElement element = driver.findElement(By.xpath(locator));
				element.click();
				Thread.sleep(10000);
			}

			else {
				Thread.sleep(3000);
				waitExplicityForVisibility(By.xpath(locator));
				WebElement element = driver.findElement(By.xpath(locator));
				element.click();
				Thread.sleep(10000);
				if (strArg1.equalsIgnoreCase("CreateEntity")) {

					waitExplicityForVisibility(By.xpath("//*[text()='ID']"));

				}

			}
		}

	}

	@When("^I navigate to \"([^\"]*)\" link by clicking on \"([^\"]*)\" link under options$")
	public void i_navigate_to_something_link_by_clicking_on__link_under_options(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		Thread.sleep(5000);
		ResultSet rst = sql.getControlsData("Controls1", "OptionMenu");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			WebElement element = driver.findElement(By.xpath(locator));
			element.click();
			Thread.sleep(2000);
		}
	}

	@When("^I navigate to \"([^\"]*)\" link by clicking on \"([^\"]*)\" link under action$")
	public void i_navigate_to_link_by_clicking_on__link_under_action(String strArg1, String strArg2) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		Thread.sleep(5000);
		String query = "select * from Controls1 where ObjectKey='" + strArg2 + "'and Name='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String locator = rst.getString("Locator");

			WebElement element = driver.findElement(By.xpath(locator));
			element.click();
			Thread.sleep(3000);
		}
	}

	@Then("^I can see \"([^\"]*)\" drop-down is displaying under \"([^\"]*)\" section$")
	public void i_can_see_something_dropdown_is_displaying_under_something_section(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String name = rst.getString("Name");
			String type = rst.getString("Type");
			switch (type) {

			case "Dropdown":
				String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + strArg1 + "'}).type")
						.toString();
				System.out.println("Value of type is " + a);
				Assert.assertEquals("MultiSelectDropdown", a);
				break;

			}

		}
	}

	@When("^I search for \"([^\"]*)\" user by providing value in \"([^\"]*)\" textbox.$")
	public void i_search_for_something_user_by_providing_value_in_something_textbox(String strArg1, String strArg2)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "UserManagement");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			String type = rst.getString("Type");
			String name = rst.getString("Name");
			String value = rst.getString("Value");
			switch (type) {
			case "TextBox":
				if (name.equals("FirstName")) {
					driver.findElement(By.xpath(locator)).sendKeys(strArg1);
					Thread.sleep(2000);
				} else {
					driver.findElement(By.xpath(locator)).sendKeys(value);
				}

				break;
			case "button":
				if (name.equals("Edit")) {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(5000);
				}

				else {
					driver.findElement(By.xpath(locator)).click();
					Thread.sleep(2000);

				}
				break;
			}

		}
	}

	@When("^I navigate to \"([^\"]*)\" button with ClientType as \"([^\"]*)\"$")
    public void i_navigate_to_something_button_with_clienttype_as_something(String strArg1, String strArg2)
            throws Throwable {
     Thread.sleep(3000);
     SqliteConnection sql = new SqliteConnection();
     SqliteConnection.getInstance();
     ResultSet rst = sql.getControlsData("Controls1", strArg1);
     while (rst.next()) {
            String name = rst.getString("Name");
            String value = rst.getString("Value");
            String Type = rst.getString("Type");
            String locator = rst.getString("Locator");

            switch (Type) {
            case "button":
                   if (name.equals("NewRequestButton")) {
                         Thread.sleep(2000);
                          waitExplicityForVisibility(By.xpath(locator));
                         WebElement element = driver.findElement(By.xpath(locator));
                         Actions actions = new Actions(driver);
                          actions.moveToElement(element).click().build().perform();
                          waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
                         Thread.sleep(3000);

                   } else if ((name.equals("Create Entity"))) {
                         WebElement element = driver.findElement(By.xpath(locator));
                         Actions actions = new Actions(driver);
                          waitExplicityForVisibility(By.xpath("//*[text()='Legal Entity Name']"));
                          actions.moveToElement(element).click().build().perform();
                          waitExplicityForVisibility(By.xpath("//*[text()='ID']"));
                         Thread.sleep(3000);

                   } else {
                          waitExplicityForVisibility(By.xpath(locator));
                          driver.findElement(By.xpath(locator)).click();
                         Thread.sleep(3000);

                         break;
                   }
            
            case "Dropdown":
                   Thread.sleep(2000);
                   if (name.equals("Client Type")) {
                         try {
                                waitExplicityForVisibility(By.xpath("//*[text()='"+name+"']"));
                         } catch (Exception e) {
                                Thread.sleep(3000);
                         }
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");
                   }

                   else if (value != null) {
                         if(strArg2.equals("Financial Institution (FI)") && name.equalsIgnoreCase("Legal Entity Type")){
                                value = "Branch of Foreign Entity";
                         } else if(strArg2.equalsIgnoreCase("Non-Bank Financial Institution (NBFI)") && name.equalsIgnoreCase("Legal Entity Type")){
                                value = "Bond Issuer & Custodian";
                         } else if(strArg2.equalsIgnoreCase("PCG-Entity") && name.equalsIgnoreCase("Legal Entity Type")){
                                value = "Regulatory and Statutory Body";
                         }else if(strArg2.equalsIgnoreCase("Business Banking Group (BBG)") && name.equalsIgnoreCase("Legal Entity Type")){
                                value = "Regulatory and Statutory Body";
                         }else if(strArg2.equalsIgnoreCase("Corporate") && name.equalsIgnoreCase("Legal Entity Type")){
                                value = "Regulatory and Statutory Body";
                         }/*else if(strArg2.equalsIgnoreCase("Corporate)") && name.equalsIgnoreCase("Legal Entity Category")){
                                value = "Branch of a Foreign Company";
                         }else if(strArg2.equalsIgnoreCase("Non-Bank Financial Institution (NBFI)") && name.equalsIgnoreCase("Legal Entity Category")){
                                value = "Custodians";
                         } else if(strArg2.equalsIgnoreCase("PCG-Entity") && name.equalsIgnoreCase("Legal Entity Category")){
                                value = "Branch of a Foreign Company";
                         }else if(strArg2.equalsIgnoreCase("Business Banking Group (BBG)") && name.equalsIgnoreCase("Legal Entity Category")){
                                value = "Branch of a Foreign Company";
                         }else if(strArg2.equals("Financial Institution (FI)") && name.equalsIgnoreCase("Legal Entity Category")){
                                value = "Custodians";
                         }*/
                         waitForDropDown(name);
                         System.out.println("Dropdown field name :'"+name+"' and value is '"+value);
                          System.out.println("PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                   }
                   Thread.sleep(2000);
                   break;
            
            
            case "TextBox":
                   if (value != null) {
                          waitExplicityForVisibility(By.xpath(locator));
                         Thread.sleep(2000);
                          driver.findElement(By.xpath(locator)).sendKeys(value);
                         Thread.sleep(2000);

                   }
                   break;
            case "DatePicker":
                   if (value != null) {

                          jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
                         Thread.sleep(2000);
                   }

                   break;

            case "MultiSelectDropdown":
                   if (value != null) {
                         jsExecutor.executeScript(
                                       "PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + strArg1 + "'])");
                         Thread.sleep(2000);
                   }

            }
     }

	}

	@Then("^I verify \"([^\"]*)\" drop-down values$")
	public static List<String> GetOptions(String strArg1) throws Throwable {
		List<String> ls = null;
		ArrayList<String> Actual = null;
		ArrayList<String> Expected = null;
		int num1 = 0;
		int num2 = 5;

		while (ls == null && num1 < num2) {
			try {
				Thread.sleep(10000);

				String Dropdown = (String) jsExecutor.executeScript("var perfarray=PageObjects.find({caption:'"
						+ strArg1 + "'}).lookups;" + "return JSON.stringify(perfarray);");
				Dropdown.trim();
				// System.out.println(jsExecutor.executeScript("var
				// perfarray=PageObjects.find({caption:'" + strArg1 + "'}).lookups;"+"return
				// JSON.stringify(perfarray);"));
				CommonSteps cs = new CommonSteps();
				Actual = cs.parseListData(Dropdown, "label");
				// System.out.println(Actual);

			} catch (Exception e) {

			}
			num1++;
		}
		Expected = Expectedvalues(strArg1);
		// System.out.println("Expected is" + Expected);
		// System.out.println("Actual is" + Actual);

		Assert.assertEquals(Expected, Actual);

		return ls;

	}

	@Then("^I verify \"([^\"]*)\" drop-down values with ClientType as \"([^\"]*)\"$")
	public static List<String> GetOptions(String strArg1, String strArg2) throws Throwable {
		List<String> ls = null;
		ArrayList<String> Actual = null;
		ArrayList<String> Expected = null;
		int num1 = 0;
		int num2 = 5;

		while (ls == null && num1 < num2) {
			try {
				Thread.sleep(10000);

				String Dropdown = (String) jsExecutor.executeScript("var perfarray=PageObjects.find({caption:'"
						+ strArg1 + "'}).lookups;" + "return JSON.stringify(perfarray);");
				// System.out.println(jsExecutor.executeScript("var
				// perfarray=PageObjects.find({caption:'" + strArg1 + "'}).lookups;"+"return
				// JSON.stringify(perfarray);"));
				CommonSteps cs = new CommonSteps();
				Actual = cs.parseListData(Dropdown, "label");
				// System.out.println("Actual is"+Actual);

			} catch (Exception e) {

			}
			num1++;
		}
		Expected = Expectedvalue(strArg1, strArg2);
		System.out.println("Expected is" + Expected);
		System.out.println("Actual is" + Actual);

		Assert.assertEquals(Expected, Actual);

		return ls;
	}

	@Then("^I verify \"([^\"]*)\" drop-down values with \"([^\"]*)\" as \"([^\"]*)\"$")
	public static List<String> GetLOVValues(String strArg1, String strArg2, String strArg3) throws Throwable {
		List<String> ls = null;
		ArrayList<String> Actual = null;
		ArrayList<String> Expected = null;
		int num1 = 0;
		int num2 = 5;

		while (ls == null && num1 < num2) {
			try {
				Thread.sleep(10000);

				String Dropdown = (String) jsExecutor.executeScript("var perfarray=PageObjects.find({caption:'"
						+ strArg1 + "'}).lookups;" + "return JSON.stringify(perfarray);");
				// System.out.println(jsExecutor.executeScript("var
				// perfarray=PageObjects.find({caption:'" + strArg1 + "'}).lookups;"+"return
				// JSON.stringify(perfarray);"));
				CommonSteps cs = new CommonSteps();
				Actual = cs.parseListData(Dropdown, "label");
				// System.out.println("Actual is"+Actual);

			} catch (Exception e) {

			}
			num1++;
		}
		Expected = ExpectedLOVvalue(strArg1, strArg2, strArg3);
		System.out.println("Expected is" + Expected);
		System.out.println("Actual is" + Actual);

		Assert.assertEquals(Expected, Actual);

		return ls;
	}

	@And("^I check that \"([^\"]*)\" is readonly$")
	public void i_check_that_something_is_readonly(String strArg1) throws Throwable {

	}

	public static ArrayList<String> Expectedvalues(String args1) throws Throwable {
		ArrayList<String> ExpectedLOV = new ArrayList<String>();
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Lovs where ObjectKey='" + args1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String value = rst.getString("Value");
			System.out.println("Value is" + value);
			ExpectedLOV.add(value);

		}
		return ExpectedLOV;

	}

	public static ArrayList<String> Expectedvalue(String args1, String args2) throws Throwable {
		ArrayList<String> ExpectedLOV = new ArrayList<String>();
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Lovs where ObjectKey='" + args1 + "' and ClientType='" + args2 + "'";
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String value = rst.getString("Value");
			System.out.println("Value is" + value);
			ExpectedLOV.add(value);
		}

		return ExpectedLOV;

	}

	public static ArrayList<String> ExpectedLOVvalue(String args1, String args2, String args3) throws Throwable {
		ArrayList<String> ExpectedLOV = new ArrayList<String>();
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Lovs where ObjectKey='" + args1 + "' and " + args2 + "='" + args3 + "'";
		System.out.println("Select * from Lovs where ObjectKey='" + args1 + "' and " + args2 + "='" + args3 + "'");
		ResultSet rst = sql.GetDataSet(query);
		while (rst.next()) {
			String value = rst.getString("Value");
			System.out.println("Value is" + value);
			ExpectedLOV.add(value);
		}

		return ExpectedLOV;

	}

	@When("^I click on \"([^\"]*)\" from LHS$")
	public void i_click_on_something_from_lhs(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Controls1 where ObjectKey='" + strArg1 + "'";
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(5000);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			WebElement element = driver.findElement(By.xpath(locator));
			Actions actions = new Actions(driver);
			actions.moveToElement(element).click().build().perform();
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(5000);
		}

	}

	@And("^I check that \"([^\"]*)\" grid is disabled$")
	public void i_check_that_something_grid_is_disabled(String strArg1) throws Throwable {
		String a = jsExecutor.executeScript("return PageObjects.find({name:'" + strArg1 + "'}).disabled").toString();
		Assert.assertEquals("true", a);
	}

	@And("^I check that \"([^\"]*)\" is Mandatory$")
	public void i_check_that__is_mandatory(String arg1) throws Throwable {

		try {
			if (arg1.contains("Grid")) {
				String a = jsExecutor.executeScript("return PageObjects.find({name:'" + arg1 + "'}).mandatory")
						.toString();
				Assert.assertEquals("true", a);
			} else {
				System.out.println("Value is " + arg1);
				Thread.sleep(2000);
				String a = jsExecutor.executeScript("return PageObjects.find({caption:'" + arg1 + "'}).mandatory")
						.toString();
				System.out.println("Value of a is " + a);
				
					Assert.assertEquals("true", a);
				
			}
		} catch (Exception e) {
			assertTrue(false, e.getLocalizedMessage());
		}
	}
//		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(src, new File(
//				"C:\\Users\\sansingh\\eclipse-workspace\\CBAFenergoAutomation\\src\\test\\resources\\Screenshot1.png"));

	// driver.close();

	@When("^I initiate \"([^\"]*)\" from action button$")
	public void i_initiate_from_action_button(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		waitExplicityForVisibility(By.xpath("//div[@class='fen-subtitlelinkcontent']"));
		driver.findElement(By.xpath("//div[@class='fen-subtitlelinkcontent']")).click();
		Thread.sleep(6000);
		waitExplicityForVisibility(By.xpath("//span[@class='icon fen-icon-ellipsis undefined']"));
		driver.findElement(By.xpath("//span[@class='icon fen-icon-ellipsis undefined']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//span[text()='" + arg1 + "']")).click();
//		waitExplicityForVisibility(By.xpath("//*[@class='read-only' and text()='Review Client Data']"));
		Thread.sleep(2000);
		

	}

	@Then("^I can see product is visible in Product grid$")
	public void i_can_see_product_is_visible_in_product_grid() throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Screen= 'CaptureRequestDetails' and ObjectKey='ProductGridExpand'";
		ResultSet rst = sql.GetDataSet(query);
		// ResultSet rst1 = sql.getControlsData("Controls1", "ProductGridExpand");
		String query1 = "select * from Controls1 where Name= 'CaptureRequestDetails' and ObjectKey='ProductExpand'";
		ResultSet rst1 = sql.GetDataSet(query1);

		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");

		Thread.sleep(5000);
		driver.findElement(By.xpath(locator)).click();
		Thread.sleep(3000);

		Boolean isPresent = driver.findElements(By.xpath(locator1)).size() < 2;
		Assert.assertEquals(true, isPresent);

	}

	@Then("^I can see product is visible in Product grid for \"([^\"]*)\"$")
	public void i_can_see_product_is_visible_in_product_grid_for_Screen(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "select * from Controls1 where Screen= '" + strArg1 + "' and ObjectKey='ProductGridExpand'";
		ResultSet rst = sql.GetDataSet(query);
		// ResultSet rst1 = sql.getControlsData("Controls1", "ProductGridExpand");
		String query1 = "select * from Controls1 where Name=  '" + strArg1 + "' and ObjectKey='ProductExpand'";
		ResultSet rst1 = sql.GetDataSet(query1);
		String name = rst.getString("Name");
		String locator = rst.getString("Locator");
		String locator1 = rst1.getString("Locator");
		while (rst.next()) {
			if (name.equals("ValidateKYCRequest")) {
				continue;
			} else {
				Thread.sleep(2000);
				driver.findElement(By.xpath(locator)).click();
				Thread.sleep(3000);
			}
		}
		Boolean isPresent = driver.findElements(By.xpath(locator1)).size() < 2;
		Assert.assertEquals(true, isPresent);
		Thread.sleep(3000);
	}

	@AfterClass(alwaysRun = true)
	public void screenShot(ITestResult result) {
		if (ITestResult.FAILURE == result.getStatus()) {

			try {
				TakesScreenshot screenshot = (TakesScreenshot) driver;
				File src = screenshot.getScreenshotAs(OutputType.FILE);

				// File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(src, new File(
						"C:\\Users\\sansingh\\eclipse-workspace\\CBAFenergoAutomation\\src\\test\\resources\\Screenshot.png"));
				System.out.println("Taken Screenshots");
			} catch (Exception e) {
				System.out.println("Exception while taking screenshot " + e.getMessage());
			}
		}
		driver.quit();

	}

	@Then("^I validate the following fields in \"([^\"]*)\" Screen$")
	@And("^I validate the following fields in \"([^\"]*)\" Sub Flow$")
	public void i_validate_the_following_fields_in_sub_flow(String Arg1, DataTable table) throws InterruptedException {Thread.sleep(6000);
    List<Map<String, String>> list = table.asMaps(String.class, String.class);
    for (Map<String, String> data : list) {
      Thread.sleep(2000);
      String label = data.get("Label").trim();
      System.out.println("Label name is" + label);
      String fieldType = data.get("FieldType").trim();
      String visible = data.get("Visible").trim();
      String readonly = data.get("ReadOnly").trim();
      String mandatory = data.get("Mandatory").trim();
      String defaultsto = data.get("DefaultsTo").trim();
      
      //Checking if visible or not 
      String locator = "//label[text()="+"\"" + label +"\"" +"]";
      System.out.println("Label is " + locator);
      Thread.sleep(1000);
      
      Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
      String Visible = isPresent.toString();
      int flag = 0;
      int size = driver.findElements(By.xpath(locator)).size();
      for (int i = 1; i <= size; i++) {
             if(isPresent == true){
         String valueParentName = "("+locator +")["+i+"]"+ "//parent::div";
         WebElement element1 = driver.findElement(By.xpath(valueParentName));
         //Thread.sleep(2000);
         String getClassAttribute = element1.getAttribute("class");
         if(getClassAttribute.contains("row hidden")){
             Visible = "false";
             //Assert.assertEquals(true, element);
             continue;
         } else{
             Visible = "true";
             break;
         }
        }
      }
      
      System.out.println(label + " visible " + Visible);
      if(!visible.equalsIgnoreCase("Conditional")){
      Assert.assertEquals(Visible, visible);
      }
      // Checking if the label contains singlequote in it ?
      label = convertLabelIfContainsSingleQuote(label);
      // Checking field Type
      if (fieldType.equals("Numeric") || (fieldType.equals("Alphanumeric"))) {
             System.out.println("Field Type is numeric");
             continue;

      } else {
             if(label.equalsIgnoreCase("Previous Name(s)")){
             jsExecutor.executeScript("return PageObjects.find({caption:'Does the entity have a previous name(s)?'}).setValueByLookupText('Yes')");
             }
             Thread.sleep(1000);
             if(!fieldType.equalsIgnoreCase("NA")){
             //waitForDropDown(label);
                   if(label.equals("Override Risk Rating:")){
                        clickRiskButton();
                   }
                   
             String FieldType = jsExecutor
                          .executeScript("return PageObjects.find({caption:'" + label + "'}).type").toString();
             System.out.println("FieldType is  " + FieldType);
             Assert.assertEquals(FieldType, fieldType);
             }
      }
      
      // checking Read-Only 
      if (!readonly.equalsIgnoreCase("NA")) {
             String ReadOnly = jsExecutor
                          .executeScript("return PageObjects.find({caption:'" + label + "'}).readOnly").toString();
             System.out.println(label + " is Editable as " + ReadOnly);
             Assert.assertEquals(readonly, ReadOnly);
      }
      if (label.equals("Is Primary Contact")) {
             continue;
      } else {
             if (!mandatory.equals("NA")) {
                   String Mandatory = jsExecutor
                                 .executeScript("return PageObjects.find({caption:'" + label + "'}).mandatory").toString();
                   if (Mandatory == null) {
                          Assert.assertEquals(null, Mandatory);

                   } else {
                          Assert.assertEquals(mandatory, Mandatory);
                   }
             }
      }
      
      // checking Defaults to value
      if (defaultsto.equals("NA") || defaultsto.equals("Select...")) {
             continue;
      } else {
             String Value = jsExecutor.executeScript("return PageObjects.find({caption:'" + label + "'}).getValue()")
                          .toString();
             System.out.println(jsExecutor
                          .executeScript("return PageObjects.find({caption:'" + label + "'}).getValue").toString());
             System.out.println("Value is" + Value);
             if (defaultsto.equals("Individual")) {
                   Assert.assertEquals(Value, "709006");
             } else if (defaultsto.equals("Branch of a Foreign Company")) {
                   Assert.assertEquals(Value, "700267");
             } else if (defaultsto.equals("Non-Resident")) {
                   Assert.assertEquals(Value, "700291");
             } else if (defaultsto.equals("Regular Review")) {
                   Assert.assertEquals(Value, "18002");
             }

             // String value = "//div[text()='" + defaultsto + "']";
             // System.out.println("//div[text()='" + defaultsto + "']");
             // Boolean IsPresent = driver.findElements(By.xpath(value)).size() > 0;
             // System.out.println("value of present is" + IsPresent);
             // String DefaultsTo = driver.findElement(By.xpath(value)).getText().toString();
             // System.out.println("Value of defaults to" + DefaultsTo);
             // Assert.assertEquals(defaultsto, DefaultsTo);

      		}

    	}
    }

	@Then("^I can see \"([^\"]*)\" field is visible$")
	public void i_can_see_field_is_visible(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			Boolean visible = driver.findElements(By.xpath(locator)).size() > 0;
			Assert.assertEquals("true", visible);

		}

	}

	@And("^I check that below data is available$")
	public void i_check_that_below_data_is_available(DataTable table) throws Throwable {
		List<Map<String, String>> list = table.asMaps(String.class, String.class);
		for (int i = 0; i < list.size(); i++) {
			try {
				String label = list.get(i).get("Label").trim();
				String value = list.get(i).get("Value").trim();
				if (value.equals("Resident")) {
					String Getvalue = jsExecutor
							.executeScript("return PageObjects.find({caption:'" + label + "'}).getValue()").toString();
					System.out.println("Value is  " + Getvalue);
					Assert.assertEquals("700290", Getvalue);
				} else if (value.equals("Non-Resident")) {
					String Getvalue = jsExecutor
							.executeScript("return PageObjects.find({caption:'" + label + "'}).getValue()").toString();
					System.out.println("Value is  " + Getvalue);
					Assert.assertEquals("700291", Getvalue);

				}
			} catch (Exception e) {

			}

		}

	}

	@And("^I navigate to \"([^\"]*)\" screen by clicking on \"([^\"]*)\" button from \"([^\"]*)\"$")
	public void i_navigate_to_something_screen_by_clicking_on_something_button(String strArg1, String strArg2,
			String strArg3) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query = "Select * from Controls1 where ObjectKey='" + strArg2 + "' and Name ='" + strArg1
				+ "' and Screen='" + strArg3 + "'";
		System.out.println("Select * from Controls1 where ObjectKey='" + strArg2 + "' and Name ='" + strArg1
				+ "' and Screen='" + strArg3 + "'");
		ResultSet rst = sql.GetDataSet(query);
		Thread.sleep(3000);
		while (rst.next()) {
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			driver.findElement(By.xpath(locator)).click();
			Thread.sleep(5000);
		}
	}

	@And("^I select \"([^\"]*)\" for \"([^\"]*)\" field \"([^\"]*)\"$")
	public void i_select_something_for_something_field(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		Thread.sleep(2000);
		switch (strArg2) {
		case "Dropdown":
			jsExecutor.executeScript(
					"PageObjects.find({caption:'" + strArg3 + "'}).setValueByLookupText('" + strArg1 + "')");
			// System.out.println( jsExecutor.executeScript(
			// "PageObjects.find({caption:'" + strArg3 + "'}).setValueByLookupText('" +
			// strArg1 + "')"));
			Thread.sleep(2000);
			break;
		case "TextBox":
			
			System.out.println("Type of parameter is" +strArg1.getClass().getName());
			if (strArg1.equals("255")) {
				String alphaNumericData = GenericStepDefinition.getAlphaNumericString(255);
				jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).clear()");
				jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).setValue('" + alphaNumericData + "')");
				
			}
			else if (strArg1.equals("36")) {
				String alphaNumericData = GenericStepDefinition.getAlphaNumericString(36);
				//jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).clear()");
				jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).setValue('" + alphaNumericData + "')");
				
			}
			else {
			//jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).clear()");
			jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).setValue('" + strArg1 + "')");
			}
			break;
        case "DatePicker":
            //Send Date in format of "YYYY-MM-DD"
            jsExecutor.executeScript("PageObjects.find({caption:'" + strArg3 + "'}).setValue('" + strArg1 + "')");
            break;
        case "MultiSelectDropdown":
        	System.out.println("Type of parameter is" +strArg1.getClass().getName());
            
                  jsExecutor.executeScript(
                                "PageObjects.find({caption:'" + strArg3 + "'}).setValueByLookupText(['" + strArg1 + "'])");
                  Thread.sleep(2000);
            
break;
            
}

		}

	

	@And("^I complete \"([^\"]*)\" with Key \"([^\"]*)\" and below data$")
	public void i_complete_capturerequestchanges_with_keyand_below_data(String Tablename, String Datakey,
			DataTable table) throws Throwable {

		try {
			List<Map<String, String>> list = table.asMaps(String.class, String.class);
			for (int i = 0; i < list.size(); i++) {
				Thread.sleep(4000);
				SqliteConnection sql = SqliteConnection.getInstance();
				ResultSet rst2 = sql.buildQuery(Tablename, Datakey);
				Thread.sleep(3000);
				FillInDataWithoutValidation(rst2);
			String Product = list.get(i).get("Product");
//				System.out.println("Product is" + Product);
				String Relationship = list.get(i).get("Relationship");
				ResultSet rst = sql.buildQuery("Product", Product);
				FillInDataWithoutValidation(rst);
				Thread.sleep(5000);
				ResultSet rst1 = sql.buildQuery("Relationship", Relationship);
				Thread.sleep(7000);
				FillInDataWithoutValidation(rst1);

			}
		} catch (Exception e) {
			assertTrue(e.getLocalizedMessage(), false);
		}

	}

	@And("^I take the screenshot for \"([^\"]*)\"$")
	public void i_take_the_screenshot(String filename) throws Throwable {
		String location = "src\\test\\resources\\";
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String timestamp = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File dest = new File(location + filename + timestamp + ".jpg");
		FileUtils.copyFile(src, dest);

	}

	public void pressKeysByActionsClass() {
		Actions actions = new Actions(driver);
		actions.keyDown(Keys.CONTROL);
		actions.sendKeys(Keys.HOME);
		actions.keyUp(Keys.CONTROL);
		actions.build().perform();
	}

	@When("^I click on \"([^\"]*)\" button and sort by \"([^\"]*)\"$")
	public void i_click_on_button_and_sort_by(String arg1, String arg2) throws Throwable {
		Thread.sleep(3000);

		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", arg1);
		String locator = rst.getString("Locator");
		driver.findElement(By.xpath(locator)).click();

		Thread.sleep(3000);
		String xpathSortOption = "(//*[text()='" + arg2 + "'])[1]";
		System.out.println(xpathSortOption);
		driver.findElement(By.xpath(xpathSortOption)).click();
		Thread.sleep(5000);
	}

	@When("^I add a document for a requirement and verify status before upload is \"([^\"]*)\" and after upload is \"([^\"]*)\"$")
	public void i_add_a_document_for_a_requirement_and_verify_status_before_upload_is_and_after_upload_is(String arg1,
			String arg2) throws Throwable {

		String statusBeforeUpload = driver.findElement(By.xpath("(//span[@class='fen-dataGrid-cell '])[2]")).getText();
		Assert.assertEquals(statusBeforeUpload, arg1);
		driver.findElement(By.xpath("(//span[@class='icon fen-icon-ellipsis undefined'])[1]")).click();
		Thread.sleep(2000);
		// driver.findElement(By.xpath("(//span[@class='fen-caption'])[1]")).click();
		Thread.sleep(5000);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst1 = sql.buildQuery("Document", "C1");
		FillInDataWithoutValidation(rst1);
		Thread.sleep(7000);
		String statusAfterUpload = driver.findElement(By.xpath("(//span[@class='fen-dataGrid-cell '])[2]")).getText();
		Assert.assertEquals(statusAfterUpload, arg2);
	}

	@When("^I complete \"([^\"]*)\" in \"([^\"]*)\" screen$")
    public void i_complete_something_in_something_screen(String strArg1, String strArg2) throws Throwable {
        SqliteConnection sql = new SqliteConnection();
        SqliteConnection.getInstance();
        String getDataQuery = sql.queryForGetDataFromControlsByObjectkeyAndScreen(strArg1, strArg2);
        ResultSet rst = sql.GetDataSet(getDataQuery);
        Thread.sleep(3000);
        while (rst.next()) {
               String name = rst.getString("Name");
               String value = rst.getString("Value");
               String Type = rst.getString("Type");
               String locator = rst.getString("Locator");

               switch (Type) {
               case "button":
                      waitExplicityForVisibility(By.xpath(locator));
                      Thread.sleep(1000);
                      driver.findElement(By.xpath(locator)).click();
                      Thread.sleep(2000);

                      break;

               case "Dropdown":
                      Thread.sleep(2000);
                      if (value != null) {
                            jsExecutor.executeScript(
                                          "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                      }
                      Thread.sleep(2000);
                      break;
               case "TextBox":
                      if ((value != null) && !(name.equals("Upload"))) {
                            // jsExecutor.executeScript("return PageObjects.find({caption:'" + name +
                            // "'}).clear()");
                            Thread.sleep(1000);
                             waitExplicityForVisibility(By.xpath(locator));
                             driver.findElement(By.xpath(locator)).sendKeys(value);
                            Thread.sleep(1000);
                      } else if (name.equals("IdentificationNumber")) {
                             waitExplicityForVisibility(By.xpath(locator));
                             driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(5));
                            Thread.sleep(3000);

                      } else if (name.equals("Upload")) {
                            Thread.sleep(3000);
                            String documentPath = System.getProperty("user.dir") + "\\" + value;
                             driver.findElement(By.xpath(locator)).sendKeys(documentPath);
                            Thread.sleep(1000);
                      }
                      break;
               case "DatePicker":
                      if (value != null) {

                             jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
                            Thread.sleep(2000);
                      }

               case "MultiSelectDropdown":
                      if (value != null) {
                            jsExecutor.executeScript(
                                          "PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
                            Thread.sleep(2000);
                      }

               }
        }
        Thread.sleep(7000);

	}
	
    @Then("^I verify that workflow name is \"([^\"]*)\" in \"([^\"]*)\" task$")
 public void i_verify_that_workflow_name_is_something_in_something_task(String strArg1, String strArg2) throws Throwable {
     
          Thread.sleep(6000);
     SqliteConnection sql = new SqliteConnection();
          SqliteConnection.getInstance();
          ResultSet rst  = sql.getControlsDataByName("LiteKYCWorkflowName");
          String caseDetailsXpath = rst.getString("Locator");
          waitExplicityForVisibility(By.xpath(caseDetailsXpath));
          String actualNameofWorkflow = driver.findElement(By.xpath(caseDetailsXpath)).getText();
          System.out.println("Expected Workflow Name is ' "+strArg1+" ' and Actual Name of Workflow is ' "+actualNameofWorkflow+" '");
          Assert.assertEquals(strArg1, actualNameofWorkflow);
          Thread.sleep(2000);
 }
    
    @Then("^I am able to upload, unlink and link following documents$")
 public void i_am_able_to_upload_unlink_and_link_following_documents(DataTable table) throws Throwable {
     Thread.sleep(1000);
     List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
     for (Map<String, String> data : inputList) {
         Thread.sleep(3000);
         String docName = data.get("Document Name");
         String xpathForDocumentExpand = GenericStepDefinition.getXpathForDocExpandByDocName(docName);
         String xpathForActions = GenericStepDefinition.getXpathForActionsByDocName(docName);
         Thread.sleep(1000);
         String getLinkedStatus = GenericStepDefinition.getXpathForLinkedStatusByDocName(docName);
         String linkedStatus2 = driver.findElement(By.xpath(getLinkedStatus)).getText();
         System.out.println("Currently status of Document linkage is : "+linkedStatus2);
         driver.findElement(By.xpath(xpathForActions)).click();
         Thread.sleep(1000);
         driver.findElement(By.xpath("//span[text()='Attach Document']")).click();
         waitExplicityForVisibility(By.xpath("//input[@name='Document_DocumentIdentificationNumber']"));
         Thread.sleep(1000);
         completeDocumentUpload();
         
         driver.findElement(By.xpath(xpathForDocumentExpand)).click();
         
         String xpathForUnlinkEllipsis = GenericStepDefinition.getXpathForUnlinkEllipsisByDocName(docName);
         waitExplicityForDropdownVisibility(By.xpath(xpathForUnlinkEllipsis));
         Thread.sleep(2000);
         driver.findElement(By.xpath(xpathForUnlinkEllipsis)).click();
         Thread.sleep(1000);
         driver.findElement(By.xpath("//span[text()='Unlink']")).click();
         Thread.sleep(2000);
         String linkedStatus = driver.findElement(By.xpath(getLinkedStatus)).getText();
         System.out.println("Currently status of Document linkage is : "+linkedStatus);
         driver.findElement(By.xpath(xpathForDocumentExpand)).click();
         
         String xpathForLinkEllipsis = GenericStepDefinition.getXpathForLinkEllipsisByDocName(docName);
         waitExplicityForDropdownVisibility(By.xpath(xpathForLinkEllipsis));
         Thread.sleep(2000);
         driver.findElement(By.xpath(xpathForLinkEllipsis)).click();
         Thread.sleep(1000);
         driver.findElement(By.xpath("//span[text()='Link']")).click();
         Thread.sleep(2000);
         driver.findElement(By.xpath(xpathForDocumentExpand)).click();
         Thread.sleep(3000);
         String linkedStatus1 = driver.findElement(By.xpath(getLinkedStatus)).getText();
         System.out.println("Currently status of Document linkage is : "+linkedStatus1);
     }
 }
    
    public void completeDocumentUpload() throws SQLException, InterruptedException{
          SqliteConnection sql = new SqliteConnection();
          SqliteConnection.getInstance();
          String query = sql.queryForGetDataFromControlsByObjectkeyAndScreen("DocumentUpload", "Document Details");
          ResultSet rst = sql.GetDataSet(query);
          fillDatafromControlsTable(rst);
          Thread.sleep(7000);
    }
    
    public void fillDatafromControlsTable(ResultSet rst) throws SQLException, InterruptedException{
          
          while(rst.next()){

                 String name = rst.getString("Name");
                 String value = rst.getString("Value");
                 String Type = rst.getString("Type");
                 String locator = rst.getString("Locator");

                 switch (Type) {
                 case "button":
                        waitExplicityForVisibility(By.xpath(locator));
                        Thread.sleep(1000);
                        driver.findElement(By.xpath(locator)).click();
                        Thread.sleep(2000);

                        break;

                 case "Dropdown":
                        Thread.sleep(2000);
                        if (value != null) {
                              jsExecutor.executeScript(
                                            "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                        }
                        Thread.sleep(2000);
                        break;
                 case "TextBox":
                        if ((value != null) && !(name.equals("Upload"))) {
                              // jsExecutor.executeScript("return PageObjects.find({caption:'" + name +
                              // "'}).clear()");
                              Thread.sleep(1000);
                               waitExplicityForVisibility(By.xpath(locator));
                               driver.findElement(By.xpath(locator)).sendKeys(value);
                              Thread.sleep(1000);
                        } else if (name.equals("IdentificationNumber")) {
                               waitExplicityForVisibility(By.xpath(locator));
                               driver.findElement(By.xpath(locator)).sendKeys(GenericStepDefinition.getAlphaNumericString(5));
                              Thread.sleep(3000);

                        } else if (name.equals("Upload")) {
                              Thread.sleep(3000);
                              String documentPath = System.getProperty("user.dir") + "\\" + value;
                               driver.findElement(By.xpath(locator)).sendKeys(documentPath);
                              Thread.sleep(1000);
                        }
                        break;
                 case "DatePicker":
                        if (value != null) {

                               jsExecutor.executeScript("PageObjects.find({caption:'" + name + "'}).setValue('" + value + "')");
                              Thread.sleep(2000);
                        }

                 case "MultiSelectDropdown":
                        if (value != null) {
                              jsExecutor.executeScript(
                                            "PageObjects.find({caption:'" + name + "'}).setValueByLookupText(['" + value + "'])");
                              Thread.sleep(2000);
                        }

                 }
          
          }
          
    }

 

	
@And("^I assert below documents are present with following properties$")
    public void i_assert_below_documents_are_present_with_following_properties(DataTable table) throws Throwable {
                    
                    SqliteConnection sql = new SqliteConnection();
                    SqliteConnection.getInstance();
                    
                    ResultSet rst11 = sql.getControlsDataByName("SortDocuments");

                    String locatorForSortButton = rst11.getString("Locator");
                    waitExplicityForVisibility(By.xpath(locatorForSortButton));
                    driver.findElement(By.xpath(locatorForSortButton)).click();
                    Thread.sleep(1000);
                    
                    ResultSet rst2 = sql.getControlsDataByName("SortByMandatory");
                    String locatorForMandatory = rst2.getString("Locator");
                    waitExplicityForVisibility(By.xpath(locatorForMandatory));
                    driver.findElement(By.xpath(locatorForMandatory)).click();
                    Thread.sleep(3000);
                    
               List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
               for (Map<String, String> data : inputList) {
                   Thread.sleep(3000);
                   String docReqName = data.get("KYC Document Requirement");
                   String defaultDocType = data.get("Default Document Type");
                   String defaultDocCategory = data.get("Default Document Category");
                   String mandatory = data.get("Mandatory");
                   if(docReqName.length()>35){
                       docReqName = docReqName.substring(0, 31);
                   }
                   String docNameXpath = "//*[contains(text(),'" + docReqName + "')]";
                   boolean isPresent = driver.findElements(By.xpath(docNameXpath)).size() > 0;
                   Assert.assertTrue(isPresent);
                   System.out.println("Document found : "+docReqName);
                  
                   String xpathForMandatory = GenericStepDefinition.getXpathForMandatoryByDocName(docReqName);
                   String xpathForActions = GenericStepDefinition.getXpathForActionsByDocName(docReqName);
                   waitExplicityForVisibility(By.xpath(xpathForMandatory));
                   String mandatoryTextActual = driver.findElement(By.xpath(xpathForMandatory)).getText();
                   Assert.assertEquals(mandatory, mandatoryTextActual);
                   Thread.sleep(2000);
                   driver.findElement(By.xpath(xpathForActions)).click();
                   Thread.sleep(1000);
                   driver.findElement(By.xpath("//span[text()='Attach Document']")).click();
                   waitExplicityForVisibility(By.xpath("//input[@name='Document_DocumentIdentificationNumber']"));
                   Thread.sleep(2000);
                   String docCategoryActual = driver.findElement(By.xpath("(//div[contains(@class,'is-disabled')])[3]")).getText();
                   Thread.sleep(2000);
                   System.out.println("Expected Document Category is ' "+defaultDocCategory+" ' and Actual Doc Category is ' "+docCategoryActual+" '");
                   String docTypeActual = driver.findElement(By.xpath("(//div[contains(@class,'is-disabled')])[6]")).getText();
                   System.out.println("Expected Document Type is ' "+defaultDocType+" ' and Actual Doc Type is ' "+docTypeActual+" '");
                   Assert.assertEquals(defaultDocCategory, docCategoryActual);
                   Assert.assertEquals(defaultDocType, docTypeActual);
                   waitExplicityForDropdownVisibility(By.xpath("//span[text()='Cancel']"));
                   driver.findElement(By.xpath("//span[text()='Cancel']")).click();
                   Thread.sleep(3000);
                   
           }
               
               
    }

	@And("^I check that below subflow is visible$")
	public void i_check_that_below_subflow_is_visible(DataTable table) throws Throwable {
		String subflow;
		List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
		for (Map<String, String> data : inputList) {
			subflow = data.get("Subflow");
			String value = "//h3[text()='" + subflow + "']";
			System.out.println(value);
			waitExplicityForVisibility(By.xpath(value));
			Boolean element = driver.findElements(By.xpath(value)).size() > 0;
			Assert.assertEquals(true, element);
			Thread.sleep(2000);

		}
	}

	@When("^I click on \"([^\"]*)\" from \"([^\"]*)\" to create \"([^\"]*)\"$")
	public void i_click_on_something_from_something_to_create_something(String strArg1, String strArg2, String strArg3)
			throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String getDataQuery = sql.queryForGetDataFromControlsByObjectKeyName(strArg2, strArg1);
		ResultSet rst = sql.GetDataSet(getDataQuery);
		String locator = rst.getString("Locator");
		waitExplicityForVisibility(By.xpath(locator));
		driver.findElement(By.xpath(locator)).click();
		String getDataQuery1 = sql.queryForGetDataFromControlsByObjectKeyName(strArg2, strArg3);
		System.out.println(getDataQuery1);
		ResultSet rst1 = sql.GetDataSet(getDataQuery1);
		String locator1 = rst1.getString("Locator");
		waitExplicityForVisibility(By.xpath(locator1));
		driver.findElement(By.xpath(locator1)).click();
	}

	@And("^I click on \"([^\"]*)\" button to take screenshot$")
	public void i_click_on_something_button_to_take_screenshot(String strArg1) throws Throwable {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", strArg1);
		String locator = rst.getString("Locator");
		waitExplicityForVisibility(By.xpath(locator));
		driver.findElement(By.xpath(locator)).click();
		Thread.sleep(2000);

		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='50%';");
		final byte[] scrShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		message.embed(scrShot, "image/png");
		((JavascriptExecutor) driver).executeScript("document.body.style.zoom='100%';");
	}

	@When("^I navigate to Advanced Search screen$")
	public void i_navigate_to_advanced_search_screen() throws SQLException, InterruptedException {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		ResultSet rst = sql.getControlsData("Controls1", "Advanced Search");
		while (rst.next()) {
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			driver.findElement(By.xpath(locator)).click();
			 Thread.sleep(3000);

			
		}
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}
	
	
	 public void waitForDropDown(String name) throws InterruptedException{
         try {
            
             if(name.contains("Is this a Prohibited client")){
                 Thread.sleep(2000);
             }
             else if(name.contains("Review Outcome")){
                 Thread.sleep(2000);
             }
             else if(name.contains("Address Type")){
                 Thread.sleep(2000);
             }
             else{
                 String xpathOfDropdownElement="//*[text()='"+name+"']";
                    waitExplicityForDropdownVisibility(By.xpath(xpathOfDropdownElement));
                    Thread.sleep(2000);
             }
            } catch (Exception e) {
                Thread.sleep(1000);
                System.out.println("Could not find dropdown with name : "+name);
            }
       
     }
    private void waitExplicityForDropdownVisibility(By by) {
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        //System.out.println("-----Used Wait for  Dropdown visibility-----=> "+ by.toString());
       
    }
	
    @When("^I fill \"([^\"]*)\" data of length \"([^\"]*)\" in \"([^\"]*)\" field$")
    public void i_fill_data_of_length_in_field(String strArg1, String strArg2, String strArg3) throws Throwable {
           
         int lengthOfString = Integer.parseInt(strArg2);
         SqliteConnection sql = new SqliteConnection();
         SqliteConnection.getInstance();
         Thread.sleep(2000);
         String queryForData = sql.queryForGetDataFromControlsByObjectkeyAndScreen(strArg3, "Review/Edit Client Data");
         ResultSet rst = sql.GetDataSet(queryForData);
         String locator = rst.getString("Locator");
        waitExplicityForVisibility(By.xpath(locator));
         driver.findElement(By.xpath(locator)).clear();
         switch (strArg1) {
        case "Alphanumeric":
             String alphaNumericData = GenericStepDefinition.getAlphaNumericString(lengthOfString);
             driver.findElement(By.xpath(locator)).sendKeys(alphaNumericData);
            break;
           
        case "Numeric":
             String numericData = GenericStepDefinition.getNumericString(lengthOfString);
            driver.findElement(By.xpath(locator)).sendKeys(numericData);
            break;   
        case "SpecialCharacters":
             String specialChars = GenericStepDefinition.getSpecialCharacterString(lengthOfString);
            driver.findElement(By.xpath(locator)).sendKeys(specialChars);
            break;   
        case "UpperCaseCharacters":
             String upperCaseChars = GenericStepDefinition.getSpecialCharacterString(lengthOfString);
            driver.findElement(By.xpath(locator)).sendKeys(upperCaseChars);
            break; 
        case "LowerCaseCharacters":
             String lowerCaseChars = GenericStepDefinition.getSpecialCharacterString(lengthOfString);
            driver.findElement(By.xpath(locator)).sendKeys(lowerCaseChars);
            break;     
        default:
            break;
        }
        }

	@When("^I enter \"([^\"]*)\" and \"([^\"]*)\" in \"([^\"]*)\" Screen$")
	public void i_enter_something_and_something_in_something_screen(String strArg1, String strArg2, String strArg3)
			throws SQLException, InterruptedException {
		String ID = scenariocontext.getValue("ID").toString();
		System.out.println("ID Stored is : " + ID);
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query1 = sql.queryForGetDataFromControlsByObjectkeyAndScreen(strArg1, strArg3);
		ResultSet rst1 = sql.GetDataSet(query1);
		String locator1 = rst1.getString("Locator");
		driver.findElement(By.xpath(locator1)).sendKeys(ID);
		String query2 = sql.queryForGetDataFromControlsByObjectkeyAndScreen(strArg2, strArg3);
		ResultSet rst2 = sql.GetDataSet(query2);
		String name2 = rst2.getString("Name");
		String locator2 = rst2.getString("Locator");
		String value2 = rst2.getString("Value");
        if(name2.equalsIgnoreCase("GLCMS UID")){
            value2 = (String) scenariocontext.getValue("GLCMSUID");
            System.out.println("Value of GLCMS UID is "+ value2);
     }

		driver.findElement(By.xpath(locator2)).sendKeys(value2);
		String query3 = sql.queryForGetDataFromControlsByObjectkeyAndScreen("Search", strArg3);
		ResultSet rst3 = sql.GetDataSet(query3);
		String locator3 = rst3.getString("Locator");
		driver.findElement(By.xpath(locator3)).click();
		Thread.sleep(3000);

	}

	@And("^I store the \"([^\"]*)\"$")
	public void i_store_the_something(String strArg1) throws InterruptedException {
		waitExplicityForVisibility(By.xpath("//h1[text()='Capture Request Details']"));
		if (strArg1.equalsIgnoreCase("Legal Entity ID")) {
			Thread.sleep(2000);
			String legalEntityID = driver.findElement(By.xpath("(//span[@class='read-only'])[1]")).getText();
			scenariocontext.setValue("ID", legalEntityID);
			System.out.println("Stored ID value as : " + legalEntityID);
		} else {
			scenariocontext.setValue("ID", strArg1);
		}
	}

	@And("^I validate the search result by clicking on it$")
	public void i_validate_the_search_result_by_clicking_on_it() throws SQLException, InterruptedException {
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
		String query1 = sql.queryForGetDataFromControlsByNameAndScreen("Search Result", "Advance Search");
		ResultSet rst1 = sql.GetDataSet(query1);
		String ID = scenariocontext.getValue("ID").toString();
		while (rst1.next()) {
			String objectKey = rst1.getString("ObjectKey");
			String locator = rst1.getString("Locator");
			if (objectKey.equalsIgnoreCase("Search Result ID")) {
				Thread.sleep(1000);
				String text1 = driver.findElement(By.xpath(locator)).getText();
				Thread.sleep(2000);
				Assert.assertEquals(ID, text1);
			} else {
				String text2 = driver.findElement(By.xpath(locator)).getText();
				Thread.sleep(2000);
				Assert.assertEquals(text2, "123");
			}
		}
	}
    @Then("^I cancel the case$")
    public void i_cancel_the_case() throws Throwable {
        Thread.sleep(5000);
        SqliteConnection sql = new SqliteConnection();
             SqliteConnection.getInstance();
             ResultSet rst = sql.getControlsData("Controls1", "CancelCase");
             fillDatafromControlsTable(rst);
             Thread.sleep(5000);
             ResultSet rst1 = sql.getControlsData("Controls1", "CancelCaseReason");
             fillDatafromControlsTable(rst1);
             Thread.sleep(5000);
             ResultSet rst2 = sql.getControlsData("Controls1", "CancellationCheckList");
             fillDatafromControlsTable(rst2);
             Thread.sleep(5000);
             WebElement element = driver.findElement(By.xpath("//div[@class='special-case-message']"));
             String caseStatus = element.getText();
             Assert.assertEquals("CANCELLED", caseStatus);
    }
    @And("^I do \"([^\"]*)\" for all pending documents$")
    public void i_do_something_for_all_pending_documents(String strArg1) throws Throwable {

        SqliteConnection sql = new SqliteConnection();
        SqliteConnection.getInstance();
        int j = 1;
        Thread.sleep(5000);
        ResultSet rst11 = sql.getControlsDataByName("SortDocuments");

        String locatorForSortButton = rst11.getString("Locator");
        waitExplicityForVisibility(By.xpath(locatorForSortButton));
        driver.findElement(By.xpath(locatorForSortButton)).click();
        Thread.sleep(1000);
        
        ResultSet rst2 = sql.getControlsDataByName("SortByMandatory");
        String locatorForMandatory = rst2.getString("Locator");
        waitExplicityForVisibility(By.xpath(locatorForMandatory));
        driver.findElement(By.xpath(locatorForMandatory)).click();
        Thread.sleep(3000);
        String getDataQuery = sql.queryForGetDataFromControlsByObjectKeyName(strArg1, "ActionButton");
        ResultSet rst = sql.GetDataSet(getDataQuery);
        while (rst.next()) {
               Thread.sleep(7000);
               String locator = rst.getString("Locator");
               waitExplicityForVisibility(By.xpath(locator));
               int Count = driver.findElements(By.xpath(locator)).size();
               System.out.println("Count of the number of element is" + Count);
               if (Count == 1) {
                     Count = 2;
               }
               for (int i = 4; i <= Count * 2; i = i + 4) {
                     
                     String Locator = "(//span[@class='fen-dataGrid-cell '])[" + i + "]";
                     waitExplicityForVisibility(By.xpath(Locator));
                     Thread.sleep(2000);
                     System.out.println("Value of locator is" + Locator);
                     String Mandatory = driver.findElement(By.xpath(Locator)).getText();
                     System.out.println("value of mandatory is" + Mandatory);
                     String statusOfDocument = getDocumentStatus(i);
                     if((Mandatory == null)||(Mandatory.equals(""))){
                                   System.out.println("value of Mandatory was read null, so waiting little longer.");
                                   Thread.sleep(5000);                           
                                   Mandatory = driver.findElement(By.xpath(Locator)).getText();
                                   System.out.println("Now the value of mandatory is " + Mandatory);
                            }
                            
                            waitIfEnvIsFAB(3);
                     System.out.println("Document upload status is : "+statusOfDocument);
                     if ((Mandatory.equals("Yes"))&& (statusOfDocument.equals("Pending"))) {
                            waitIfEnvIsFAB(4);
                            String Action = "(//button[@class='f-context-menu__button  icon-button '])[" + j + "]";
                            System.out.println("(//button[@class='f-context-menu__button  icon-button '])[" + j + "]");
                            waitExplicityForVisibility(By.xpath(Action));
                            driver.findElement(By.xpath(Action)).click();
                            ResultSet rst1 = sql.buildQuery("Document", "C1");
                            FillInDataWithoutValidation(rst1);
                            Thread.sleep(5000);

                     }
                     j = j + 1;
                     
               }

        }

 
}

 public String getDocumentStatus(int xpathNumber){
      
       int statusXpathNum = xpathNumber-2;
      String status = driver.findElement(By.xpath("(//span[@class='fen-dataGrid-cell '])["+statusXpathNum+"]")).getText();
      return status;
}
 @Then("^I see \"([^\"]*)\" task is generated$")
 public void i_see_task_is_generated(String strArg1) throws Throwable {
  Thread.sleep(7000);   
    SqliteConnection sql = new SqliteConnection();
     SqliteConnection.getInstance();
     ResultSet rst = sql.getControlsData("Controls1", strArg1);
     while (rst.next()) {
         String locator = rst.getString("Locator");
         Thread.sleep(3000);
         Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
         Assert.assertEquals(true, isPresent);

     }
     
 }

@And("^I validate \"([^\"]*)\" task is Assigned to \"([^\"]*)\"$")
 public void i_validate_something_task_is_assigned_to_something(String strArg1, String strArg2) throws Throwable {
   Thread.sleep(4000);
     String xpathForAssignedTeam = GenericStepDefinition.getXpathForAssignedTeamByTaskName(strArg1);
     String getAssignedTeamName = driver.findElement(By.xpath(xpathForAssignedTeam)).getText();
     Assert.assertEquals(strArg2, getAssignedTeamName);
     Thread.sleep(2000);
 }

@When("^I validate below context value under options button for task \"([^\"]*)\"$")
 public void i_validate_below_context_value_under_options_button_for_task_something(String strArg1, DataTable table) throws Throwable {
   Thread.sleep(4000);
   String xpathOfActionsButton = GenericStepDefinition.getXpathForActionsButtonByTaskName(strArg1);
   driver.findElement(By.xpath(xpathOfActionsButton)).click();
   List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
     for (Map<String, String> data : inputList) {
         Thread.sleep(3000);
         String labelName = data.get("Label");
         String xpathOfLabel = "//div[text()='"+labelName+"']" ;
         boolean isLabelPresent = driver.findElements(By.xpath(xpathOfLabel)).size() > 0;
         Assert.assertTrue(isLabelPresent);
     }
 }

@And("^I validate status of task \"([^\"]*)\" task is \"([^\"]*)\"$")
 public void i_validate_status_of_task_something_task_is_something(String strArg1, String strArg2) throws Throwable {
   Thread.sleep(4000);
   String xpathOfStatus = GenericStepDefinition.getXpathForStatusByTaskName(strArg1);
   String getStatusOfTask = driver.findElement(By.xpath(xpathOfStatus)).getText();
   System.out.println("Status of the task is :"+strArg2);
   if(getStatusOfTask.equals(strArg2)){
          Assert.assertEquals(strArg2, getStatusOfTask);
   }
   else{
          //Here I go to task and click on Generate RESEND button to get GLCMSUID
          //Code is not written becoz this UI is not completed.
          
    }
   
 }
@When("^I click on \"([^\"]*)\" option displaying under options button for \"([^\"]*)\" task$")
 public void i_click_on_something_option_displaying_under_options_button_for_something_task(String strArg1, String strArg2) throws Throwable {
   Thread.sleep(4000);
   String xpathOfActionsButton = GenericStepDefinition.getXpathForActionsButtonByTaskName(strArg1);
   driver.findElement(By.xpath(xpathOfActionsButton)).click();
   String xpathOfNavigateButton = "//div[text()='"+strArg1+"']" ;
   driver.findElement(By.xpath(xpathOfNavigateButton)).click();
   
    
    
 }

@Then("^I verify that \"([^\"]*)\" task screen is displayed$")
 public void i_verify_that_something_task_screen_is_displayed(String strArg1) throws Throwable {
   Thread.sleep(4000);
   String xpath = "//div//h1[text()='"+strArg1+"']";
   boolean isPageNameCorrect = driver.findElements(By.xpath(xpath)).size() > 0;
Assert.assertTrue(isPageNameCorrect);
 
}

@And("^I validate following nature of field \"([^\"]*)\"$")
 public void i_validate_following_nature_of_field_something(String strArg1, DataTable table) throws Throwable {
   Thread.sleep(2000);
   List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
     for (Map<String, String> data : inputList) {
         Thread.sleep(1000);
         String isChecked = data.get("Is Ultimate Beneficial Owner Checked");
         boolean isActuallyTicked = driver.findElements(By.xpath("//label[@class='ticked']")).size() > 0;
         System.out.println(isActuallyTicked);
         if (isChecked.equals("Yes")) {
                       Assert.assertTrue(isActuallyTicked);
                 }
         else{
         Assert.assertFalse(isActuallyTicked);
         }
         
     }
   
}


@Then("^I validate that added association is displaying with a UBO tag in \"([^\"]*)\" task$")
 public void i_validate_that_added_association_is_displaying_with_a_ubo_tag_in_something_task(String strArg1) throws Throwable {
   Thread.sleep(4000);
   String xpath = "//span[@class='fen-caption' and text()='UBO']";
   boolean isUBOTagPresent = driver.findElements(By.xpath(xpath)).size() > 0;
   Assert.assertTrue(isUBOTagPresent);
   
}

@When("^I validate that added association is not displaying with a UBO tag in \"([^\"]*)\" task$")
 public void i_validate_that_added_association_isnot_displaying_with_a_ubo_tag_in_something_task(String strArg1) throws Throwable {
   Thread.sleep(4000);
   String xpath = "//span[@class='fen-caption' and text()='UBO']";
   boolean isUBOTagPresent = driver.findElements(By.xpath(xpath)).size() == 0;
   Assert.assertTrue(isUBOTagPresent);
   
}
@Then("^I validate \"([^\"]*)\" after clicking on \"([^\"]*)\" button of \"([^\"]*)\" screen$")
public void i_validate_something_after_clicking_on_something_button_of_something_screen(String strArg1, String strArg2, String strArg3) throws Throwable {
Thread.sleep(4000);
       SqliteConnection sql = new SqliteConnection();
       SqliteConnection.getInstance();
       String query = sql.queryForGetDataFromControlsByObjectKeyNameScreen(strArg1, strArg2, strArg3);
       ResultSet rst = sql.GetDataSet(query);
       String locator = rst.getString("Locator");
       boolean isErrorMessagePresent = driver.findElements(By.xpath(locator)).size() > 0;
       Assert.assertTrue(isErrorMessagePresent);
}

@And("^I complete \"([^\"]*)\" task from Actions button$")
public void i_complete_actions_button(String strArg1) throws Throwable {
    String xpathOfActionsButton = GenericStepDefinition.getXpathForActionsButtonByTaskName(strArg1);
    Thread.sleep(5000);
    driver.findElement(By.xpath(xpathOfActionsButton)).click(); 
    Thread.sleep(1000);
    WebElement element = driver.findElement(By.xpath("//span[text()='Complete']"));	
	jsExecutor.executeScript("arguments[0].scrollIntoView();", element);
	Thread.sleep(1000);
    driver.findElement(By.xpath("//span[text()='Complete']")).click();
    Thread.sleep(4000);
}

@And("^I validate the following fields in \"([^\"]*)\" Sub Flow with dataKey$")
public void i_validate_the_following_fields_in_something_sub_flow_with_datakey(String strArg1, DataTable table) throws Throwable {
       Thread.sleep(6000);
       List<Map<String, String>> list = table.asMaps(String.class, String.class);
       JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
       Thread.sleep(2000);
       for (Map<String, String> data : list) {
         Thread.sleep(2000);
         String label = data.get("Label").trim();
         System.out.println("Label name is" + label);
         String fieldType = data.get("FieldType").trim();
         String visible = data.get("Visible").trim();
         String readonly = data.get("ReadOnly").trim();
         String mandatory = data.get("Mandatory").trim();
         String defaultsto = data.get("DefaultsTo").trim();
         String dataKey = data.get("DataKey").trim();
         
         boolean dataKeyFlag = false;
         if(!dataKey.equals("NA")){
               dataKeyFlag=true;
         }
         
         //Checking if visible or not 
         String locator = "//*[text()="+"\"" + label +"\"" +"]";
         System.out.println("Label is " + locator);
         Thread.sleep(1000);
         
         Boolean isPresent = driver.findElements(By.xpath(locator)).size() > 0;
         String Visible = isPresent.toString();
         int size = driver.findElements(By.xpath(locator)).size();
         for (int i = 1; i <= size; i++) {
                if(isPresent == true){
            String valueParentName = "("+locator +")["+i+"]"+ "//parent::div";
            WebElement element1 = driver.findElement(By.xpath(valueParentName));
            //Thread.sleep(2000);
            String getClassAttribute = element1.getAttribute("class");
            if(getClassAttribute.contains("row hidden")){
                Visible = "false";
                //Assert.assertEquals(true, element);
                continue;
            } else{
                Visible = "true";
                break;
            }
           }
         }
         
         System.out.println("Expected value of Label Visibility is : " + visible);
         System.out.println("Actual value of Label Visibility is : " + Visible);
         if(!visible.equalsIgnoreCase("Conditional")){
         Assert.assertEquals(visible, Visible);
         }
         
         // Checking if the label contains SingleQuote in it ?
         label = convertLabelIfContainsSingleQuote(label);
         // Checking field Type
         if (fieldType.equals("Numeric") || (fieldType.equals("Alphanumeric"))) {
                System.out.println("Field Type is numeric");
                continue;
       
         } else {
                if(label.equalsIgnoreCase("Previous Name(s)")){
                jsExecutor.executeScript("return PageObjects.find({caption:'Does the entity have a previous name(s)?'}).setValueByLookupText('Yes')");
                }
                Thread.sleep(1000);
                if(!fieldType.equalsIgnoreCase("NA")){
                //waitForDropDown(label);
                    if(label.equals("Override Risk Rating:")){
                           clickRiskButton();
                    }
                    if(dataKeyFlag){
                    String FieldType = jsExecutor
                                   .executeScript("return PageObjects.find({dataKey:'" + dataKey + "'}).type").toString();
                      System.out.println("Actual FieldType is of dataKey ->'"+dataKey+ "' is : " + FieldType);
                      System.out.println("Expected FieldType is  " + fieldType);
                      Assert.assertEquals(fieldType, FieldType);
                    }else{
                      String FieldType = jsExecutor
                                   .executeScript("return PageObjects.find({caption:'" + label + "'}).type").toString();
                      System.out.println("Actual FieldType is of label ->'"+label+ " is : " + FieldType);
                      System.out.println("Expected FieldType is  " + fieldType);
                      Assert.assertEquals(FieldType, fieldType);
                    }
                }
         }
         
         // checking Read-Only 
         if (!readonly.equalsIgnoreCase("NA")) {
               if(dataKeyFlag){
                      String ReadOnly = jsExecutor
                      .executeScript("return PageObjects.find({dataKey:'" + dataKey + "'}).readOnly").toString();
         System.out.println("DataKey->' "+dataKey + " ' readOnly status is : " + ReadOnly);
         Assert.assertEquals(readonly, ReadOnly);
               }else{
                String ReadOnly = jsExecutor
                             .executeScript("return PageObjects.find({caption:'" + label + "'}).readOnly").toString();
                System.out.println("' "+label + " ' readOnly status is : " + ReadOnly);
                Assert.assertEquals(readonly, ReadOnly);
               }
         }
         if (label.equals("Is Primary Contact")) {
                continue;
         } else {
                if (!mandatory.equals("NA")) {
                     String Mandatory;
                     if(dataKeyFlag){
                            Mandatory = jsExecutor
                             .executeScript("return PageObjects.find({dataKey:'" + dataKey + "'}).mandatory").toString();
                     } else{
                      Mandatory = jsExecutor
                                    .executeScript("return PageObjects.find({caption:'" + label + "'}).mandatory").toString();
                     }
                      if (Mandatory == null) {
                             Assert.assertEquals(null, Mandatory);
       
                      } else {
                             Assert.assertEquals(mandatory, Mandatory);
                      }
                }
         }
         
         // checking Defaults to value
         if (defaultsto.equals("NA") || defaultsto.equals("Select...")) {
                continue;
         } else {
                String Value = jsExecutor.executeScript("return PageObjects.find({caption:'" + label + "'}).getValue()")
                             .toString();
                System.out.println(jsExecutor
                             .executeScript("return PageObjects.find({caption:'" + label + "'}).getValue").toString());
                System.out.println("Value is" + Value);
                if (defaultsto.equals("Individual")) {
                      Assert.assertEquals(Value, "709006");
                } else if (defaultsto.equals("Branch of a Foreign Company")) {
                      Assert.assertEquals(Value, "700267");
                } else if (defaultsto.equals("Non-Resident")) {
                      Assert.assertEquals(Value, "700291");
                } else if (defaultsto.equals("Regular Review")) {
                      Assert.assertEquals(Value, "18002");
                }
       
                // String value = "//div[text()='" + defaultsto + "']";
                // System.out.println("//div[text()='" + defaultsto + "']");
                // Boolean IsPresent = driver.findElements(By.xpath(value)).size() > 0;
                // System.out.println("value of present is" + IsPresent);
                // String DefaultsTo = driver.findElement(By.xpath(value)).getText().toString();
                // System.out.println("Value of defaults to" + DefaultsTo);
                // Assert.assertEquals(defaultsto, DefaultsTo);
       
                    }
       
             }
}




@When("^I assign the task \"([^\"]*)\" to role group \"([^\"]*)\" and user name \"([^\"]*)\"$")
public void i_assign_the_task_something_to_role_group_something_and_user_name_something(String strArg1, String strArg2, String strArg3) throws Throwable {
    Thread.sleep(3000);
    String getXpathforActions = GenericStepDefinition.getXpathForActionsButtonByTaskName(strArg1);
    driver.findElement(By.xpath(getXpathforActions)).click();
    waitExplicityForVisibility(By.xpath("//*[text()='Edit Task']"));
    Thread.sleep(1000);
    driver.findElement(By.xpath("//*[text()='Edit Task']")).click();
    waitForDropDown("Business Unit");
    Thread.sleep(3000);
    jsExecutor.executeScript(
                    "PageObjects.find({caption:'Business Unit'}).setValueByLookupText('CIB')");
    Thread.sleep(1000);
    System.out.println("PageObjects.find({caption:'Role Group'}).setValueByLookupText('" + strArg2 + "')");
    jsExecutor.executeScript(
                    "PageObjects.find({caption:'Role Group'}).setValueByLookupText('" + strArg2 + "')");
    String newstrArg3 = "LastName, "+strArg3;
    Thread.sleep(1000);
    System.out.println("PageObjects.find({caption:'User Name'}).setValueByLookupText('" + newstrArg3 + "')");
    jsExecutor.executeScript(
                    "PageObjects.find({caption:'User Name'}).setValueByLookupText('" + strArg3 + "')");
    driver.findElement(By.xpath("//span[text()='Save']")).click();
    Thread.sleep(3000);
       }

@And("^I validate that the RiskCategory is \"([^\"]*)\"$")
public void i_validate_the_riskcategory(String strArg1) throws Throwable {
  
	 SqliteConnection sql = new SqliteConnection();
     SqliteConnection.getInstance();
      ResultSet rst = sql.getControlsData("Controls1","RiskCategory");
      Thread.sleep(5000);
      while (rst.next()) {
    	  String locator=rst.getString("Locator");
    	  String Riskvalue=driver.findElement(By.xpath(locator)).getText();
    	  System.out.println("Risk value is" +Riskvalue);
    	  Assert.assertEquals(strArg1, Riskvalue);
      }
}

@And("^I validate that the OverRideRiskCategory is \"([^\"]*)\"$")
public void i_validate_that_the_overrideriskcategory_(String strArg1) throws Throwable {
	 SqliteConnection sql = new SqliteConnection();
     SqliteConnection.getInstance();
      ResultSet rst = sql.getControlsData("Controls1","OverridenRiskCategory");
      Thread.sleep(5000);
      while (rst.next()) {
    	  String locator=rst.getString("Locator");
    	  String Riskvalue=driver.findElement(By.xpath(locator)).getText();
    	  System.out.println("Risk value is" +Riskvalue);
    	  Assert.assertEquals(strArg1, Riskvalue);
      }
}
@Then("^I override risk to \"([^\"]*)\"$")
public void i_override_risk_to_something(String strArg1) throws Throwable {
      SqliteConnection sql = new SqliteConnection();
      SqliteConnection.getInstance();
      String query1 = sql.queryForGetDataFromControlsByNameAndScreen("Risk Override Icon", "Complete Risk Assessment");
      System.out.println(query1);
      boolean isOverriddenRiskPresent = driver.findElements(By.xpath("//*[contains(text(),'Overridden from')]")).size() > 0;
      
      ResultSet rst1 = sql.GetDataSet(query1);
      Thread.sleep(3000);
      while (rst1.next()) {
             String locator = rst1.getString("Locator");
             if (isOverriddenRiskPresent){
                    locator = locator + "[2]";
             } else {
                    locator = locator + "[1]";
             }
             driver.findElement(By.xpath(locator)).click();
      }
      System.out.println("PageObjects.find({caption:'Override Risk Rating:'}).setValueByLookupText('" + strArg1 + "')");
      jsExecutor.executeScript(
         "PageObjects.find({caption:'Override Risk Rating:'}).setValueByLookupText('" + strArg1 + "')");
      
      Thread.sleep(1000);
}

@Then("^I verify the populated risk rating is \"([^\"]*)\"$")
public void i_verify_the_populated_risk_rating_is_something(String strArg1) throws Throwable {
 Thread.sleep(7000);
 //scenariocontext.setValue("OldRiskRating", strArg1);
 boolean isPresent = driver.findElements(By.xpath("//h3[text()='Risk Assessment']//following::span[text()='"+strArg1+"']")).size()>0;
 Assert.assertTrue(isPresent);
 System.out.println("Populated Risk Rating is : "+strArg1);
}

@Then("^I verify the overridden risk rating is \"([^\"]*)\"$")
public void i_verify_the_overridden_risk_rating_is_something(String strArg1) throws Throwable {
      Thread.sleep(2000);
      
 boolean isRiskRatingPresent = driver.findElements(By.xpath("//h3[text()='Risk Assessment']//following::span[text()='"+strArg1+"']")).size()>0;
 String overrideMsg = driver.findElement(By.xpath("//*[@class='f-inline-message__text']")).getText();
 String expectedString =  "Overridden from "+(String) scenariocontext.getValue("OldRiskRating");
 Assert.assertEquals(expectedString, overrideMsg);
 Assert.assertTrue(isRiskRatingPresent);
 System.out.println("Populated Risk Rating is : "+strArg1);
}

@When("^I select \"([^\"]*)\" for field \"([^\"]*)\"$")
public void i_select_something_for_field_something(String strArg1, String strArg2) throws Throwable {
 Thread.sleep(5000);
 switch (strArg2) {
      case "Area":
             System.out.println("PageObjects.find({caption:'"+strArg2+"'}).setValueByLookupText('" + strArg1 + "')");
        jsExecutor.executeScript(
                "PageObjects.find({caption:'"+strArg2+"'}).setValueByLookupText('" + strArg1 + "')");
        Thread.sleep(2000);
             break;
      case "LE Details Changes":
             System.out.println(
                          "PageObjects.find({caption:'" + strArg2 + "'}).setValueByLookupText(['" + strArg1 + "'])");
             jsExecutor.executeScript(
                          "PageObjects.find({caption:'" + strArg2 + "'}).setValueByLookupText(['" + strArg1 + "'])");
             driver.findElement(By.xpath("//textarea[@name='LEM_Reason']")).sendKeys("Test");
             Thread.sleep(2000);
             break; 

      default:
             break;
      }
 

 
}

@Then("^I check that below subflow is not visible$")
public void i_check_that_below_subflow_is_not_visible(DataTable table) throws Throwable {
      String subflow;
      List<Map<String, String>> inputList = table.asMaps(String.class, String.class);
      for (Map<String, String> data : inputList) {
             subflow = data.get("Subflow");
             String value = "//h3[text()='" + subflow + "']";
             System.out.println(value);
             boolean isElementPresent = driver.findElements(By.xpath(value)).size() > 0;
             if(isElementPresent){
                    String xpathOfHiddenClass = value+"//parent::div//ancestor::div[contains(@class,'hidden')]";
                    boolean isHiddenClassPresent = driver.findElements(By.xpath(xpathOfHiddenClass)).size() > 0;
                    if(isHiddenClassPresent){
                    isElementPresent = false;
                    }
                    Assert.assertFalse(isElementPresent);
             }else{
                    Assert.assertFalse(isElementPresent);
             }

      }
}

@And("^I fill \"([^\"]*)\" data of length \"([^\"]*)\" in \"([^\"]*)\" field of \"([^\"]*)\" screen in \"([^\"]*)\" workflow$")
    public void i_fill_something_data_of_length_something_in_something_field_of_something_screen_in_something_workflow(String strArg1, String strArg2, String strArg3, String strArg4, String strArg5) throws Throwable {
      int lengthOfString = Integer.parseInt(strArg2);
  SqliteConnection sql = new SqliteConnection();
  SqliteConnection.getInstance();
  Thread.sleep(2000);
  String queryForData = sql.queryForGetDataFromControlsByObjectKeyNameScreen(strArg3, strArg5, strArg4);
  ResultSet rst = sql.GetDataSet(queryForData);
  String locator = rst.getString("Locator");
  waitExplicityForVisibility(By.xpath(locator));
  driver.findElement(By.xpath(locator)).clear();
  switch (strArg1) {
 case "Alphanumeric":
      String alphaNumericData = GenericStepDefinition.getAlphaNumericString(lengthOfString);
      driver.findElement(By.xpath(locator)).sendKeys(alphaNumericData);
     break;
    
 case "Numeric":
      String numericData = GenericStepDefinition.getNumericString(lengthOfString);
     driver.findElement(By.xpath(locator)).sendKeys(numericData);
     break;   
 case "SpecialCharacters":
      String specialChars = GenericStepDefinition.getSpecialCharacterString(lengthOfString);
     driver.findElement(By.xpath(locator)).sendKeys(specialChars);
     break;   
 case "UpperCaseCharacters":
      String upperCaseChars = GenericStepDefinition.getSpecialCharacterString(lengthOfString);
     driver.findElement(By.xpath(locator)).sendKeys(upperCaseChars);
     break; 
 case "LowerCaseCharacters":
      String lowerCaseChars = GenericStepDefinition.getSpecialCharacterString(lengthOfString);
     driver.findElement(By.xpath(locator)).sendKeys(lowerCaseChars);
     break;     
 default:
     break;
 }
    }

 @Then("^I check that \"([^\"]*)\" button is enabled in \"([^\"]*)\" screen$")
    public void i_check_that_something_button_is_enabled_in_something_screen(String strArg1, String strArg2) throws Throwable {
       Thread.sleep(3000);
       String xpath = "//*[text()='"+strArg1+"']";
       boolean isElementPresent = driver.findElements(By.xpath(xpath)).size() > 0;
       Assert.assertTrue(isElementPresent);
    }

 public String getXpathOfPlusButtonBySubflowName(String subflowName){
      String str = "//h3[text()='"+subflowName+"']//parent::div//following-sibling::div[@class='fen-header-components']//child::a";
      return str;
}

 @And("^I assert \"([^\"]*)\" section is greyed out$")
    public void i_assert_something_section_is_greyed_out(String strArg1) throws Throwable {
        
       String getXpathOfPlusButton = getXpathOfPlusButtonBySubflowName(strArg1);
      String getLinkOfPlusButton = driver.findElement(By.xpath(getXpathOfPlusButton)).getAttribute("href");
      Assert.assertEquals(getLinkOfPlusButton, null);
      
       
    }

 @And("^I assert that \"([^\"]*)\" is popluated with values from COB$")
    public void i_assert_that_something_is_popluated_with_values_from_cob(String strArg1) throws Throwable {
      SqliteConnection sql = new SqliteConnection();
  SqliteConnection.getInstance();
  Thread.sleep(2000);
  String queryForData = sql.queryForGetDataFromControlsByObjectKey(strArg1);
  ResultSet rst = sql.GetDataSet(queryForData);
  String locator = rst.getString("Locator");
  waitExplicityForVisibility(By.xpath(locator));
  driver.findElement(By.xpath(locator)).click();
  i_take_a_screenshot();
    }
//Megha- To upload 40 documents
	@When("^I add a \"([^\"]*)\" in KYCDocument on \"([^\"]*)\" screen$")
	public void i_add_a_in_KYCDocument_on_screen(String arg1, String arg2) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		SqliteConnection sql = new SqliteConnection();
		SqliteConnection.getInstance();
	
		int i = 2;
		int k = 1;
		
		ResultSet rst11 = sql.getControlsDataByName("SortDocuments");

		String locatorForSortButton = rst11.getString("Locator");
		waitExplicityForVisibility(By.xpath(locatorForSortButton));
		driver.findElement(By.xpath(locatorForSortButton)).click();

		ResultSet rst2 = sql.getControlsDataByName("SortByMandatory");
		String locatorForMandatory = rst2.getString("Locator");
		waitExplicityForVisibility(By.xpath(locatorForMandatory));
		driver.findElement(By.xpath(locatorForMandatory)).click();
		Thread.sleep(5000);
		
		if (arg2.equals("Next")){
			driver.findElement(By.xpath("//span[text()='Next']")).click();
			Thread.sleep(9000);
		}
		
		ResultSet rst = sql.getControlsData("Controls1", arg1);
			
		while (rst.next()) {
			Thread.sleep(7000);
			String locator = rst.getString("Locator");
			waitExplicityForVisibility(By.xpath(locator));
			int Count = driver.findElements(By.xpath(locator)).size();
			System.out.println("Count of the number of element is" + Count);
		
			if (arg2.equals("Next")){
			  while (Count == 15) {
				for (int j = 1; j <= Count; j++) {
					Thread.sleep(9000);
					
				// code to check status of Document - Pending/Recieved
				String Locator1 = "(//span[@class='fen-dataGrid-cell '])[" + i + "]";
				Thread.sleep(4000);
				String Docstatus = driver.findElement(By.xpath(Locator1)).getText();
				System.out.println("status of doc" + Docstatus);
				
				
					if (Docstatus.isEmpty()) {
					throw new Exception("Unable to fetch Docstatus");
					}
				
					Thread.sleep(5000);


				if(Docstatus.equals("Pending")) {			
					String Action = "(//button[@class='f-context-menu__button  icon-button '])[" + k + "]";
					System.out.println("(//button[@class='f-context-menu__button  icon-button '])[" + k + "]");
					Thread.sleep(5000);
					waitExplicityForVisibility(By.xpath(Action));
					driver.findElement(By.xpath(Action)).click();
					ResultSet rst1 = sql.buildQuery("Document", "C1");
					FillInDataWithoutValidation(rst1);

				}
				i = i+4;
				k = k+1;
				
				}
				
				Count = 0;
			}
		}
			
			
			if (arg2.equals("PreviousOne")){			
			while (Count == 25) {
				for (int j = 1; j <= Count; j++) {
					Thread.sleep(9000);
				// code to check status of Document - Pending/Recieved
				String Locator1 = "(//span[@class='fen-dataGrid-cell '])[" + i + "]";
				Thread.sleep(4000);
				String Docstatus = driver.findElement(By.xpath(Locator1)).getText();
				System.out.println("status of doc" + Docstatus);
				

				
				
				if (Docstatus.isEmpty()) {
					throw new Exception("Unable to fetch Docstatus");
					}
				
				Thread.sleep(5000);

				if(Docstatus.equals("Pending")) {			
					String Action = "(//button[@class='f-context-menu__button  icon-button '])[" + k + "]";
					System.out.println("(//button[@class='f-context-menu__button  icon-button '])[" + k + "]");
					Thread.sleep(5000);
					waitExplicityForVisibility(By.xpath(Action));
					driver.findElement(By.xpath(Action)).click();
					ResultSet rst1 = sql.buildQuery("Document", "C1");
					FillInDataWithoutValidation(rst1);

				}
				i = i+4;
				k = k+1;
				
				}
				Count = 0;
			}
			
			}
			if (arg2.equals("PreviousTwo")){		
			while (Count == 25) {
				i = 50;
				k = 13;
				for (int j = 13; j <= Count; j++) {
					Thread.sleep(9000);
				// code to check status of Document - Pending/Recieved
				String Locator1 = "(//span[@class='fen-dataGrid-cell '])[" + i + "]";
				System.out.println("Docstatus" + Locator1);
				Thread.sleep(4000);
				String Docstatus = driver.findElement(By.xpath(Locator1)).getText();
				System.out.println("status of doc" + Docstatus);
				
				
				if (Docstatus.isEmpty()) {
					throw new Exception("Unable to fetch Docstatus");
					}
				Thread.sleep(5000);


				if(Docstatus.equals("Pending")) {			
					String Action = "(//button[@class='f-context-menu__button  icon-button '])[" + k + "]";
					System.out.println("(//button[@class='f-context-menu__button  icon-button '])[" + k + "]");
					Thread.sleep(5000);
					waitExplicityForVisibility(By.xpath(Action));
					driver.findElement(By.xpath(Action)).click();
					ResultSet rst1 = sql.buildQuery("Document", "C1");
					FillInDataWithoutValidation(rst1);

				}
				i = i+4;
				k = k+1;
				
				}
				Count = 0;
			}
		}
		}

	}
	public void clickRiskButton() throws InterruptedException{
	       SqliteConnection sql = new SqliteConnection();
	     try {
	             SqliteConnection.getInstance();
	       } catch (SQLException e) {
	             
	       }
	     String query1 = sql.queryForGetDataFromControlsByNameAndScreen("Risk Override Icon", "Complete Risk Assessment");
	     System.out.println(query1);
	     boolean isOverriddenRiskPresent = driver.findElements(By.xpath("//*[contains(text(),'Overridden from')]")).size() > 0;
	     
	     ResultSet rst1 = sql.GetDataSet(query1);
	     Thread.sleep(3000);
	     try {
	             while (rst1.next()) {
	                     String locator = rst1.getString("Locator");
	                     if (isOverriddenRiskPresent){
	                            locator = locator + "[2]";
	                     } else {
	                            locator = locator + "[1]";
	                     }
	                     driver.findElement(By.xpath(locator)).click();
	             }
	       } catch (SQLException e) {
	             
	             e.printStackTrace();
	       }
	}

	@And("^I validate that the RiskCategory is \"([^\"]*)\" and is overriden$")
	public void i_validate_that_the_riskcategory_is_something_and_is_overriden(String strArg1) throws Throwable {
	       i_verify_the_populated_risk_rating_is_something(strArg1);
	       boolean isOverriddenRiskPresent = driver.findElements(By.xpath("//*[contains(text(),'Overridden from')]")).size() > 0;
	       Assert.assertTrue(isOverriddenRiskPresent);
	}
    @And("^I validate individual risk rating of labels as given below$")
 public void i_validate_individual_risk_rating_of_labels_as_given_below(DataTable table) throws Throwable {
          Thread.sleep(6000);
    
          List<Map<String, String>> list = table.asMaps(String.class, String.class);
             for (Map<String, String> data : list) {
                    Thread.sleep(2000);
                    String label = data.get("LabelName").trim();
                    System.out.println("Label name is" + label);
                    String expectedRiskRating = data.get("RiskRating").trim();
                    if(expectedRiskRating.equals("Default")){
                         expectedRiskRating = "-";
                    }
                    System.out.println("Expected Risk rating is" + expectedRiskRating);
                    String xpathOfRiskField = "";
                    if((label.equalsIgnoreCase("Main Entity / Association Screening Risk"))||(label.equalsIgnoreCase("Association Country Risk"))){
                         xpathOfRiskField = GenericStepDefinition.getXpathOfRiskRatingByGroupName(label);
                    }else{
                         xpathOfRiskField = GenericStepDefinition.getXpathOfRiskRatingByLabelName(label);
                    }
                    String actualRiskRating = driver.findElement(By.xpath(xpathOfRiskField)).getText();
                    System.out.println("Actual Risk Rating of "+label+" is "+actualRiskRating);
                    Assert.assertEquals(expectedRiskRating, actualRiskRating);
             }
 }

    public void zoomToPercentage(int Percetage){
        ((JavascriptExecutor) driver).executeScript("document.body.style.zoom='"+Percetage+"%'");
  }
    
    private String convertLabelIfContainsSingleQuote(String label) {
		String finalStr = label;
		if(label.contains("'")){
			finalStr = label.replace("'", "\\'");
		}
		
		return finalStr;
	}
    
    public void waitIfEnvIsFAB(int waitTimeInSeconds){
    	String login = configFileReader.getloginValue();
    	int waitTimeInMilliSeconds = 1000 * waitTimeInSeconds;
		if(login.equalsIgnoreCase("FAB")){
			try {
				Thread.sleep(waitTimeInMilliSeconds);
				System.out.println("Waited for "+waitTimeInMilliSeconds+" seconds when env is FAB");
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
    }
    
    @And("^I validate that risk is not overriden$")
    public void i_validate_that_risk_is_not_overriden() throws Throwable {
       Thread.sleep(6000);
       boolean isOverriddenRiskPresent = driver.findElements(By.xpath("//*[contains(text(),'Overridden from')]")).size() > 0;
              Assert.assertFalse(isOverriddenRiskPresent);
    }
    @When("^I Navigate to Legal Entity screen of \"([^\"]*)\" added associated party$")
    public void i_Navigate_to_Legal_Entity_screen_of_added_associated_party(String arg1) throws Throwable {
               Thread.sleep(3000);
                    Actions a = new Actions(driver);
                    
                    a.moveToElement(driver.findElement(By.xpath("(//*[@fill='#000000'])["+arg1+"]"))).contextClick().build().perform();
                    Thread.sleep(2000);
                    driver.findElement(By.xpath("//div[text()='Navigate to Legal Entity']")).click();
                    Thread.sleep(3000);
                    
                    
       }
    private void putExpirationDateIfDocTypeIsConstitutive() throws InterruptedException {
        Thread.sleep(1000);
        boolean isDocCategoryConstitutive = driver.findElements(By.xpath("//div[text()='Constitutive']")).size() > 0;
        if(isDocCategoryConstitutive){
               JavascriptExecutor jsExec = (JavascriptExecutor) driver;
               String value = "2023-05-05";
               System.out.println("PageObjects.find({caption:'Expiration Date'}).setValue('" + value + "')");
               jsExec.executeScript("PageObjects.find({caption:'Expiration Date'}).setValue('" + value + "')");
               Thread.sleep(1000);
        }
        
  }

    public void waitExplicityForTextToBePresent(String text){
        try{
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        String xpathOfText = "//*[text()='"+text+"']";
        
        WebElement element = driver.findElement(By.xpath(xpathOfText));
        wait.until(ExpectedConditions.textToBePresentInElement(element, text));
        Thread.sleep(2000);
        } catch (Exception e) {
                     System.out.println("Wait For Text to be present did not work");
              }
     }
     
     public String getIdOfGraphByXpath(String xpath){
        return xpath+"//ancestor::*[contains(@id,'graph')]";
     }
     @When("^I validate that \"([^\"]*)\" is derived based on the selected \"([^\"]*)\" for entity type \"([^\"]*)\"$")
     public void i_validate_that_is_derived_based_on_the_selected_for_entity_type(String strArg1, String strArg2, String strArg3) throws Throwable {
         Thread.sleep(4000);
         String FabSegmentLabelValue = getLabelValueByLabelName(strArg2);
         String actualTargetCodeValue = getLabelValueByLabelName(strArg1);
         System.out.println("Actual Target Code is "+actualTargetCodeValue);
         SqliteConnection sql = new SqliteConnection();
         SqliteConnection.getInstance();
         Thread.sleep(2000);
         String queryForData = sql.queryForGetDataFromControlsByObjectKeyNameType("FABSegment",FabSegmentLabelValue,strArg3 );
         ResultSet rst = sql.GetDataSet(queryForData);
         String expectedTargetCodeValue = rst.getString("Value");
         Assert.assertEquals(expectedTargetCodeValue, actualTargetCodeValue);
         System.out.println("Expected target code is "+expectedTargetCodeValue);
         message.write("Actual Target Code is "+actualTargetCodeValue);
         message.write("Expected Target Code is "+expectedTargetCodeValue);
         
         
         
     }

        public String getLabelValueByLabelName(String LabelName) throws InterruptedException {
              String xpathOfFABSegmentLabel ="//label[text()='"+LabelName+"']";
              String fabSegmentValueElementXpath = xpathOfFABSegmentLabel+"//following-sibling::div//child::span";
               waitExplicityForVisibility(By.xpath(fabSegmentValueElementXpath));
              Thread.sleep(1000);
              String fabSegmentValue = driver.findElement(By.xpath(fabSegmentValueElementXpath)).getText();
              return fabSegmentValue;
        }
        @And("^I click on edit button of added screening$")
        public void i_click_on_edit_button_of_added_screening() throws Throwable {
               SqliteConnection sql = new SqliteConnection();
               SqliteConnection.getInstance();
               Thread.sleep(4000);
               ResultSet rst1 = sql.getControlsData("Controls1", "ActionsInAssessment");
               fillDatafromControlsTable(rst1);
               ResultSet rst = sql.getControlsData("Controls1", "Assessment");
               fillDatafromControlsTable(rst);
        }
        @And("^I navigate and complete added screening$")
        public void i_navigate_and_complete_added_screening() throws Throwable {
               SqliteConnection sql = new SqliteConnection();
               SqliteConnection.getInstance();
               ResultSet rst1 = sql.getControlsData("Controls1", "CompleteScreeningSummary");
               fillDatafromControlsTable(rst1);
        }
        
      //megha
    	@When("^I add a \"([^\"]*)\" in CaptureRequest with username \"([^\"]*)\" and relationshipType \"([^\"]*)\"$")
    	public void i_add_a_in_CaptureRequest_with_username_and_relationshipType(String arg1, String arg2, String arg3) throws Throwable {
    	    // Write code here that turns the phrase above into concrete actions
    		SqliteConnection sql = new SqliteConnection();
    		SqliteConnection.getInstance();
    		ResultSet rst = sql.getControlsData("Controls1", arg1);
            System.out.println("arg3" +arg3);

    		
    		while(rst.next()){

                String name = rst.getString("Name");
                String value = rst.getString("Value");
                String Type = rst.getString("Type");
                String locator = rst.getString("Locator");
                
                System.out.println("name" +name);
                System.out.println("value" +value);
                System.out.println("Type" +Type);
                System.out.println("locator" +locator);

                
                switch (Type) {
                case "button":
                    Thread.sleep(2000);

                	waitExplicityForVisibility(By.xpath(locator));
                    driver.findElement(By.xpath(locator)).click();
                    Thread.sleep(2000);

                    break;
                case "TextBox":
                	Thread.sleep(3000);
                    waitExplicityForVisibility(By.xpath(locator));
                    driver.findElement(By.xpath(locator)).sendKeys(arg2);
                   Thread.sleep(1000);
                   break;
                case "CheckBox":
    				Thread.sleep(1000);
                	waitExplicityForVisibility(By.xpath(locator));
    				driver.findElement(By.xpath(locator)).click();
    				Thread.sleep(3000);
    				break;
                case "Dropdown":
                    Thread.sleep(2000);
                    waitForDropDown(name);
                	if (name.equals("Relationship Type")) {
                    System.out.println("Dropdown field name :'"+name+"' and value is '"+arg3);
    				  jsExecutor.executeScript(
    						"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + arg3 + "')");
                    
                    }
                    else 
                    {
                      waitForDropDown(name);
    				  System.out.println("Dropdown field name :'"+name+"' and value is '"+value);
    				  jsExecutor.executeScript(
    						"PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
    				 Thread.sleep(1000);
    			   }
                	break;

    	}
    		}
    		
    	}
        @And("^I click on Plus button of \"([^\"]*)\" subflow$")
        public void i_click_on_plus_button_of_something_subflow(String subflowName) throws Throwable {
            String xpathOfPlusButton = GenericStepDefinition.getXpathForPlusButtonBySubflowName(subflowName);
            WebElement element = driver.findElement(By.xpath(xpathOfPlusButton));
            waitExplicityForVisibility(By.xpath(xpathOfPlusButton));
            Thread.sleep(2000);
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();", element);
            js.executeScript("arguments[0].click();", element);
            if(subflowName.equalsIgnoreCase("Associated Parties")){
                   waitExplicityForVisibility(By.xpath("(//input[contains(@name,'Legal')])[1]"));
            }
        }

    //megha
        
        private void waitExplicityForTextToBePresent_New(By by, String textToBePreset) {
            waitExplicityForVisibility(by);
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.textToBePresentInElementLocated(by, textToBePreset));
        //System.out.println("-----Used Wait for  Dropdown visibility-----=> "+ by.toString());
       
    }
    	@And("^I initiate Onboarding for Client Type as \"([^\"]*)\" and Legal Entity Type as \"([^\"]*)\"$")
        public void i_initiate_onboarding_for_client_type_as_something_and_legal_entity_type_as_something(String strArg1, String strArg2) throws Throwable {
    		SqliteConnection sql = new SqliteConnection();
    		SqliteConnection.getInstance();
    		ResultSet rst = sql.getControlsData("Controls1", "EnterEntityDetailsLiteKYC");
    		while(rst.next()){

                String name = rst.getString("Name");
                String value = rst.getString("Value");
                String Type = rst.getString("Type");
                String locator = rst.getString("Locator");
       
                switch (Type) {
                case "button":                   
                	waitExplicityForVisibility(By.xpath(locator));
                	Thread.sleep(1000);
                    driver.findElement(By.xpath(locator)).click();
                    Thread.sleep(1000);

                    break;
                case "TextBox":
                	if(value != null){
                    waitExplicityForVisibility(By.xpath(locator));
                	Thread.sleep(1000);
                    driver.findElement(By.xpath(locator)).sendKeys(value);
                   Thread.sleep(1000);
                	}
                   break;
                case "CheckBox":   				
                	waitExplicityForVisibility(By.xpath(locator));
                	Thread.sleep(1000);
    				driver.findElement(By.xpath(locator)).click();
    				Thread.sleep(1000);
    				break;
                case "Dropdown":

                    waitForDropDown(name);
                    if (name.equals("Client Type")) {
                    	System.out.println(
                                "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
                          jsExecutor.executeScript(
                                        "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
                    }

                    else if (name.equals("Legal Entity Type")) {
                    	strArg2 = convertLabelIfContainsSingleQuote(strArg2);
                    	System.out.println(
                                "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");
                          jsExecutor.executeScript(
                                        "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg2 + "')");
                    }
                    else{
                    	jsExecutor.executeScript(
                                "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                    }
                          
                    break;
                }
    		}
        }

    	@And("^I store the \"([^\"]*)\" in HashMap as \"([^\"]*)\"$")
        public void i_store_the_something_in_hashmap(String strArg1, String strArg2) throws Throwable {   		
    			scenariocontext.setValue(strArg1, strArg2);
    		
        }
    	
    	@When("^I complete LegalEntityCategoryWithoutSubmit task for Legal Entity Type \"([^\"]*)\"$")
        public void i_complete_legalentitycategorywithoutsubmit_task_for_legal_entity_type_something(String strArg1) throws Throwable {
    		SqliteConnection sql = new SqliteConnection();
    		SqliteConnection.getInstance();
    		ResultSet rst = sql.getControlsData("Controls1", "LegalEntityCategoryWithoutSubmit");
    		while(rst.next()){

                String name = rst.getString("Name");
                String value = rst.getString("Value");
                String Type = rst.getString("Type");
                String locator = rst.getString("Locator");
       
                switch (Type) {
                case "button":                   
                	waitExplicityForVisibility(By.xpath(locator));
                	Thread.sleep(1000);
                    driver.findElement(By.xpath(locator)).click();
                    Thread.sleep(2000);

                    break;
                case "TextBox":
                	if(value != null){
                    waitExplicityForVisibility(By.xpath(locator));
                	Thread.sleep(1000);
                    driver.findElement(By.xpath(locator)).sendKeys(value);
                   Thread.sleep(1000);
                	}
                   break;
                case "CheckBox":   				
                	waitExplicityForVisibility(By.xpath(locator));
                	Thread.sleep(1000);
    				driver.findElement(By.xpath(locator)).click();
    				Thread.sleep(1000);
    				break;
                case "Dropdown":

                    waitForDropDown(name);
                    if (name.equals("Client Type")) {
                    	System.out.println(
                                "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                          jsExecutor.executeScript(
                                        "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + strArg1 + "')");
                    }

                    else if (value != null) {
                          if (strArg1.equals("Financial Institution (FI)") && name.equalsIgnoreCase("Legal Entity Type")) {
                                 value = "Branch of a Foreign Entity";
                          } else if (strArg1.equalsIgnoreCase("Non-Bank Financial Institution (NBFI)")
                                        && name.equalsIgnoreCase("Legal Entity Type")) {
                                 value = "Bond Issuer & Custodian";
                          } else if (strArg1.equalsIgnoreCase("PCG-Entity") && name.equalsIgnoreCase("Legal Entity Type")) {
                                 value = "Regulatory and Statutory Body";
                          } else if (strArg1.equalsIgnoreCase("Business Banking Group (BBG)")
                                        && name.equalsIgnoreCase("Legal Entity Type")) {
                                 value = "Regulatory and Statutory Body";
                          } else if (strArg1.equalsIgnoreCase("Corporate") && name.equalsIgnoreCase("Legal Entity Type")) {
                              value = "Regulatory and Statutory Body";
                          }   
                          System.out.println(
                                        "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                          jsExecutor.executeScript(
                                        "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                          
                    
                    }else{
                    	System.out.println("PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                    	jsExecutor.executeScript(
                                "PageObjects.find({caption:'" + name + "'}).setValueByLookupText('" + value + "')");
                    }
                    Thread.sleep(2000);
                    break;
                }
    		}
        }

}


