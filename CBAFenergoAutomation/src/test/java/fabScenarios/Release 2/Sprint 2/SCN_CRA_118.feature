#Test Case: SCN_CRA_118 
#User Story ID: 
#Designed by: Vibhav Kumar	
#Last Edited by: Vibhav Kumar
Feature: SCN_CRA_118

 @Automation
  Scenario: Verify the overall risk rating is auto overridden to Very High from WAC Risk rating (LOW) when Country of Incorporation/Establishment score is 5 and override is true  
    Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "SCN_CRA_118" 
	When I complete "Product" screen with key "FundsFABFundsMutualFunds" 
	When I complete "Product" screen with key "Insurance" 
	When I complete "Product" screen with key "CorporateLoan" 
  And I complete "CaptureNewRequest" with Key "SCN_CRA_118" and below data 
		| Product | Relationship |
		| C1      | C1           |	
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"
	Then I store the "CaseId" from LE360  
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "SCN_CRA_118" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
   When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "SCN_CRA_118" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Buyer/Customer" 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "SupplierIndividual" 
	When I complete "CaptureHierarchyDetails" task 
 
	
	When I navigate to "KYCDocumentRequirementsGrid" task  
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_118" 
	When I search for the "CaseId" 
	
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "SCN_CRA_118" 
	When I search for the "CaseId" 
	
 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_118" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task
	And I verify the overridden risk rating is "Medium-Low"
	 And I validate individual risk rating of labels as given below
      | LabelName                                           | RiskRating | 
      | Country of Incorporation / Establishment:           | Low     | 
      | Country of Domicile / Physical Presence:            | Medium | 
      | Countries of Business Operations/Economic Activity: | Medium     | 
      | Association Country Risk                            | Very High       | 
      | Legal Entity Type:                                  | Medium-Low | 
      | Types of Shares (Bearer/Registered):                | -          | 
      | Length of Relationship:                             | Medium | 
      | Industry (Primary/Secondary):                       | High        | 
      | Main Entity / Association Screening Risk            | -  | 
      | Product Type:                                       | High     | 
      | Anticipated Transactions Turnover (Annual in AED):  | Medium     | 
      | Channel & Interface:                                | High        | 


	

	