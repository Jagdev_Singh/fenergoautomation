#Test Case: TC1_CLM_CIB_TC_170
#User Story ID: SCN_RR_085
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC1_CLM_CIB_TC_170_To Verify Risk gets calculated properly for Low risk and is not overridden

  @Automation
  Scenario: To verify whether Risk gets calculated properly for High risk and is overridden
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "SCN_CRA_085"
	  When I complete "Product" screen with key "CapitalMarketAdvisory"
	  When I complete "Relationship" screen with key "C1"
    And I complete "CaptureNewRequest" screen with key "SCN_CRA_085"
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "SCN_CRA_085"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "SCN_CRA_085"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
    When I complete "AssociationDetails" screen with key "DirectorIndividual"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty
    When I complete "LEDetailsNonIndividual" screen with key "SCN_CRA_085"
    When I search for the "CaseId"
    When I navigate to "CompleteAMLGrid" task
    And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty
    When I complete "LEDetailsIndividual" screen with key "SCN_CRA_085"
    When I search for the "CaseId"
    When I navigate to "CompleteAMLGrid" task
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "SCN_CRA_085" 
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I verify the populated risk rating is "High"
    
    And I click on "Case Details" button
    When I assign the task "Complete Risk Assessment" to role group "CIB R&C KYC APPROVER - KYC Manager" and user name "LastName, KYCManager"
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "CompleteRiskAssessmentGrid" task
    And I validate individual risk rating of labels as given below
      | LabelName                                           | RiskRating  | 
      | Country of Incorporation / Establishment:           | Low         | 
      | Country of Domicile / Physical Presence:            | Medium-Low  | 
      | Countries of Business Operations/Economic Activity: | Medium-Low  | 
      | Association Country Risk                            | Medium      | 
      | Legal Entity Type:                                  | Medium      | 
      | Types of Shares (Bearer/Registered):                | -           | 
      | Length of Relationship:                             | Medium-Low  | 
      | Industry (Primary/Secondary):                       | Medium      | 
      | Main Entity / Association Screening Risk            | High        | 
      | Product Type:                                       | Low         | 
      | Anticipated Transactions Turnover (Annual in AED):  | Medium      | 
      | Channel & Interface:                                | Low         | 

