#Test Case: TC10_FI_PensionFunds
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC10_FI_PensionFunds-ETE Validations

  @Automation
  Scenario: Validate ETE LiteKYC flow" for Client Type "FI"  and LE Category "Pension Funds"
    
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Pension Funds" for "Dropdown" field "Legal Entity Category"
    And I check that below data is visible
      | FieldLabel                                 |
      | Does the CDD profile qualify for Lite KYC? |
    And I validate the following fields in "Complete" Screen
      | Label                                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Does the CDD profile qualify for Lite KYC? | Dropdown  | true    | false    | true      | Select...  |
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Pension Funds" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    When I navigate to "LE360overview" screen
    When I navigate to "Cases" from LHN section
    Then I Validate the CaseName Contains "Lite KYC Onboarding" in it
    And I navigate to "CaptureRequestDetailsGrid" task
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I assert below documents are present with following properties
      | KYC Document Requirement | Default Document Type | Default Document Category | Mandatory |
      | Prospectus               | Prospectus            | Constitutive              | No        |
      | Reliance / AML Letter    | Reliance / AML Letter | Constitutive              | No        |
      | IM Agreement             | IM Agreement          | Constitutive              | No        |
      | Issue Document           | Issue Document        | Constitutive              | No        |
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    Then I check that the "RiskAssessmentLiteKYC" button is disabled
    And I assert "RiskAssessmentLiteKYC" is populated as "Medium"
    When I complete "RiskAssessment" screen with key "LiteKYC"
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    