#Test Case: TC13_Non Vostro Non Casa_Link Document_FI
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC13_Non Vostro Non Casa_Link Document_FI

  @Automation
  Scenario: Verify KYC maker is able to link document at KYC document requirement task
    
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    And I check that below data is visible
      | FieldLabel                                 |
      | Does the CDD profile qualify for Lite KYC? |
    And I validate the following fields in "Complete" Screen
      | Label                                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Does the CDD profile qualify for Lite KYC? | Dropdown  | true    | false    | true      | Select...  |
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    When I navigate to "LE360overview" screen
    When I navigate to "Cases" from LHN section
    Then I Validate the CaseName Contains "Lite KYC Onboarding" in it
    And I navigate to "CaptureRequestDetailsGrid" task
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I am able to upload, unlink and link following documents
      | Document Name           | 
      | Wolfsberg Questionnaire | 
    And I take a screenshot
    