#Test Case: TC_R1EPIC001PBI020_06
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: LEVerified details screen- "Home Phone, Work Phone, Mobile" fields
@To_be_automated
Scenario: Verify following Auto-populated fields are displaying under contacts section on "LE verified details" screen
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I click on "+" sign displaying at the top of contact section to create contact
	When I navigate to "CreateContact" task
	#Test Data: validate values in "Home Phone, Work Phone, Mobile" fields on" Create contact" screen
	When I enter values in "Home Phone, Work Phone, Mobile" fields in format "CCCNNNNNNNNNNN exact 14 numbers" and save the details 
	When I complete "CaptureRequestDetailsFAB" task 
	When I complete to "ReviewRequest" task	
	Then I store the "CaseId" from LE360
	Given I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task	
	When I complete "ValidateKYCandRegulatoryGrid" task	
	When I navigate to "EnrichKYCProfileGrid" task
	When I complete "EnrichKYCProfile" task 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "RiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	
	#Login with different user due to maker/checker roles
	Given I login to Fenergo Application with "Superuser2" 
	When I search for the "CaseId" 
	When I navigate to "ReviewOnboardingGrid" task 
	When I complete "ReviewOnboarding" task
		
	#	Validating that the case status is closed
	And I assert that the CaseStatus is "Closed" 	
	When I navigate to "LEVerifieddetails" task of "LE360-overview" screen	
	#Test Data: Validate auto-populated values under contact section on "LEVerifieddetails" task
	When I expand "Contacts" section 
	Then I can see values in "Home Phone, Work Phone, Mobile" fields are displaying as auto-populated
	
	
	
	
	
	
	
	
	
	
	
	
	
	