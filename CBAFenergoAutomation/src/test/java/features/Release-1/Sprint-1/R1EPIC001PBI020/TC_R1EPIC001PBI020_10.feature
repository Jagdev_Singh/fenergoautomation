#Test Case: TC_R1EPIC001PBI020_10
#PBI: R1EPIC001PBI020
#User Story ID: US126/SR10
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: create contact screen-"Mobile" field

Scenario: Validate "Mobile" field becomes mandatory when "Primary Phone Number" is set to "Mobile" on "Create contact" screen
	Given I login to Fenergo Application with "RM" 
	When I create new request with LegalEntityrole as "Client/Counterparty" 
	When I navigate to "CaptureRequestDetailsFAB" task 	
	When I click on "+" sign displaying at the top of contact section to create contact
	When I navigate to "CreateContact" task
	#Test Data: Validate "Mobile" field becomes mandatory when "Primary Phone Number" field is set to "Mobile" on" Create contact" screen
	When I set "Primary Phone Number" field to "Mobile" on" Create contact" screen
	Then I can see "Mobile" field becomes mandatory
