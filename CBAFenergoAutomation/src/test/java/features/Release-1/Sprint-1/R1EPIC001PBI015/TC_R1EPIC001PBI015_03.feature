#Test Case: TC_R1EPIC001PBI015_03
#PBI: R1EPIC001PBI015
#User Story ID: TOM156
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI015_03

Scenario: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid 
	Given I login to Fenergo Application with "RM"
	When I create new request with LegalEntityrole as "Client/Counterparty"
	When I navigate to "CaptureRequestDetailsFAB" task 
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker" 
	When I search for "CaseID"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task
   When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
  Then I select "Refer" option by clicking on "Actions" button on "Casedetails" screen
	
	#Test data: Validate "Cancel for Reprocessing" case status has been renamed as "Referred for Reprocessing" in Tasks grid on "Case details" screen
	Then I can see case status for case is displaying as "Referred for Reprocessing" in Tasks grid
