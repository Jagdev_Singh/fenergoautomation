#Test Case: TC_R1EPIC002PBI031_03
#PBI: R1EPIC002PBI031
#User Story ID: US094, US095, US096, US097, OOTBF047, OOTBF046, OOTBF045, OOTBF043, OOTBF041
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Enrich Client Details Industry Codes

  Scenario: Validate behaviour of fields in Industry Codes Details section in LE360 - LE Verified Details screen and behaviour of "Primary Industry of Operation Islamic" field when CountryOfIncorporation is not "UAE" and UID Orginating Branch is not "Head Office_ISB-100"
    Given I login to Fenergo Application with "RM"
    #Creating a legal entity with "Client Entity Type" as "FI" and "Legal Entity Role" as "Client/Counterparty" and "Country of Incorporation" as "AD-ANDORRA"
    When I create new request with ClientEntityType as "FI" and LegalEntityrole as "Client/Counterparty" and CountryOfIncorporation as "AD-ANDORRA"
    And I navigate to "CaptureRequestDetailsFAB" task
    #Test data C10: Select "Head Office _ BNK-100" value for "UID Orginating Branch" field and fill all mandatory data
    And I complete "CaptureRequestDetailsFAB" task with key "C11"
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #And I validate the following field in "Industry Codes Details" section
    And I validate "Primary Industry of Operation Islamic" field is defaulted to blank
    And I validate "Primary Industry of Operation Islamic" field is not-editable
    And I select "Dummy 2" for "Primary Industry of Operation" field
    And I select "Dummy 1" for "Secondary Industry of Operation" field
    And I validate "Primary Industry of Operation Islamic" field is defaulted to blank
    And I validate "Primary Industry of Operation Islamic" field is not-editable
    And I select "Dummy 3" for "Primary Industry of Operation UAE" field
    When I complete "EnrichKYCProfileFAB" task
    #Complete all stages and close the case
    Then I navigate to LE360 screen
    #Verify if only the following fields are visible under "Industry Codes Details" section in the mentioned order
    And I validate the only following fields in "Industry Codes Details" section in LE-Verified Details screen
      | Fenergo Label Name                    | Field Type | Visible | Editable | Mandatory   | Field Defaults To |
      | Primary Industry of Operation         | Drop-down  | Yes     | No       | Yes         | Dummy 2           |
      | Secondary Industry of Operation       | Drop-down  | Yes     | No       | No          | Dummy 1           |
      | Primary Industry of Operation Islamic | Drop-down  | Yes     | No       | Conditional | blank             |
      | Primary Industry of Operation UAE     | Drop-down  | Yes     | No       | Yes         | Dummy 3           |
