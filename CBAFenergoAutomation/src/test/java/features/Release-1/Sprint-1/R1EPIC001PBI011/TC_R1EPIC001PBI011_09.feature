#Test Case: TC_R1EPIC01PBI011_09
#PBI: R1EPIC01PBI011
#User Story ID: US031
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI011_09 

@TC_R1EPIC01PBI001_09
Scenario: Validate "Accounts" subflow is displaying as hidden for following users on "Review Request details" task
	Given I login to Fenergo Application with "RM" 
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button 
	And I store the "CaseId" from LE360 
	Then I can see "Accounts" subflow is hidden 
	When I complete "ReviewRequest" task 
	Given I login to Fenergo Application with "OnboardingChecker"
	When I search for the "CaseID"
	When I navigate to "ReviewRequestGrid" task
	Then I can see "Accounts" subflow is hidden 
	Given I login to Fenergo Application with "FLoydKYC"
	When I search for the "CaseId" 
	When I navigate to "ReviewRequestGrid" task
	Then I can see "Accounts" subflow is hidden 
	Given I login to Fenergo Application with "FlodAVP"
	When I search for the "CaseId" 
	When I navigate to "ReviewRequestGrid" task

	When I login to Fenergo Application with "BusinessUnitHead"
	When I search for the "CaseId" 
	When I navigate to "ReviewRequestGrid" task
	When I login to Fenergo Application with "FlodVP"
	When I search for the "CaseId" 
    When I navigate to "ReviewRequestGrid" task
	Then I can see "Accounts" subflow is hidden 
	When I login to Fenergo Application with "FlodSVP"
	When I search for the "CaseId" 
	When I navigate to "ReviewRequestGrid" task
	Then I can see "Accounts" subflow is hidden 
	When I login to Fenergo Application with "Business Head (N2)"
	When I search for the "CaseId" 
	When I navigate to "ReviewRequestGrid" task
	Then I can see "Accounts" subflow is hidden 
	When I login to Fenergo Application with "Compliance"
	When I search for the "CaseId" 
	When I navigate to "ReviewRequestGrid" task
	Then I can see "Accounts" subflow is hidden 