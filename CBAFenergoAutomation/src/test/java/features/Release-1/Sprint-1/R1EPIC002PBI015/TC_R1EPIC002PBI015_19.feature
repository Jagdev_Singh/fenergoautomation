#Test Case: TC_R1EPIC002PBI015_19
#PBI: R1EPIC002PBI015
#User Story ID: US106
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate behavior of "Entity Type" drop-down field on "LE-verififed data" screen of "New request" stage
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen
	When I complete "Enter Entity details" screen task
	#Test Data: LE role as "Client/Counterparty"
	When I complete request by selecting "Legal Entity Role" as "Client/counterparty"
	When I create "CompleteRequest" by clicking on "Create Entity" button
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
  When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessmentFAB" task 
	When I navigate to "QualityControlGrid" task 
	When I complete "ReviewOnboarding" task 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task
	When I navigate to "FLODKYCReviewandSign-OffGrid" task 
	When I complete "FLODKYCReviewandSign-Off" task
	When I navigate to "FLODAVPReviewandSign-OffGrid" task 
	When I complete "FLODAVPReviewandSign-Off" task
	When I navigate to "BUHReviewandSignOffGrid" task 
	When I complete "BUHReviewandSignOff" task
	When I navigate to "CaptureFabReferencesGrid" task
	When I complete "CaptureFabReferences" task
	#Validating that the case status is closed
   And I see the CaseStatus is "Closed" 
	When I navigate to "LE360overview" screen task
	When I navigate to "LEVerifieddetails" task
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LEVerifieddetails" under "Customerdetailsgrid" when user select "Corporate" in "Client Type" drop-down
	When I select "Corporate" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LEVerifieddetails" screen
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LEVerifieddetails" under "Customerdetailsgrid" when user select "Financial Institution(FI)" in "Client Type" drop-down
	When I select "Financial Institution(FI)" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LEVerifieddetails" screen
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LEVerifieddetails" under "Customerdetailsgrid" when user select "Non-Bank Financial Institution (NBFI)" in "Client Type" drop-down
	When I select "Non-Bank Financial Institution (NBFI)" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LEVerifieddetails" screen
	#Test Data:"Entity Type" drop-down field will be mandatory,defaults to "Non-individual" on "LEVerifieddetails" under "Customerdetailsgrid" when user select "PCG-Entity" in "Client Type" drop-down
	When I select "PCG-Entity" in "Client Type" drop-down
	Then I can see "Entity Type" drop-down is mandatory,defaults to "Non-individual" and the field is displaying as "read only" on "LEVerifieddetails" screen
	