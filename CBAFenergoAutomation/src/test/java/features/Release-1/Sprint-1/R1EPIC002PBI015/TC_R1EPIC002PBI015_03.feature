#Test Case: TC_R1EPIC002PBI015_03
#PBI: R1EPIC002PBI015
#User Story ID: US112
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI015_03 

@Automation
Scenario: Validate "FAB Entity Type" label is renamed as "Client Type" label on "LE360-overview" stage 
	Given I login to Fenergo Application with "RM:IBG-DNE"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I navigate to "LE360overview" screen
	When I click on "LEDetails" from LHS
	#Test Data:"FAB Entity Type" label should be renamed as "Client Type" label on "LE360-LE details" screen of "LE360-overview" stage
	Then I can see "FabEntityType" label is renamed as "ClientType" on EnterEntitydetails screen