#Test Case: TC_R1EPIC002PBI015_24
#PBI: R1EPIC002PBI015
#User Story ID: US114
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate LOVs for "Legal Entity Type" field on "LE360overview" screen
	Given I login to Fenergo application with "RM" user
	When I click on "+" sign to create new request
	When I navigate to "Enter Entity details" screen task
	When I complete "Enter Entity details" screen task
	When I create "CompleteRequest" by clicking on "Create Entity" button
	When I navigate to "LE360overview" screen
	When I navigate to "Customerdetailsgrid" link by clicking on "Ledetails" link present on Left hand navigation panel
	#Test Data: verify LOvs for "Legal Entity Type" field on "LE360overview" screen
	Then I verify LOVs for "Legal Entity Type" field (sequence,LOV,Visibility/editability/mandatory) on "LE360overview" screen