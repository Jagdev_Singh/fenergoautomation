#Test Case: TC_R1EPIC002PBI014_01
#PBI: R1EPIC002PBI014
#User Story ID: US045
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI014_01
	@Tobeautomated
  Scenario: Validate the 'Product Type' dropdown in Capture request details screen and verify the values in Verified LE Details screen
    Given I login to Fenergo Application with "RM"
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureNewRequest" task
    When I navigate to "ProductInformation" screen
    #Validate product type dropdown is filtered with 37 values when product category is selected as 'Cash Management'
    #Fill in data for product category : Cash management
    And I fill the data for "ProductInformation" with key "CM"
    #Validate the product type lov (37 lovs) as mentioned in the PBI
    And I validate the LOV of "ProductType" with key "CMLov"
    #Validate product type dropdown is filtered with 40 values when product category is selected as 'Corporate Finance'
    #Fill in data for product category : Corporate Finance
    And I fill the data for "ProductInformation" with key "CF"
    #Validate the product type lov (40 lovs) as mentioned in the PBI
    And I validate the LOV of "ProductType" with key "CFLov"
    #Validate product type dropdown is filtered with 25 values when product category is selected as 'Global Markets'
    #Fill in data for product category : Global Markets
    And I fill the data for "ProductInformation" with key "CM"
    #Validate the product type lov (25 lovs) as mentioned in the PBI
    And I validate the LOV of "ProductType" with key "CMLov"
    #Validate product type dropdown is filtered with 15 values when product category is selected as 'Trade Finance'
    #Fill in data for product category : Trade Finance
    And I fill the data for "ProductInformation" with key "TF"
    #Validate the product type lov (15 lovs) as mentioned in the PBI
    And I validate the LOV of "ProductType" with key "TFLov"
    #Verify the Product category and Product type field values in Verified LE details screen of LE360 screen
    When I navigate to LE360 screen
    And I check that below data is available
      | FieldLabel       | Value |
      | Product Category |       |
      | Product Type     |       |
