#Test Case: TC_R1EPIC001PBI002 _06
#PBI: R1EPIC001PBI002
#User Story ID: US029, US030
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC001PBI002_06-GLCMS UID validation, CIF validation

  @Automation 
  Scenario: Verify the RM user is able to search entity using GLCMS UID and T24 CIF ID in Advanced search screen.
    Precondition:Entity creation by adding GLCMS UID and T24 CIF ID in Capture FAB References screen
    #Given I login to Fenergo Application with "RM:IBG-DNE"
    #When I complete "NewRequest" screen with key "Corporate"
    #And I store the "Legal Entity ID"    
    And I store the "4930"
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |  
    #And I add a Product from "CaptureRequestDetails"    
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    
    #Given I login to Fenergo Application with "KYCMaker: Corporate"
    #Given I login to Fenergo Application with "SuperUser"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #
    #When I navigate to "EnrichKYCProfileGrid" task
    #When I complete "AddAddress" task
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I complete "CaptureHierarchyDetails" task
    #
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #
    #When I navigate to "CompleteAMLGrid" task
    #Then I complete "CompleteAML" task
    #
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #
    #When I navigate to "CompleteRiskAssessmentGrid" task
    #When I complete "RiskAssessment" task
    #
    #Then I login to Fenergo Application with "RM:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "ReviewSignOffGrid" task
    #When I complete "ReviewSignOff" task
    #
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #When I complete "ReviewSignOff" task
    #
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    #When I complete "ReviewSignOff" task
    #
    #Then I login to Fenergo Application with "BUH:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "BHUReviewandSignOffGrid" task
    #=ADD External references as CIF ID and Counterparty UID
    #When I complete "ReviewSignOff" task
    #
    #Then I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #And I complete "Waiting for UID from GLCMS" task from Actions button 
#		When I navigate to "CaptureFabReferencesGrid" task 
#		When I complete "CaptureFABReferencesCIFId" task 
#		When I complete "CaptureFABReferences" task 
#		And I assert that the CaseStatus is "Closed"
    #=Test Case objective starts here
    And I store the "GLCMSUID" in HashMap as "716NP" 
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I navigate to Advanced Search screen
    When I click on "Filters" button
    When I enter "Legal Entity ID" and "GLCMS UID" in "Advance Search" Screen
    Then I validate the search result by clicking on it
    
