#Test Case: TC_R1EPIC001PBI002_03
#PBI: R1EPIC001PBI002
#User Story ID: US029, US030
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC001PBI002_03

@TC_R1EPIC001PBI002_03
Scenario: RM:Verify the field behaviors of GLCMS UID and T24 CID ID in New Request-Search for duplicates screen for RM User
	
	Given I login to Fenergo Application with "RM" 
	When I complete "Enterentitydetails" task 
	And I check that below data is available 
	|FieldLabel|Mandatory |
	|GLCMS UID |NO				|
	|T24 CIF ID |NO				|
	
	When I enter data in "GLCMS ID" as "ALPHABETICAL"
	Then I validate the error messgage for "GLCMS UID-Alphabetical" as "GLCMS UID can contain only digits. Only zero is invalid input."
	#Test Data: Enter values other than numeric values in GLCMS UID field
	#User should NOT be able to enter values other than numeric
	When I enter data in "GLCMS ID" as "NumericalLessThenSix"
	#Test Data: Enter numeric value less than or equal to 6 digits in GLCMS UID field
	#User should be able to enter numeric values less than or equal to 6 digits
	When I enter data in "GLCMS ID" as "NumericalMoreThenSix"
	#Test Data: Enter numeric value more than 6 digit in GLCMS UID Field
	#User should NOT be able to enter numeric value more than 6 digit
	When I enter data in "GLCMS ID" as "AllZeroes"
	#Test Data: Enter all zeros as 6 digit in GLCMS UID Field
	#User should NOT be able to enter only zeros in GLCMS UID field
	#T24 CIF ID Validation
	When I enter data in "T24 CIF ID" as "AllZeroes"
	#Test Data: Enter values other than numeric values in T24 CIF ID field
	#User should NOT be able to enter values other than numeric
	When I enter data in "T24 CIF ID" as "Lessthan10"
	#Test Data: Enter numeric value less than or equal to 10 digits in T24 CIF ID field
	#User should be able to enter numeric values less than or equal to 10 digits
	When I enter data in "T24 CIF ID" as "Morethan10"
	#Test Data: Enter numeric value more than 10 digit in T24 CIF ID Field
	#User should NOT be able to enter numeric value more than 10 digit
	When I enter data in "T24 CIF ID" as "AllZeroes"
	#Test Data: Enter all zeros as 10 digit in T24 CIF ID Field
	#User should NOT be able to enter only zeros in T24 CIF ID field
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
