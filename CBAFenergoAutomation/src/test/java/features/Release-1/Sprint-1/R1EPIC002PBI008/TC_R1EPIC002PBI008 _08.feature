#Test Case: TC_R1EPIC002PBI008_08
#PBI: R1EPIC002PBI008
#User Story ID: US068
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI008_08

  Scenario: Validate the 'Legal Entity Category' dropdown is filtered with relevant values (15) in Validate KYC and Regulatory data screen when 'Client Type' is selected as 'NBFI' from the Enter entity details screen (Refer lov in the PBI)
    Given I login to Fenergo Application with "RM:NBFI"
    #Select client Type as 'NBFI' in Enter Entity details screen
    When I complete "NewRequest" screen with key "NBFI"
    And I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    Then I store the "CaseId" from LE360
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: FIG"
    Then I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Validate the Legal Entity Category lovs (Refer lov from the PBI)
    Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
