#Test Case: TC_R1EPIC01PBI003_05
#PBI: R1EPIC01PBI003
#User Story ID: US032
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC01PBI003_05

  @Automation @TC_R1EPIC01PBI003_05
  Scenario: 
    Validate if "Product Details" section is removed on Add Product Screen for "Enrich KYC Profile screen"(using edit button)

    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    And I store the "CaseId" from LE360
    When I complete "ReviewRequest" task
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    Then I can see product is visible in Product grid for "EnrichKYCProfile"
    When I view the existing Product from "EnrichKYCProfile"
    Then I can see "ProductDetails" section is not visible
