#Test Case: TC_R1EPIC002PBI006_04
#PBI: R1EPIC002PBI006
#User Story ID: US141, US142, US143
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI006_04
  Scenario: Verify the below fields are greyed out in KYC Conditions section of Validate KYC and Regulatory Data Screen
    #Is the entity an Intragroup entity?
    #Dodd-Frank US Person Type
    #Direct Electronic Access Client?
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = corporate
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I check that below data is available
      | FieldLabel                          | Field Type | Mandatory | Editable | Field Defaults to |
      | Is the entity an Intragroup entity? | Greyed out | No        | No       | Select...         |
      | Dodd-Frank US Person Type           | Greyed out | No        | No       | Select...         |
      | Direct Electronic Access Client     | Greyed out | No        | No       | Select...         |
