#Test Case: TC_R1EPIC002PBI006_03
#PBI: R1EPIC002PBI006
#User Story ID: US130
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI006_03
  Scenario: Verify the below fields are removed in KYC Conditions section of Validate KYC and Regulatory Data Screen
    #Parent Company: Country of Incorporation
    #Parent Regulated By
    #Parent's AML Guidelines Reviewed and Approved
    #Name of Exchange(s) the Parent Entity is Listed On
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = corporate
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I check that below data is available
      | FieldLabel                                         | Visible |
      | Parent Company: Country of Incorporation           | No      |
      | Parent Regulated By                                | No      |
      | Parents AML Guidelines Reviewed and Approved       | No      |
      | Name of Exchange(s) the Parent Entity is Listed On | No      |
