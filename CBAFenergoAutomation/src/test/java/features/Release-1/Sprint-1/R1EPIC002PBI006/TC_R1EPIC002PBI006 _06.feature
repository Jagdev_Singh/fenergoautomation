#Test Case: TC_R1EPIC002PBI006_06
#PBI: R1EPIC002PBI006
#User Story ID: US139
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI006_06
  Scenario: Verify the below filed behaviour for client type NBFI
    Given I login to Fenergo Application with "RM"
    #Create entity with  client type = NBFI
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I check that below data is available
      | FieldLabel                        | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Regulatory ID                     | Textbox    | No        | Yes      | System Calculated | Yes     |
      | Is there an AML program in place? | Dropdown   | No        | Yes      | System Calculated | Yes     |
    #Verfiy the field 'Name of Regulatory Body ' is displayed when 'yes' is selected for the field 'Is this Entity Regulated?'
    #Test data: Is this Entity Regulated? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel              | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Name of Regulatory Body | Textbox    | Yes       | Yes      |                   | Yes     |