#Test Case: TC_R1EPIC01PBI001_01
#PBI: R1EPIC01PBI008
#User Story ID: US028
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora

Feature: COB 

Scenario: Validate the behavior of "Tax Value field" for TAX identifier Type as"VAT ID"
	Given I login to Fenergo application with "RM" user
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "ClientCounterparty"
	When I navigate to "CaptureRequestDetailsGrid" task
	#Test Data: Tax Type: VAT ID
	When I add a "TAX Type" other than "VAT ID" on "AddTaxIdentifier" screen
	Then I can see "TaxIdentifierValue" is displaying as mandatory and accept max 15 numeric characters