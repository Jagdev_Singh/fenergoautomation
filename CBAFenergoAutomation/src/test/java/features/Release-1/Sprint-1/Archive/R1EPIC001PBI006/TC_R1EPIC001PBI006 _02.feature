Feature: TC_R1EPIC001PBI006 _02

Scenario: Verify the RM user is able to search a case using 'Assigned To'.
	
	Given I login to Fenergo Application with "RMUser" 
	When I navigate to "CaseSearch" screen
	#Test Data: Assigned To = Any RM user
	And I fill in data in CaseSearch screen
	When I click on Search button in CaseSearch screen
	#User should be able to view all cases assigned to the RM user
	Then I validate the search grid data
