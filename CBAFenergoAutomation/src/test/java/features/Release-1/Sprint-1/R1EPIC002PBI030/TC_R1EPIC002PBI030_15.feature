#Test Case: TC_R1EPIC002PBI030_15
#PBI: R1EPIC002PBI030
#User Story ID: US085
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI030_15

  Scenario: Validate for conditional field "Offshore Banking License" under "Source Of Funds And Wealth Details" section on "Enrich KYC Profile" task for Onboarding Maker
  Given I login to Fenergo Application with "RM"
   When I create a new request with FABEntityType as "Financial Institution (FI)" and LegalEntityRole as "Client/Counterparty"
     And I complete "CaptureNewRequest" with Key "C3" and below data 
		|Product|Relationship|
		|C1|C1|
	And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "OnboardingMaker"
    When I search for the "CaseID"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
   Then I check that below data is visible
    And I validate the LOVs for "Offshore Banking License" field as per DD and save
