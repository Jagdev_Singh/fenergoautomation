#Test Case: TC_R1EPIC002PBI017.1_04
#PBI: R1EPIC002PBI017.1
#User Story ID: US113
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI017.1_04

  Scenario: Validate "legal Entity name" field "Customer Details" section on "LE360-overview" screen for RM user
    Given I login to Fenergo Application with "RM"
    When I enter Legal entity name with Length upto 255 characters
    When I create new request with LegalEntityrole as "Client/Counterparty"
    When I navigate to "CaptureRequestDetailsFAB" task
    Then I fill values in all mentioned fields and complete "CaptureRequestDetailsFAB" task
    When I navigate to "LE360-overview" task
    When I navigate to "Customer details" section by clicking on "LEdetails" grid
    #Test Data: I Validate "legal Entity name" is displaying as mentioned on New request stage i.e.length upto 255 characters and  and as per DD (sequence, Visible, editable, mandatory)
    And I Validate "legal Entity name" field under "Customer details" section with value length upto 255 characters
