#Test Case: TC_R1EPIC002PBI011_05
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: TC_R1EPIC002PBI011_05 

@Automation @TC_R1EPIC002PBI011_05
Scenario: Validate the behavior of "TaxIdentifierValue" field for input as more than "15 numeric characters" for TAX identifier Type as "VAT ID" and Country as "UAE" in Capture Request Details screen
	Given I login to Fenergo Application with "RM:IBG-DNE"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I complete "TaxIdentifierwithoutSubmit" with TaxType as "VAT ID" and Country as "AE-UNITED ARAB EMIRATES" for "CaptureRequestDetails"
	#Test Data: Tax Type: VAT ID, Country: AE-UNITED ARAB EMIRATES
	And I enter "TaxIdentifierValue" as "Morethan15character"
	And I check that the "SaveandComplete" button is disabled
	Then I validate the error messgage for "TaxIdentifierValue" as "Tax value for VAT ID is not valid. Please enter numeric values of maximum 15 character length." 
	
	