#Test Case: TC_RR1EPIC002PBI011_02
#PBI: R1EPIC002PBI011
#User Story ID: US108
#Designed by: Priyanka Arora (as part of R1EPIC001PBI008)
#Last Edited by: Anusha PS
Feature: TC_R1EPIC002PBI011_02 
 
@Automation
Scenario: 1.Validate the behavior of "Tax Value field" for TAX identifier Type other than "VAT ID" in Capture Request Details screen
and Validate the behavior of "Tax Value field" for TAX identifier Type as "VAT ID" and county other than "UAE" in Capture Request Details screen

	Given I login to Fenergo Application with "RM:IBG-DNE"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	When I complete "TaxIdentifierwithoutSubmit" with TaxType as "SSN" and Country as "AE-UNITED ARAB EMIRATES" for "CaptureRequestDetails"
	
	#Test Data: Tax Type: SSN (anything other than VAT ID)
	
	#the behavior should be same as OOTB as below
	 
	
#	#Test Data: Tax Type: VATID, Country: Other than AE-UNITED ARAB EMIRATES
#	When I add a "TAX Type" as "VATID" and Country as "UNITED STATES" on "AddTaxIdentifier" screen
#	#the behavior should be same as OOTB as below
#	Then I can see "TaxIdentifierValue" is displaying as mandatory and there is no restriction for data/length 
#	
	
