#Test Case: TC_R1S3EPIC002PBI300_03
#PBI: R1S3EPIC002PBI300
#User Story ID: OOTBF035
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI300_03
@Automation
  Scenario: "Add Details" button is displaying as hidden on "Product" screen of  "Validate data and KYC Regulatory data" task of "Validate data" Stage for RM & Onboarding / KYC Maker.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    When I navigate to "Product" screen by clicking on "Plus" button from "CaptureNewRequest"
    #Test-data:  "Add Details" button is displaying as hidden on "Product" screen
    #Then I verify "Add Details" button is displaying as hidden on "Product" screen
    Then I check that below data is not visible
      | FieldLabel  |
      | Add details |
    And I take a screenshot
    And I click on "Cancel" button
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I navigate to "Product" screen by clicking on "Edit" button from "ValidateKYC"
    #Test-data:  "Add Details" button is displaying as hidden on "Product" screen
    #Then I verify "AddDetails" button is displaying as hidden on "Product" screen
    Then I check that below data is not visible
      | FieldLabel  |
      | Add details |
    And I take a screenshot
    And I click on "Cancel" button
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button

    When I navigate to "EnrichKYCProfileGrid" task
    Then I check that below data is not visible
      | FieldLabel  |
      | Add details |
    And I take a screenshot
    

    #Given I login to Fenergo Application with "RM"
    #When I create new request with LegalEntityrole as "Client/Counterparty"
    #When I navigate to "CaptureRequestDetailsFAB" task
    #And I complete "CaptureRequestDetailsFAB" task
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #When I login to Fenergo Application with "OnBoardingMaker"
    #When I search for "Caseid"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    #When I navigate to "Product" task by clicking on + sign displaying at the top of "product" grid
    #Test-data:  "Add Details" button is displaying as hidden on "Product" screen
    #Then I verify "Add Details" button is displaying as hidden on "Product" screen
    #And I save the details successfully
    #When I navigate to "EnrichKYCProfileGrid" task as "OnboardingManager"
    #When I navigate to "Product" task by clicking on Edit button
    #Test-data: "Add Details" button is displaying as hidden on "Product" screen
    #Then I verify "Add Details" button is displaying as hidden on "Product" screen
