#Test Case: TC_R1EPIC011PBI007_01
#PBI: R1EPIC011PBI007
#User Story ID: 
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI007_01

  @Automation 
  Scenario: Validate the LOVs of Document Type field for client type "Business Banking Group" in all 8 screens and validate the Document Identification number field in all screens
  
    #Creating a legal entity with "Client Entity Type" as "BusinessBankingGroup" and "Legal Entity Role" as "Client/Counterparty" and "Country of Incorporation" as "AE-UNITED ARAB EMIRATES"
    Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "BBG" 
    When I navigate to "Document" screen by clicking on "Plus" button from "CaptureRequestDetails"
    And I select "AOF" for "Dropdown" field "Document Category"
    Then I verify "Document Type" drop-down values with "DocumentCategory" as "AOF"
    And I select "Authorized Signatories" for "Dropdown" field "Document Category"
    Then I verify "Document Type" drop-down values with "DocumentCategory" as "Authorized Signatories"
     And I select "Constitutive" for "Dropdown" field "Document Category"
    Then I verify "Document Type" drop-down values with "DocumentCategory" as "Constitutive"
    And I select "MISC" for "Dropdown" field "Document Category"
    Then I verify "Document Type" drop-down values with "DocumentCategory" as "MISC"
    
    
    
   
#    And I validate LOVs of "Document Type" field
#    #Refer PBI for LOV list (21 values)
#    And I select "Authorized Signatories" for "Document Category" field
#    And I validate LOVs of "Document Type" field
#    #Refer PBI for LOV list (9 values)
#    And I select "Constitutive" for "Document Category" field
#    And I validate LOVs of "Document Type" field
#    #Refer PBI for LOV list (22 values)
#    And I select "MISC" for "Document Category" field
#    And I take a screenshot
#    And I validate LOVs of "Document Type" field
#    #Refer PBI for LOV list (9 values)
#    And I enter duplicate value for "Document Identification Number" field
#    And I assert that the following message is diplayed along with reference to the details and location of the other (duplicate) document
#      | Document Identification Number already exists |
    #Repeat the same validation in the below screens
    #LE > Documents > Document Details
    #Enrich Client Info > KYC Doc Reqts > Document Details
    #AML > Complete AML > Document Details
    #AML > Complete ID&V > Edit Verification > Document Details
    #Capture Request Details > Product > Document Details
    #Enrich KYC Profile > Tax Identifier > Document Details
    #AML > Complete AML > Hierarchy > Add Fircosoft Screening > Assessment > Document Details