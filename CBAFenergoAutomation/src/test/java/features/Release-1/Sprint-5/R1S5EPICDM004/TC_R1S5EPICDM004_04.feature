#Test Case: TC_R1S5EPICDM004_04
#PBI: R1S5EPICDM004
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to add a Product subflow (DM screen - 1)

  @Tobeautomated
  Scenario: Validate RM user is able to add  Product  via Product subflow along with Fields validation on "DM screen-1 (Capture LE details)" for "PCG Entity" client Type
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a new DM request with FABEntityType as "PCG Entity" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
    When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    #Test-data: Verify Product Category, Product Type, Booking Entity under Product information section
    Then I check that below data is available
      | FieldLabel       | Value |
      | Product Category |       |
      | Product Type     |       |
      | Booking Entity   |       |
    When I fill in data for product category as "Global markets"
    #Test-data: Verify Product Type gets enabled when filled data in Product Category field under Product information section
    When I fill in data for "ProductType" and "BookingEntity" field
    Then I can see Save button is enabled
    When I click on "Save" button
    #Test-data: Verify Product is added under Products sub-flow on "CaptureLEdetails" task
    Then I Check that the newly added product is visible
    #Test-data:Verify user is able to Add another Product
	  When I click on plus button displaying at the top of "Products" sub-flow
    When I navigate to "ProductInformation" screen
    Then I enter all the required details and click on save
    And I see another product is added to Product grid