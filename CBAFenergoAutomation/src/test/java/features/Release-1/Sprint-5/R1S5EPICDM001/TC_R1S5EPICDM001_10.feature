#Test Case: TC_R1S5EPICDM001_10
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Ability to search migrated profiles

 
  Scenario: Validate if DM user/any othere is able to search migrated entity
  #Precondition - Create a migrated entity using DM screen 1 and 2 and note the T24 CIF ID of the migrated entity
  		
  Given I login to Fenergo Application with "DM user"
  And I navigate to "Legal Entity Search" screen
  And I expand "Advanced Search"
  And I enter fill "T24 CIF ID" with T24 CIF ID value of the migrated entity
  And I assert that the migrated entity is visible in the result grid
  
  Given I login to Fenergo Application with "DM user"
  And I navigate to "Legal Entity Search" screen
  And I enter fill "Legal Entity ID" with Legal Entity ID value of the migrated entity
  And I assert that the migrated entity is visible in the result grid
  
    ## Repeat the above validations for all the roles mentioned users