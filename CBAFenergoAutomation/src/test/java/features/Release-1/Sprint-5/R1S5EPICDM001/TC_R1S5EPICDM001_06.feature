#Test Case: TC_R1S5EPICDM001_06
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Ability to perform database validation - Validate the data values and formats entered by BOT

 
  Scenario: Validate if application throws appropriate error message when invalid data is inputted
  #Placeholder test case
  #This is a generic test case to check valid data. For example, "Legal Entity Name" field should accept only numerals, upper/lower case alphabets, space and dot. These validation will be covered as part of the test cases created for panel validation. Hence this is just a place holder. Once all those validations are passed, this TC can be passed
  