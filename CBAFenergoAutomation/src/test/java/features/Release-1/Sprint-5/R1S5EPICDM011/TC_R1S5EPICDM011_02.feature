#Test Case: TC_R1S5EPICDM011_02
#PBI: R1S5EPICDM011
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to receive a reconcilation file from Fenergo

  Scenario: Verify generated data (DM LE having Partial data in sub-flows) in XML file exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "BBG" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Do not add record for "Anticipated Transactional Activity (Per Month)" sub-flow
    And I validate no record is added in "Anticipated Transactional Activity (Per Month)" sub-flow
    
    #Test-data: Do not add doc for "Documents" sub-flow
    And I validate no Document is added in Document Sub-flow
  
   #Test-data: Verify user is able to add record for "Products" sub-flow
    When I click on plus button displaying at the top of "Products" sub-flow
    Then I navigate to "Add Products" task screen
    Then I enter all required details and click on save button
    And I validate Product is added successfully
    
    #Test-data:Verify user is able to Add Relationship
    When I click on plus button displaying at the top of "Relationship" sub-flow
    When I navigate to "AddRelationship" task
    Then I enter all the required details and click on save
    And I see relationship is added to relationship grid successfully
    
    #Test-data:Do not Add record  in Addresses sub-flow
    And I validate no record is added in Addresses Sub-flow
    
    #Test-data:Verify user is able to Add Contacts
    When I click on plus button displaying at the top of "Contacts" sub-flow
    When I navigate to "AddContact" task
    Then I enter all the required details and click on save
    And I see Contact is added to Address grid successfully
    And I save the details successfully
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.
 		
 		
 		
 		
 		
 		
 		
 		
 		
 		
    
 
 
 
 
 