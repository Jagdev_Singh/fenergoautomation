#Test Case: TC_R1S5EPICDM012_07
#PBI: R1S5EPICDM012
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S5EPICDM012_07

    Scenario: Verify INability to add subflows for completed cases
    Given I login to Fenergo Application with "DMuser"
    #Create DM Request with Client type = FI and Country of Incorporation / Establishment = UAE or any other country
    #Click on + button and then click on New DM Request
    When I navigate to DM Request screen
    #Fill in all the mandatory and non-mandatory fields and click on Save button
    #Add all the subflows 
    #Navigate to Complete AML scree and complete the task
    #Search existing DM Entity
    #Click on + button and then click on New DM Request and select 'Existing' in DM Request type
    #Enter T24 CIF ID (for the entiy created with adding all the sub flows)
    #Verify the system not allows to add the sub flows
