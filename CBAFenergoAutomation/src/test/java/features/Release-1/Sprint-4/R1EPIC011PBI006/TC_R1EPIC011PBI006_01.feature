#Test Case: TC_R1EPIC011PBI006_01
#PBI: R1EPIC011PBI006
#User Story ID: US100, US090, US068 (SECONDARY)
#Designed by: Niyaz Ahmed
#Last Edited by: Vibhav Kumar
Feature: TC_R1EPIC011PBI006_01 

@Automation 
Scenario:
Validate behaviour of below fields in Customer Details section of Enrich KYC Profile screen 
#    #country of Incorporation / Establishment
#    #Group Name
#    #Legal Entity Category
# Comment: Legal Entity Category field is now hidden, hence updated that step validation
	Given I login to Fenergo Application with "RM:BBG" 
	#Create entity with Country of Incorporation = UAE and client type = Business Banking Group
	When I create a new request with FABEntityType as "Business Banking Group (BBG)" and LegalEntityRole as "Client/Counterparty" 
	And I complete "CaptureNewRequest" with Key "BBG" and below data 
		|Product|Relationship|
		|C1			|C1					 |
	And I click on "Continue" button 
	
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
  #=Given I login to Fenergo Application with "OnboardingMaker" 
  #=When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
	#Verify the below behaviour in Customer Details section
	#Validate the Legal entity category value is defaulted from complete screen
	And I validate the following fields in "Customer Details" Sub Flow 
      | Label                                    | FieldType    | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Country of Incorporation / Establishment | Dropdown     | true    | false    | true      | Select...  | 
      | Group Name                               | Alphanumeric | true    | false    | false     | NA         | 
      | Legal Entity Category                    | NA           | false   | NA       | NA        | NA         | 
	And I verify "Country of Incorporation / Establishment" drop-down values 
	#Refer Country lov 
