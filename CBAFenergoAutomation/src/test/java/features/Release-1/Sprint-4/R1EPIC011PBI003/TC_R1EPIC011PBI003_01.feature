#Test Case: TC_R1EPIC011PBI003_01
#PBI: R1EPIC011PBI003
#User Story ID: US068 (PRIMARY)
#Designed by: Niyaz Ahmed
#Last Edited by: Vibhav Kumar
Feature: TC_R1EPIC011PBI003_01

  @Automation
  Scenario: Validate field behaviour of field 'Legal Entity Category' in Complete screen
    Given I login to Fenergo Application with "RM:BBG"
    #Creating a legal entity with "Client Entity Type" as "Business Banking Group" and "Legal Entity Role" as "Client/Counterparty"
    #Create entity by selecting New request and enter data and click on Search button and click on continue
    When I complete LegalEntityCategoryWithoutSubmit task for Legal Entity Type "Business Banking Group (BBG)"
    #=Legal Entity Category is now hidden (08-04-21)
    And I validate the following fields in "Legal Entity Category" Sub Flow
      | Label                 | FieldType | Visible | ReadOnly | Mandatory |DefaultsTo|
      | Legal Entity Category | NA        | false   | NA       | NA        | NA				|
    #Verify only the below 13 lovs available in Legal Entity category for client type "Business Banking Group"
    #=Legal Entity Category is now hidden (08-04-21)
    #Then I verify "Legal Entity Category" drop-down values with ClientType as "Corporate"
