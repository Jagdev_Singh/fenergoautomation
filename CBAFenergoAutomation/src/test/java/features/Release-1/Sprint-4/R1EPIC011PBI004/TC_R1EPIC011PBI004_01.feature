#Test Case: TC_R1EPIC011PBI004_01
#PBI: R1EPIC011PBI004
#User Story ID: US024, US026, US040, US055, US057
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI004_01

  @To_be_automated
  Scenario: Validate behaviour of below fields in Customer details section & Internal booking details section of Capture Request Details screen
    #Channel & Interface
    #Justification for opening/maintaining non-resident account
    #Customer Tier
    #Entity Type
    #Target Code
    #UID Originating Branch
    #Create entity with Country of Incorporation = Andora (other than UAE) and client type = Business Banking Group
    Given I login to Fenergo Application with "RM:BBG"
    #Select client Type as 'Business Banking group' in Enter entity details screen
    When I create a new request with FABEntityType as "Business Banking Group (BBG)" and LegalEntityRole as "Client/Counterparty"
    #Navigate to Capture request details screen
    #Verify the below behaviour in Customer details section
    And I validate the following fields in "Customer Details" Sub Flow
      | Label                                                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo     | 
      | Channel & Interface                                        | Dropdown  | true    | false    | true      | Select...      | 
      | Justification for opening/maintaining non-resident account | TextBox   | false   | false    | true      | NA             | 
      | Customer Tier                                              | Dropdown  | true    | false    | true      | Select...      | 
      | Entity Type                                                | Dropdown  | true    | true     | true      | Non Individual | 
    #Verify the lovs for "Channel & Interface" field
#    Then I validate the specific LOVs for "Channel & Interface"
#      | Lovs                                                                        |
#      | Over the counter/Face-to-face (direct banking, branch/relationship manager) |
#      | Non-face-to-face (agents, representative, third parties)                    |
#      | Others                                                                      |
#    #Verify the lovs for "Customer Tier" field
#    Then I validate the specific LOVs for "Customer Tier"
#      | Lovs     |
#      | Platinum |
#      | GOLD     |
#      | Silver   |
#      | Bronze   |
#    #Verify the below behaviour in Internal Booking details section
#    And I validate the following fields in "InternalBookingDetails" section
#      | Label                  | FieldType | Visible | Editable | Mandatory | DefaultsTo |
#      | Target Code            | Dropdown  | true    | true     | true      | Select...  |
#      | UID Originating Branch | Dropdown  | true    | true     | true      |            |
#    #Verify only two lovs displaying in  "UID Originating Branch"
#    Then I validate the specific LOVs for "UID Originating Branch"
#      | Lovs                  |
#      | Head Office _ BNK-100 |
#      | Head Office _ ISB-100 |
#    #Verify the lovs for "Target code" field
#    Then I validate the specific LOVs for "Target Code"
#      | Lovs                                 |
#      | 10 - HEAD OFFICE                     |
#      | 20 - CORPORATE                       |
#      | 21 - PROJECT FINANCE AND SYNDICATION |
#      | 22 - SYNDICATIONS/MNC/EGY            |
#      | 23 - MULTI NATIONAL COMPANIES /MNC   |
#      | 24 - COMMERCIAL BANKING              |
#      | 26 - PRIVATE BANKING CUSTOMERS-HNI   |
#      | 27 - SPECIAL ACCOUNT UNIT            |
#      | 28 - FINANCIAL INSTITUTION GROUP     |
#      | 29 - CORPORATE RESTRUCTURING UNIT    |
#      | 30 - RETAIL                          |
#      | 31 - SMALL BUSINESS                  |
#      | 32 - WEALTH MANAGEMENT               |
#      | 33 - BANC ASSURANCE-WM               |
#      | 40 - TREASURY                        |
#      | 41 - INVESTMENT                      |
#      | 50 - MERCHANT BANKING                |
#      | 60 - GOVERNMENT RELATED ENTITY       |
#      | 75 - FGFS - DUMMY CUSTOMER           |
#      | 80 - INTERNATIONAL BUSINESS DIVISION |
#      | 61 - GOVERNMENT RELATED ENTITY-HNI   |
#      | 25 - ISLAMIC BANKING                 |
#      | 85 - INTERCOMPANY ELIMINATION        |
#      | 86 - FAB SUBSIDIARIES                |
#      | 87 - OTHER SUBSIDIARY                |
#    And I complete "CaptureNewRequest" with Key "C1" and below data
#      | Product | Relationship |
#      | C1      | C1           |
#    And I click on "Continue" button
#    #Verify the data entered in capture new request is getting populated in Review request screen
#    When I complete "ReviewRequest" task
#    Then I store the "CaseId" from LE360
#    #Navigate back to Capture request details screen and check the datas are retained
#    When I navigate to "CaptureNewRequest" task
