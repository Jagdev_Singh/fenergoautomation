#Test Case: TC_R2EPIC018PBI101_23
#PBI:R2EPIC018PBI101
#User Story ID: CRS 107: 1-5, CRS 108, CRS 109
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC018PBI101_23 

	Scenario:Validate 'document requirement' generated in 'Document requirement' sub-flow is derived by the  value selected in 'Document Type' drop-down under CRS Detail sub-flow On Complete CRS classification task of RR workflow for Client type BBG

	#Validate Notification 'Cannot save and complete as the document requirements have not been met" displays at the top of the 'Complete CRS classification' task screen if user does not fulfill the generated document requirement and user is not able to complete the task without fulfilling document requirement of RR workflow for Client type BBG

	#Validate user is able to add comments in  'Comments' sub-flow and comments sub-flow display as mandatory on 'Add comments' screen of CRS Classification task of RR workflow

	#Validate user is able to Capture the 'Document Signed date' and 'Document received date' dates under CRS details sub-flow of Complete CRS classification task of RR workflow for Client type BBG

	#Validate when user navigates through Document sub-flow, then document category display as "AOF" and Doc type as "CRS & FATCA Self-Certification Form",  When Doc type "CRS & FATCA Self-Certification Form"  is selected on CRS details screen for RR Workflow for Client type BBG

	#Validate when user navigates through Document sub-flow, then document category display as "MISC" and Document type "others ;When Doc type "CRS Waiver"  is selected on CRS details screen for RR workflow for Client type BBG

	#Validate below Lovs for 'IF UAE resident, then visa is valid for 5 years or more' under 'Tax controlling Person details' sub-flow of Complete CRS Classification task screen in RR workflow for Client type Corporate#PreCondition: Create entity with client type as Corporate and confidential as BBG
	
	Given I login to Fenergo Application with "RM:BBG" 
	When I complete "NewRequest" screen with key "BBG" 
	#Do not add product to LE
	And I complete "CaptureNewRequest"  
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "Preliminary Tax Assessment" task
	When I complete the "Preliminary Tax Assessment" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	When I navigate to 'Classifications' stage
	#Validate Complete CRS classification is not triggered in Classification stage
	Then I validate 'CRSClaasification' task is not triggered
	When I navigate to 'CRSClaasification' task screen
	Then I validate 'CRSClaasification' task is not triggered
	When I complete "Classification" task    
    When I login to Fenergo Application with "RM:BBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:BBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Given I login to Fenergo Application with "KYCMaker: BBG
    When I search for the "CaseId"
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
        		
	# Initiate RR workflow
    And I initiate "Regular Review" from action button
    And I initiate "Regular Review" from action button 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "ReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task	 
	When I select "Counter Party Type" as "FINANCIAL_INSTITUTION_OTHER_THAN_BANK" and "Legal Constitution Type" as "Sovereign"
	When I complete "Review/EditClientData" Task
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "Preliminary Tax Assessment" task	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	And I click on "ID&VLinkInRR" button 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task
   	When I navigate to "classification" stage
  	Then I validate "CRSclassification" is triggered
	When I navigate to 'CRSClassification' task screen
	#Validate Complete CRS classification is triggered in Classification stage
	Then  I validate 'CRSClaasification' task is triggered 
	When  I select 'CRS & FATCA Self-Certification Form' from document type	drop-down 
	#validate the 'CRS & FATCA Self-Certification Form' is appearing as document requirement under Document requirement sub-flow
	Then  I validate the 'CRS & FATCA Self-Certification Form' is appearing as document requirement under Document requirement sub-flow 
	#Validate when user navigates through Document sub-flow, then document category display as "AOF" and Doc type as "CRS & FATCA Self-Certification Form",  When Doc type "CRS & FATCA Self-Certification Form"  is selected on CRS details screen
	When  I click on plus sign displaying at the top of document requirement sub-flow to add document 
	Then  I validate document category display as "AOF" and Doc type as	"CRS & FATCA Self-Certification Form" 
	
	When  I select 'CRS Waiver' from document type drop-down 
	#validate the 'CRS Waiver' is appearing as document requirement under Document requirement sub-flow
	Then  I validate the 'CRS Waiver' is appearing as document requirement under	Document requirement sub-flow 
	#Validate when user navigates through Document sub-flow, then document category display as "MISC" and Document type "others ;When Doc type "CRS Waiver"  is selected on CRS details screen
	When  I click on plus sign displaying at the top of document requirement sub-flow to add document 
	Then  I validate document category display as "MISC" and Doc type as "Others"on Add documents task screen 
	
	When  I click on 'Save and complete' button 
	#validate the notification Cannot save and complete as the document requirements have not been met" displays at the top of the 'Complete CRS classification' task screen
	Then  I validate the notification Cannot save and complete as the document requirements have not been met" displays at the top of the 'Complete CRS classification' task screen 
	When  I add document corresponding to document requirement 
	Then  I validate document is saved successfully 
	
	#Validate user is able to Capture the 'Document Signed date' and 'Document received date' dates under CRS details sub-flow and save dates successfully
	When  I add dates for 'Document Signed date' and 'Document received date' dates	under CRS details sub-flow 
	When  I click on save, dates are saved successfully 
	
	When  I navigate to Add comments screen by clicking on Plus button displaying at the top of comments sub-flow 
	#Validate user is able to add comments in  'Comments' sub-flow and comments sub-flow display as mandatory on 'Add comments' screen of CRS Classification task of COB workflow
	Then  I validate 'Comments' text box is displaying as mandatory 
	And  user is not able to proceed further without adding comments on	'Add comments' task screen 	When  I add comments and then click on save 
	Then  I verify comments are saved 
	
	When  I navigate back to 'CRSClaasification' task 
	When  I click on Plus sign displaying at the top of	'Tax controlling person details' 
	#Validate below Lovs for 'IF UAE resident, then visa is valid for 5 years or more' under 'Tax controlling Person details' sub-flow
	When  I click on 'IF UAE resident, then visa is valid for 5 years or more'	field 
	Then  I verify LOVs for	'IF UAE resident, then visa is valid for 5 years or more' field 
	|Yes, 5 years or more|
	|No, less than 5 years|
	|Not applicable|	
	And  I complete "Classification" task 
	
	
  	