#Test Case: TC_R2EPIC018PBI001_09
#PBI:R2EPIC018PBI001
#User Story ID:PRELIM-001,PRELIM-002,PRELIM-003
#Designed by: Sasmita Pradhan
#Last Edited by: 
Feature: TC_R2EPIC018PBI001_09

  Scenario: BBG- Validate the task "Preliminary Tax Assessment" is triggered after the task "Capture Hierarchy Details" in Stage 3 - Enrich Client Information and assign to KYC Maker role group in COB workflow.
    #Validate the task "Preliminary Tax Assessment" is triggered even after adding a product
    #Validate the field behavior in "Other US Indicia" and "US Tax Form" section on "Preliminary Tax Assessment" screen
   ##################################################################################################
    #PreCondition: Create entity with client type as BBG and confidential as BBG.
    #######################################################################################
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Adding a Product 
    When I navigate to "Product" screen
    When I add a "Product " in Product 
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
   
    # Validate the task "Preliminary Tax Assessment" is triggered after the task "Capture Hierarchy Details" and assign to KYC Maker role group
    And I Validate the task "Preliminary Tax Assessment " is triggered aftre the task "Capture Hierarchy Details" and assign to KYC Maker role group
    
    When I navigate to "Preliminary Tax Assessment" task
    #"Preliminary Tax Assessment" screen
   
    #verify field behavior for the fields "Does the client have any standing instructions to the US?,Does the client have an 'in-care of' / 'Hold mail' address that is the sole address of the entity?
    And I validate the following fields in "Other US Indicia" section
      | Label                                                                                             | Field Type     | Visible | Editable | Mandatory | Field Defaults To |
      | Does the client have any standing instructions to the US?                                         |  drop-down     | Yes     | Yes      | No        |  Select...        |
      | Does the client have an 'in-care of' / 'Hold mail' address that is the sole address of the entity?|  drop-down     | Yes     | Yes      | No        |  Select...        |
    #Verify field behavior for the fields ""Do you have a US Tax Form from the client?""
    And I validate the following fields in "US Tax Form" section
    | Label                                         | Field Type    | Visible | Editable | Mandatory | Field Defaults To |
    | "Do you have a US Tax Form from the client?"  | drop-down     | Yes     | Yes      | No        |  Select...        |
   
      
    
    
    
