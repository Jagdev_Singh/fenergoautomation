#Test Case: TC_R2EPIC024PBI059.2c_06
#PBI:R2EPIC024PBI059.2c
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2c_06

Scenario: Validate the documents in the document matrix will add/change based on new LE Category in an LEM case.

#Pre-Requisite : An already COB case.
	
	Given I login to Fenergo Application with "SuperUser" 
	When I search for the "CaseId" 
	
	And I initiate "Maintenance Request" from action button 
	When I select "LE Details" for field "Area" 
	When I select "KYC Data and Customer Details" for field "LE Details Changes" 
	And I click on "CreateLEMSubmit" button 
	And I complete "MaintenanceRequest" screen with key "KYCData" 
	Then I store the "CaseId" from LE360 
	Then I see "CaptureProposedChangesGrid" task is generated 
	And I navigate to "CaptureProposedChangesGrid" task 
	When I complete "CaptureProposedChanges" screen with key "LowRisk" 
	And I click on "SaveandCompleteCaptureProposedChanges" button 
	
	And I navigate to "UpdateCusotmerDetailsGrid" task 
	When I complete "UpdateCustomerDetails" screen with key "LowRisk" 
	And I click on "SaveandCompleteUpdateCustomerDetails" button 
	 When I navigate to "KYCDocumentRequirementsGrid" task 
   #Positive Test
   #Validate new documents are added as per new LE category
     Validate new in-scope document(s) are added as per new LE Category
   #Negative Test
	 Validate document(s) based on old Category are removed
     When I add a "DocumentUpload" in KYCDocument 
     Then I complete "KYCDocumentRequirements" task 