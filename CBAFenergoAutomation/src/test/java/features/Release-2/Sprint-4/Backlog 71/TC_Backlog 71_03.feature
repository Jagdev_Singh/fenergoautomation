#Test Case: TC_Backlog 71_03
#PBI: Backlog 71
#User Story ID:
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_Backlog 71_03

  @To_be_automated
  Scenario: Validate 'fenergo' logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen across all the tasks of Regular Review workflow
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "CaptureHierarchyDetails" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I complete "KYCDocumentRequirements" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "AddressAddition" in "Edit Verification" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "Documents" in "Edit Verification" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to 'CaptureFABreferennces' task
    Then I complete 'CaptureFABreferennces' task
    And I validate case status is updated as 'closed'
    # Initiate regular Review workflow
    When I navigate to 'LE360- LE details' screen
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I Click on 'Actions' button and select 'RegularReview' workflow
    # Verify Regular review case has been triggered
    Then I see 'RegularReview' Workflow has been triggered
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I navigated to 'CloseAssociatedCases' task
    Then I complete 'CloseAssociatedCases' task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    Then I complete "ValidateKYCandRegulatoryGrid" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "ReviewrequestDetails" task
    Then I complete "ReviewrequestDetails" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "Review/edit client data" task
    # Add Contacts to 'Review/edit client data' task screen
    ##Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen for all the sub-flows
    When I Click on + button displaying at the top of 'Contacts' grid
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to 'AddContacts' task screen and save the details
    Then I see Address is added under 'Contacts' grid
    When I complete "Review/editClientData" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "KYCDocumentrequirement" task
    Then I complete "KYCDocumentrequirement" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    And I complete "Fircosoft" from assessment grid with Key
    And I click on "SaveandCompleteforAssessmentScreen1" button
    When I navigate to "CompleteAMLGrid" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I Initiate "Googlescreening" by rightclicking
    And I complete "Googlescreening" from assessment grid with Key
    Then I complete "CompleteAML" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CaptureRiskCategoryGrid" task
    #Select the Risk category as "High" and complete "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessmentFAB" task with 'High' risk rating
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "RelationshipManagerReviewSignOffGrid" task
    When I complete "RelationshipManagerReviewSignOffGrid" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
    And I click on "Submit" button
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC VP"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-VPReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-VPReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC SVP"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CIBR&CKYCApprover-SVPReviewandSign-Off" task
    When I complete "CIBR&CKYCApprover-SVPReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "Group Compliance (CDD) Review and Sign-Off"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "GroupCompliance(CDD)ReviewandSign-Off" task
    When I complete "GroupCompliance(CDD)ReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "Business Unit Head (N3)"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "Business Head (N2)"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
    When I complete "BusinessUnitHeadReviewandSign-Off" task
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    Then I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    #Validate fenergo logo is replaced with 'ALPHA' Logo diplaying at the Top left corner of the screen
    When I navigate to "CaptureFABReferences" task
    When I complete "CaptureFABReferences" task
    And I Assert case status as 'Closed'
