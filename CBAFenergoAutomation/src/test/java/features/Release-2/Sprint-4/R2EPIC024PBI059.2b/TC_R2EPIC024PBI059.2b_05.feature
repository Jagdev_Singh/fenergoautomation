#Test Case: TC_R2EPIC024PBI059.2b_05
#PBI:R2EPIC024PBI059.2b
#User Story ID: BL059 - LE_CAT_01
#Designed by: Jagdev Singh
#Last Edited by: Jagdev Singh
Feature: TC_R2EPIC024PBI059.2b_05

Scenario: Validate 'Legal Entity Category' field is visible on LE360 page.

	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "VeryHighRiskCorporate" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	Then I store the "CaseId" from LE360 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I verify the populated risk rating is "Very High" 
	When I complete "RiskAssessment" task 
	
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - VP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "SVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverSVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "Group Compliance (CDD)" 
	When I search for the "CaseId" 
	When I navigate to "GroupComplianceReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BH:Corporate" 
	When I search for the "CaseId" 
	When I navigate to "BHReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "7937" 
	Then I store the "CaseId" from LE360 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	Then I navigate to the LE360 page
	and Navigate to LEDetails screen
	Validate 'LE Category' on LEDetails screen
    Then I validate 'LE Category' field is visible
    Then I validate 'LE Category' field is un-editable