#Test Case: TC_NBFI_WAC_COB_01
#PBI: FAB_Fenergo-WAC NBFI RiskAssessmentModel v1.4
#User Story ID: 
#Designed by: Priyanka/Sasmita
#Last Edited by: Vibhav Kumar
Feature: TC_NBFI_WAC_COB_01

  #Placeholder for NBFI WAC COB
  #Refer to S.No 01 from the FAB_Fenergo-WAC NBFI_datasheet
  @Automation
  Scenario: TC_NBFI_WAC_COB_01
	
	Given I login to Fenergo Application with "SuperUser" 
	When I complete "NewRequest" screen with key "TC_NBFI_WAC_COB_01" 
	
	When I complete "Product" screen with key "Green_Bonds" 
	When I complete "Product" screen with key "Guarantees&Standby_LCs" 
	And I store the "CaseId" from LE360 
	
	And I complete "CaptureNewRequest" with Key "TC_NBFI_WAC_COB_01" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	
	Given I login to Fenergo Application with "SuperUser"
	And I search for the "CaseId"
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "TC_NBFI_WAC_COB_01" 
	And I click on "SaveandCompleteforValidateKYC" button 
	
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "TC_NBFI_WAC_COB_01" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual" 
	When I complete "AssociationDetails" screen with key "DirectorIndividual" 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	And I store the "CaseId" from LE360 
	And I navigate to "Navigate to Legal Entity" screen of the added "NonIndividual" AssociatedParty 
	When I complete "LEDetailsNonIndividual" screen with key "TC_NBFI_WAC_COB_01" 
	
	When I search for the "CaseId" 
	When I navigate to "CompleteAMLGrid" task 
	And I navigate to "Navigate to Legal Entity" screen of the added "Individual" AssociatedParty 
	When I complete "LEDetailsIndividual" screen with key "TC_NBFI_WAC_COB_01" 
	When I search for the "CaseId" 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "TC_NBFI_WAC_COB_01" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	And I verify the populated risk rating is "Medium-Low"
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	