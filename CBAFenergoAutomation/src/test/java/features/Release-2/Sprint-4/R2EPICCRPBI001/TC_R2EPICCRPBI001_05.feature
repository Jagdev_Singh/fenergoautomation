  #Test Case: TC_R2EPICCRPBI001_05
  #PBI: R2EPICCRPBI001
  #User Story ID: Data_ADD_001
  #Designed by: Sasmita Pradhan
  #Last Edited by:
  @LEM
  Scenario: Corporate(in-flight case) -Validate when the user refer back to the Addresses subflow, a validation message should be displayed ,if the length of the character is more than 35 & other than specified special characters in "Maintenance Request" Stage
   #Validate the maximum length for the fields "Address line 1", "Address line 2", and "Town/ city" should be 35 characters
   #Validate only "  " " ' ( ) + , - . / : ? "special characters are allowed for the fields "Address line 1", "Address line 2", and "Town/ city"
   #validate "save" button should not be enabled, when user enters more than specified character limit and other than specified special characters for above mentioned fields
   #Validate When referring back to Maintenance Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
   #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
   #Internal Booking Details section - No change (OOTB feature)
    #Customer Details section - No change (OOTB feature)
    #Relationship section - No change (OOTB feature)
    ##################################################################################################
    #PreCondition: COB should be closed.
    #PreCondition:Triggered LEM flow with Address more than 35charactersincluding the special characters as well
    #Case ID: 7897
    ###################################################################################################
    
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
     #Initiate LEM
    And I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and Area of change as 'KYC Data and Customer Details'
    When I navigate to "CaptureProposedChanges" task
    When I complete  "CaptureProposedChanges" task
     When I navigate to "Update Customer Details" task
   
    #Update Customer Details screen
    And I select "Edit" option from the "Action" button on "Addresses" subflow
    #Verify the data entered during COB flow should be visible in LEM workflow   
    And I Validate all the updated values are retained in "Address" screen under section " Address Information"
    
    And I write "Address-12345678("Correspondance")####@@@@@Adresss234 for field "Address line 1"
    And I write "Address-12345678("Correspondance")####@@@@@Adresss234 for field "Address line 2"
   And I write "Australia-12345678("Correspondance")####@@@@@Adresss234 for field "Town/ City"
   And I click on "Save" button   
    When I complete "Update Customer Details" task
    When I naviagate to "KYC Document Requirements" task
    When I Complete "KYC Document Requirements" task
    When I naviagate to "KYC Document Requirements" task
    When I Complete "KYC Document Requirements" task
    When I naviagate to "OnboardingReview" task
    When I Complete "OnboardingReview" task
    
     #Verify When referring back to Maintenance Request stage then data entered earlier  should be retained for the fields "Address line 1", "Address line 2", and "Town/ city"
    When I select "Refer" from action button
    When I select "Maintenance Request stage" for field "RefertoStage"
    when I write "Test" for field "Referral Reason"
    And I click on "Refer" button
    Then I see "CaptureProposedChanges" task is generated
    When I navigate to "CaptureProposedChanges" task
    When I complete "CaptureProposedChanges" task
    When I navigate to "Update Customer Details" task
  
    #Update Customer Details screen
    And I select "Edit" option from the "Action" button on "Addresses" subflow
    And I Validate all the updated values are retained in "Address" screen
    #Validate "Save and Add another" & "Save" button should not be enabled,unless the data is not trimmed/ corrected
    And I verify "Save and Add another" & "Save" button is not enabled
    #Verify a validation message should be displayed ,if  the length of the character is more than 35 & other than specified special characters
    And I Valiate validation message is appearing on screen for the fields "Address line 1", "Address line 2", and "Town/ city" "
    And I validate the validation message "You've reached the maximum length. Address Line 1 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen for field "Address 1"
    And I validate the validation message "You've reached the maximum length. Address Line 2 accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen for the field "Address 2"
     And I validate the validation message "You've reached the maximum length.Town/ City accepts 35 characters and Only " " ' ( ) + , - . / : ? special characters are allowed" is appearing on screen for the field "Town/ City"
    
    
    
    