Feature: LEM - KYC Data and Customer Details - Data Attributes Changes
#Test Case: TC_R2EPIC018PBI002.3_04
#PBI: R2EPIC018PBI002.3v0.2
#User Story ID: NA
#Designed by: Sasmita Pradhan
#Last Edited by: Sasmita Pradhan
@TobeAutomated @LEM
Scenario: FI - Validate the behavior of "Capture Proposed Changes" task when area is 'LE Details' and Le Details Changes is 'KYC Data and Customer details' during LEM :
#Area for Change section - No change (OOTB feature)
#'Business Markets' subflow should be hidden
#Comments section - No change (OOTB feature) 
#Field behavior in Customer Details section (11 fields should be hidden, 8 new fields should be added, 9 fields should be modified) 
#Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 12 fields should be modified)
#Field behavior in  Regulatory Data section (1field should be modified)

#Precondition: Create COB with Client Type = FI and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task and case status should be closed
 ##Is this entity publicly listed? - No
##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-YesThen I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
When I search for the "CaseId" 
When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
When I complete "ReviewSignOff" task 
Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
When I search for the "CaseId" 
When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
When I complete "ReviewSignOff" task 
Then I login to Fenergo Application with "BUH:IBG-DNE" 
When I search for the "CaseId" 
When I navigate to "BHUReviewandSignOffGrid" task 
When I complete "ReviewSignOff" task 
Then I login to Fenergo Application with "KYCMaker: Corporate" 
When I search for the "CaseId" 
When I navigate to "CaptureFabReferencesGrid" task 
When I complete "CaptureFABReferences" task 
And I assert that the CaseStatus is "Closed" 
#LEM flow starts
And I initiate "Maintenance Request" from action button 
When I select "Product/Trading Entities" for "Dropdown" field "Area"
Then I click on "Submit" button
When I navigate to "Capture Proposed ChangesGrid" task
When I complete "Capture Proposed Changes" task
When I navigate to "LE360"
And I initiate "Maintenance Request" from action button 
Then I validate "Product/Trading Entities" dropdown is available

