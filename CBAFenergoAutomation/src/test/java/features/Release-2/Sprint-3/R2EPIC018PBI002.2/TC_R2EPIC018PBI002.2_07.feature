Feature: LEM - Product Area WF & Data Attribute Changes

  #Test Case: TC_R2EPIC018PBI002.2_07
  #PBI: R2EPIC018PBI002.2
  #User Story ID: NA
  #Designed by: Anusha PS
  #Last Edited by: Anusha PS
  @LEM
  Scenario: LiteKYC - Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities':
    #Area for Change section - No change (OOTB feature)
    #Customer Details section should be hidden
    #Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be added, 13 fields should be modified)
    #Trading Entities section should be greyed out
    #Products section - No change (OOTB feature)
    #Comments section - No change (OOTB feature)
    ##Additional Scenario: Validate if "Update Customer Details" task is removed
    #######################################################################################
    #######Precondition: Create Lite KYC with Client Type = FI, Confidential = FI and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task.
    ##Is this entity publicly listed? - No
    ##Is this parent entity publicly listed?-Yes
    ##Is this entity regulated? - Yes
    ##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-Yes
    ##Select multiple values from "Name of Stock Exchange" and "Stock Exchange Domicile Country" fields
    ##Add comments from "Enrich KYC Profile" screen
    #######################################################################################
       Given I login to Fenergo Application with "RM:FI"
    When I complete "NewRequest" screen with key "LiteKYC-FI"
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "LiteKYC-FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    
    When I complete "EnrichKYC" screen with key "LiteKYC-FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    And I assert only the following document requirements are listed for LiteKYC WF
      | KYC Document Requirement                                                                     | Default Document Type                                                                        | Default Document Category | Mandatory |
      | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Certificate of Incorporation or Trade License or Registration Certificate or Banking License | Constitutive              | True      |
      | Wolfsberg Questionnaire                                                                      | Wolfsberg Questionnaire                                                                      | MISC                      | False     |
      | Others                                                                                       | Others                                                                                       | MISC                      | False     |
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    And I click on "SaveandCompleteforAssessmentScreen1" button
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task   
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" screen with key "LiteKYC"
    
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #LEM flow starts
    And I initiate "Maintenance Request" from action button
    When I select "Products / Trading Entities" for field "Area"
    And I fill "Lem test" for field "Reason"
    And I submit the "Maintenance Request"
    And I assert that "Capture Proposed Changes" task is triggered
    And I navigate to "Capture Proposed Changes" task
    #Validate the behavior of "Capture Proposed Changes" task when area for change during LEM is 'Products/Trading Entities'
    ##Area for Change section - No change (OOTB feature)
    And I validate the following fields in "Area for Change" section
      | Label  | FieldType    | Visible | ReadOnly | DefaultsTo                |
      | Area   | Dropdown     | true    | true     | Products/Trading Entities |
      | Reason | Alphanumeric | true    | true     | Lem test                  |
    ##Customer Details section should be hidden
    And I assert "Customer Details" section is not visible
    ##Field behavior in KYC Conditions section (7 fields should be hidden, 5 new fields should be hidden, 13 fields should be modified)
    And I validate the following fields in "KYC Conditions" section #Only the following fields should be present in the mentioned order
      | Label                                                                      | FieldType             | Visible | ReadOnly | Mandatory | DefaultsTo              |
      | Legal Entity Type                                                          | Drop-down             | Yes     | true     | No        | Value from LiteKYC          |
      | Country of Incorporation / Establishment                                   | Drop-down             | Yes     | true     | No        | AE-United Arab Emirates |
      | Is this entity publicly listed?                                            | Drop-down             | Yes     | true     | Yes       | No                      |
      | Is this parent entity publicly listed?                                     | Drop-down             | Yes     | true     | Yes       | Yes                     |
      | Name of Stock Exchange                                                     | Multiselect drop-down | Yes     | true     | Yes       | Value from LiteKYC          |
      | Stock Exchange Domicile Country                                            | Multiselect drop-down | Yes     | true     | Yes       | Value from LiteKYC          |
      | Is this FAB''s Recognized Stock Exchange?                                  | Drop-down             | Yes     | true     | Yes       | Value from LiteKYC          |
      | Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?       | Drop-down             | Yes     | true     | Yes       | Yes                     |
      | Specify the prohibited Category                                            | Alphanumeric          | Yes     | true     | Yes       | Value from LiteKYC          |
      | Has the exceptional approvals been obtained to on-board/retain the client? | Drop-down             | Yes     | true     | Yes       | Value from LiteKYC          |
      | Is this entity regulated?                                                  | Drop-down             | Yes     | No       | No        | Yes                     |
      | Name of Regulatory Body                                                    | Drop-down             | Yes     | No       | Yes       | Value from LiteKYC          |
      | Regulatory ID                                                              | Alphanumeric          | Yes     | No       | No        | Value from LiteKYC          |
      | Is there an AML program in place?                                          | Drop-down             | Yes     | No       | No        | Value from LiteKYC          |
      | Is the entity a wholly-owned subsidiary of a parent?                       | Drop-down             | Yes     | true     | No        |                         |
      | Types of Shares (Bearer/Registered)                                        | Drop-down             | Yes     | true     | Yes       | Value from LiteKYC          |
      | Is the Entity operating with Flexi Desk?                                   | Drop-down             | Yes     | true     | Yes       | Value from LiteKYC          |
      | Is UAE Licensed                                                            | Auto                  | Yes     | true     | No        | True                    |
    And I validate the following fields are not visible in "KYC Conditions" section
      | Legal Entity Category                              |
      | Country of Domicile                                |
      | Parent Company: Country of Incorporation           |
      | Is the parent entity regulated?                    |
      | Parent Regulated By                                |
      | Parent''s AML Guidelines Reviewed and Approved     |
      | Name of Exchange(s) the Parent Entity is Listed On |
    ##Validate if Trading Entities section should be greyed out
    And I assert "Trading Entities" section is greyed out
    ##Validate Products section - No change (OOTB feature)
    And I assert "Product section" is popluated with values from LiteKYC
    ##Validate Comments section - No change (OOTB feature)
    And I assert "Comments" section is popluated with values from LiteKYC
    And I complete "Capture Proposed Changes" task
		##Validate if "Update Customer Details" task is not triggered
		And I assert "Update Customer Details" task is not triggered
		And I assert "KYC Document Requirements" task is trigerred
