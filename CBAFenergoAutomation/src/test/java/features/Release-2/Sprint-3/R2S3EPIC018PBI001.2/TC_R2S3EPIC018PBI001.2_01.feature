#Test Case: TC_R2S3EPIC018PBI001.2_01
#PBI: R2S3EPIC018PBI001.2
#User Story ID: US03
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2S3EPIC018PBI001.2_01

  Scenario: Validate for Account Service maker 'Proposed changes summary' Hyperlink is present in Left hand navigation Panel to navigate to  'Proposed changes summary' of LEM Workflow
    #Validate user is able to navigate to 'Proposed changes summary' screen by clicking on 'Proposed changes summary' link from LHN panel on LEM workflow.
    #Validate grid on 'Proposed changes summary' screen is auto-populated with details as displaying on OOTB for LEM workflow.
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    #	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate"
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "4027"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate Legal Entity Maintenance case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Relationships" and add "Reason" and click on 'Submit'
    #Validate Proposed Changes summary Link is present on LHN panel below Documents link
    Then I validate Proposed Changes summary Link is present on LHN panel
    #Validate user is able to navigate to 'Proposed changes summary' screen by clicking on 'Proposed changes summary' link from LHN panel
    When I navigate to Proposed Changes screen by clicking on "ProposedChangesSummary" link present on LHN panel
      #Validate grid on 'Proposed changes summary' screen is auto-populated with below labels in Grid
      | Labels            |
      | ID                |
      | Entity Type       |
      | Name              |
      | Type              |
      | Status            |
      | Last Updated Date |
      | Last Updated By   |
