#Test Case: TC_R2EPIC014PBI003_14
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_14

  @To_be_automated
  Scenario: BBG:Validate the field behaviours in Business Details section of Review/Edit Client Data Screen in RR workflow
    #Precondition: COB case with Client type as BBG and COI as UAE to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    #Validate in Business details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Customer Details" Sub Flow
      | FieldLabel                    | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
      | Nature of Activity / Business | Alphanumeric | True    | False    | True      | Auto populated from COB |
      | Annual Business Turnover      | Numeric      | True    | True     | False     | Auto populated from COB |
    #Validate the below new fields are available (new fields & Existing fields) in Business details section and values are autopopulated from COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Business Details" Sub Flow
      | FieldLabel                                               | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
      | Primary Business Activity                                | Alphanumeric | True    | True     | False     | Auto populated from COB |
      | Anticipated Transactions Turnover (Annual in AED)        | Drop-down    | True    | True     | True      | Auto populated from COB |
      | Value of Initial Deposit (AED)                           | Numeric      | True    | True     | False     | Auto populated from COB |
      | Source of Initial Deposit & Country of Source            | String       | True    | True     | False     | Auto populated from COB |
      | Value of Capital or Initial Investment in Business (AED) | Numeric      | True    | True     | False     | Auto populated from COB |
      | Projected Annual Business Turnover (AED)                 | Numeric      | True    | False    | False     | Auto populated from COB |
      | Annual Business Turnover of Group (AED)                  | Numeric      | True    | True     | False     | Auto populated from COB |
      | Active Presence in Sanctioned Countries/Territories      |              | False   |          |           |                         |
      | If Yes, Specify the Sanctioned Countries/Territories     |              | False   |          |           |                         |
      | Offshore Banking License                                 |              | False   |          |           |                         |
    #Verify the field 'Source of Initial Deposit & Country of Source' is conditional mandatory when value is entered in the field 'Value of Initial Deposit (AED)'
    And I validate the following fields in "Business Details" Sub Flow
      | FieldLabel                                    | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      | Source of Initial Deposit & Country of Source | String     | True    | True     | True      | Auto populated from COB |
    #Verify the field 'Source of Initial Deposit & Country of Source' is non-mandatory when the entered value is cleared in the field 'Value of Initial Deposit (AED)'
    And I validate the following fields in "Business Details" Sub Flow
      | FieldLabel                                    | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      | Source of Initial Deposit & Country of Source | String     | True    | True     | False     | Auto populated from COB |
    #Validate the below fields are hidden in Business details section
    And I validate the following fields in "Business Details" Sub Flow
      | Label                            | Visible |
      | Payment Countries                | false   |
      | Anticipated Transactions Profile | false   |
    ##
    #Field Validation for 'Nature of Activity / Business' field
    #Verify 'Name of Registration Body' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify Legal Entity Name field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify Legal Entity Name field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Primary Business Activity
    #Verify 'Primary Business Activity' field NOT accepts more than 256 Alphanumeric characters. Test data : 257 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Primary Business Activity' field accepts less than 256 Alphanumeric characters. Test data : 255 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Primary Business Activity' field accepts 256 Alphanumeric characters. Test data : 256 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Value of Initial Deposit (AED)' field
    #Verify 'Value of Initial Deposit (AED)' field NOT accepts more than 25 numeric values. Test data : 26 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Value of Initial Deposit (AED)' field accepts less than 24 numeric values. Test data : 24 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Value of Initial Deposit (AED)' field accepts 25 numeric values. Test data : 25 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Verify 'Value of Initial Deposit (AED)' field NOT accepts other than numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Source of Initial Deposit & Country of Source' field
    #Verify 'Source of Initial Deposit & Country of Source' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Source of Initial Deposit & Country of Source' field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Source of Initial Deposit & Country of Source' field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Value of Capital or Initial Investment in Business (AED)' field
    #Verify 'Value of Capital or Initial Investment in Business (AED)' field NOT accepts more than 25 numeric values. Test data : 26 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Value of Capital or Initial Investment in Business (AED)' field accepts less than 24 numeric values. Test data : 24 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Value of Capital or Initial Investment in Business (AED)' field accepts 25 numeric values. Test data : 25 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Verify 'Value of Capital or Initial Investment in Business (AED)' field NOT accepts other than numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Projected Annual Business Turnover (AED)' field
    #Verify 'Projected Annual Business Turnover (AED)' field NOT accepts more than 25 numeric values. Test data : 26 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Projected Annual Business Turnover (AED)' field accepts less than 24 numeric values. Test data : 24 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Projected Annual Business Turnover (AED)' field accepts 25 numeric values. Test data : 25 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Verify 'Projected Annual Business Turnover (AED)' field NOT accepts other than numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Annual Business Turnover of Group (AED)' field
    #Verify 'Annual Business Turnover of Group (AED)' field NOT accepts more than 25 numeric values. Test data : 26 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Annual Business Turnover of Group (AED)' field accepts less than 24 numeric values. Test data : 24 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Annual Business Turnover of Group (AED)' field accepts 25 numeric values. Test data : 25 numeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Verify 'Annual Business Turnover of Group (AED)' field NOT accepts other than numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #LOV validation - Anticipated Transactions Turnover (Annual in AED). (Refer PBI-LOV tab)
    And I validate the LOV of "AnticipatedTransactionsTurnover(AnnualinAED)" with key "AnticipatedTransactionsTurnover"
