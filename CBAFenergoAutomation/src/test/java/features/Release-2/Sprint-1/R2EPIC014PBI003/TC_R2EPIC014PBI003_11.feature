#Test Case: TC_R2EPIC014PBI003_11
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_11

  @To_be_automated
  Scenario: NBFI:Validate the field behaviours in 'Source Of Funds And Wealth Details' and 'Industry Codes Details' section of Review/Edit Client Data Screen in RR workflow
    #Precondition: COB case with Client type as NBFI and COI as NON UAE (ex: Andora) to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "KYCMaker_FIG"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    #Source Of Funds And Wealth Details section
    #Validate in Source Of Funds And Wealth Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      | FieldLabel                             | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      | Legal Entity Source of Income & Wealth | String     | True    | True     | False     | Auto populated from COB |
      | Legal Entity Source of Funds           | String     | True    | True     | False     | Auto populated from COB |
    ##
    #Field Validation for 'Legal Entity Source of Income & Wealth' field
    #Verify 'Legal Entity Source of Income & Wealth' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Legal Entity Source of Income & Wealth' field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Legal Entity Source of Income & Wealth' field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Legal Entity Source of Funds' field
    #Verify 'Legal Entity Source of Funds' field NOT accepts more than 1000 Alphanumeric characters. Test data : 1001 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data8"
    #Verify 'Legal Entity Source of Funds' field accepts less than 1000 Alphanumeric characters. Test data : 999 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data9"
    #Verify 'Legal Entity Source of Funds' field accepts 1000 Alphanumeric characters. Test data : 1000 Alphanumeric characters
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Industry Code details section
    #Validate in Industry Codes Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      | FieldLabel                            | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      | Primary Industry of Operation Islamic | Drop-down  | True    | False    | True      | Auto populated from COB |
      | Primary Industry of Operation         | Drop-down  | True    | True     | True      | Auto populated from COB |
      | Primary Industry of Operation UAE     | Drop-down  | True    | True     | False     | Auto populated from COB |
    ##
    #Validate the below new fields are available (new fields) in Industry Code Details section and values are autopopulated from COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Industry Code Details" Sub Flow
      | FieldLabel                      | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      | Secondary Industry of Operation | Drop-down  | True    | True     | False     | Auto populated from COB |
    #Validate the below fields are hidden in Industry Code Details section
    And I validate the following fields in "Industry Code Details" Sub Flow
      | Label               | Visible |
      | NAIC                | false   |
      | ISIN                | false   |
      | Secondary ISIC      | false   |
      | NACE 2 Code         | false   |
      | Stock Exchange Code | false   |
      | Central Index Key   | false   |
      | SWIFT BIC           | false   |
    ##
    #LOV validation - Primary Industry of Operation. (Refer PBI-LOV tab)
    And I validate the LOV of "PrimaryIndustryofOperation" with key "PrimaryIndustryofOperation"
    ##
    #LOV validation - Primary Industry of Operation UAE. (Refer PBI-LOV tab)
    And I validate the LOV of "PrimaryIndustryofOperationUAE" with key "PrimaryIndustryofOperationUAE"
    ##
    #LOV validation - Secondary Industry of Operation. (Refer PBI-LOV tab)
    And I validate the LOV of "SecondaryIndustryofOperation" with key "SecondaryIndustryofOperation"
