#Test Case: TC_R2EPIC014PBI003_12
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_12

  @To_be_automated
  Scenario: NBFI:Validate the field behaviours in 'Internal Booking Details', 'Anticipated Transactional Activity (Per Month)' section and other sections of Review/Edit Client Data Screen in RR workflow
    #Addtional objective: In LE360 screen validate all th data are retained
    #Additional objective: Refer back and check the data are retained in 'Validate KYC and Regulatory Date' and 'Review/Edit Client data screens
    #Precondition: COB case with Client type as NBFI and COI as NON UAE (Ex: Andora) to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
    Given I login to Fenergo Application with "KYCMaker_FIG"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
    #Internal Booking Details section
    #Validate in Internal Booking Details section the below fields are available (Label change) and values are autopopulated from the COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Source Of Funds And Wealth Details" Sub Flow
      | FieldLabel      | Field Type | Visible | Editable | Mandatory | Field Defaults To       |
      | Booking Country | Drop-down  | True    | True     | True      | Auto populated from COB |
      | Confidential    | Drop-down  | True    | True     | True      | Auto populated from COB |
    ##
    #Validate the below new fields are available (new fields) in Internal Booking Details section and values are autopopulated from COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Internal Booking Details" Sub Flow
      | FieldLabel                  | Field Type   | Visible | Editable | Mandatory | Field Defaults To              |
      | Target Code                 | Drop-down    | True    | True     | True      | 28-FINANCIAL INSTITUTION GROUP |
      | Sector Description          | Drop-down    | True    | True     | False     | Auto populated from COB        |
      | UID Originating Branch      | Drop-down    | True    | True     | True      | Head Office _ BNK-100          |
      | Propagate To Target Systems | Alphanumeric | True    | False    | True      | BNK                            |
    #Validate the below fields are hidden in Internal Booking Details section
    And I validate the following fields in "Internal Booking Details" Sub Flow
      | Label                                   | Visible |
      | Priority                                | false   |
      | From Office Area                        | false   |
      | On Behalf Of                            | false   |
      | Internal DeskNACE 2 Code                | false   |
      | Request Type                            | false   |
      | Request Origin                          | false   |
      | Client Reference                        | false   |
      | Consented to Data Sharing Jurisdictions | false   |
      | Jurisdiction                            | false   |
    ##
    #LOV validation - Booking Country. (Refer PBI-LOV tab)
    And I validate the LOV of "BookingCountry" with key "BookingCountry"
    ##
    #LOV validation - Confidential. (Refer PBI-LOV tab)
    And I validate the LOV of "Confidential" with key "Confidential"
    ##
    #LOV validation - Target Code. (Refer PBI-LOV tab)
    And I validate the LOV of "TargetCode" with key "TargetCode"
    ##
    #LOV validation - Sector Description. (Refer PBI-LOV tab)
    And I validate the LOV of "SectorDescription" with key "SectorDescription"
    ##
    #LOV validation - UID Originating Branch. (Refer PBI-LOV tab)
    And I validate the LOV of "UIDOriginatingBranch" with key "UIDOriginatingBranch"
    #Anticipated Transactional Activity (Per Month) section
    #Validate the below new fields are available (new fields) in Anticipated Transactional Activity (Per Month) section and values are autopopulated from COB case
    #Validate the field type, visibility, editable and mandatory and field values are defautled from COB
    And I validate the following fields in "Anticipated Transactional Activity (Per Month)" Sub Flow
      | FieldLabel         | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
      | ID                 | Alphanumeric | True    | False    | False     | Auto populated from COB |
      | Inward Amount(AED) | Numeric      | True    | True     | False     | Auto populated from COB |
      | Outward Amount     | Numeric      | True    | True     | False     | Auto populated from COB |
      | NBFI               | Drop-down    | True    | True     | False     | Auto populated from COB |
    ##
    #Field Validation for 'Inward Amount(AED)' field
    #Verify 'Inward Amount(AED)' field NOT accepts other than numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Verify 'Inward Amount(AED)' field accepts numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #Field Validation for 'Inward Amount(AED)' field
    #Verify 'Inward Amount(AED)' field NOT accepts other than numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    #Verify 'Inward Amount(AED)' field accepts numeric characters.
    And I fill the data for "ReviewEditClientData" with key "Data10"
    ##
    #LOV validation - NBFI. (Refer NBFI in PBI-LOV tab)
    And I validate the LOV of "NBFI" with key "NBFI"
    ##
    #Verify 'Business Markets' grid/section is hidden from the screen
    #Verify 'External Data' grid/section is hidden from the screen
    #Verify 'Trading Agreements' grid/section is hidden from the screen
    #Verify 'Relationships' section/grid is Mandatory
    #Verify 'Tax Identifier' section is NON-mandatory
    ##
    #Verify the below existing grids are available
    #Addresses
    #Trading Entities
    #Products
    #External References
    #Comments
    #Associated Parties
    #Contacts
    #Roles
    #Managed Accounts
    #History
    When I complete "ReviewEditClientData" screen with key "C1"
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    #Validate the data are retained in LE360 screen
    Then I navigate to LE360 screen
    #Refer back to Review Client Data Stage
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    #Verify all the data are retained in this screen
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "ReviewEditClientData" task
