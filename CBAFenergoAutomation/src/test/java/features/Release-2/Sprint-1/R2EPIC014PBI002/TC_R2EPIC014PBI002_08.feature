#Test Case: TC_R2EPIC014PBI002_08
#PBI: R2EPIC014PBI002
#User Story ID: Corp/PCG-7, FIG-7
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R2EPIC014PBI002_08

@Automation 
Scenario:
PCG-Entity:Validate if the new task "Review Request" task is generated after "Validate KYC and Regulatory Data" and Before task "Review / Edit Client Data" for RR and is assgined to "KYC maker" workflow 
#Validate the metadata of the fields/panels in "Review Request" task when logged in as KYC Maker
#Validate if the user is able to save and complete "Review Request" task without providing all mandatory data
#Validate if the KYC Maker is able to assign the "Review Request" task to RM and the case is reflected in RM queue
#Precondition: Closed COB case with Client type as Corporte with Country of Incorpoation as "UAE" and data for non mandatory subflows Tax Identifier, Trading Entities and Contacts

	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	When I complete "Contacts" task
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-UNITED ARAB EMIRATES" for "CaptureRequestDetails"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate" 
	
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	And I complete "Waiting for UID from GLCMS" task from Actions button
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	
	#Comment: Regular Review
	And I initiate "Regular Review" from action button 
	Then I store the "CaseId" from LE360 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	And I check that "AddTradingEntity" grid is disabled 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	When I add a "AnticipatedTransactionActivity" from "Review/EditClientData" 
	When I complete "EnrichKYC" screen with key "RegularReviewClientaData" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	
	#    Given I login to Fenergo Application with "KYCMaker: Corporate"
	#    When I search for the "CaseId"
	#    #Initiate Regular Review
	#    When I select "RegularReview" from Actions menu
	#    When I navigate to "CloseAssociatedCases" task
	#    And I click on "SaveandComplete" button
	#    When I navigate to "ValidateKYCandRegulatoryGrid" task
	#    When I complete "ValidateKYC" screen with key "C1"
	#    ########################
	#    #Validate if the New task "Review Request Details" task is generated after "Validate KYC and Regulatory Data" and Before task "Review / Edit Client Data" for RR and is assgined to "KYC maker" workflow
	#    And I assert that new task "Review Request Details" task is generated between "Validate KYC and Regulatory Data" and "Review / Edit Client Data" tasks
	#    And I assert that "Review Request Details" task is assigned to "KYC Maker" team
	#    When I navigate to "Review Request Details" task
	#    ########################
	#    #Validate the metadata of the fields/panels in "Review Request" task when logged in as KYC Maker
	#    And I assert that "Customer Details" is the first panel
	#    And I validate the following fields in "Customer Details" panel #in the same order. Refer PBI for mockup
	#      | FieldLabel                                                 | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
	#      | ID                                                         | Alphanumeric | True    | False    | False     | Auto populated from COB |
	#      | Client Type                                                | Drop-down    | True    | False    | False     | Auto populated from COB |
	#      | Legal Entity Name                                          | Alphanumeric | True    | False    | False     | Auto populated from COB |
	#      | Legal Entity Type                                          | Drop-down    | True    | False    | False     | Non Individual          |
	#      | Entity Type                                                | Drop-down    | True    | False    | False     | Auto populated from COB |
	#      | Legal Entity Category                                      | Drop-down    | True    | False    | False     | Auto populated from COB |
	#      | Country of Incorporation / Establishment                   | Drop-down    | True    | True     | False     | Auto populated from COB |
	#      | Country of Domicile/ Physical Presence                     | Drop-down    | True    | True     | False     | Auto populated from COB |
	#      | Date of Incorporation                                      | Date         | True    | True     | False     | Auto populated from COB |
	#      | Channel & Interface                                        | Drop-down    | True    | True     | True      | Auto populated from COB |
	#      | Purpose of Account/Relationship                            | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#      | Residential Status                                         | Drop-down    | True    | True     | True      | Resident                |
	#      | Justification for opening/maintaining non-resident account | Alphanumeric | False   | N/A      | N/A       | N/A                     |
	#      | Customer Tier                                              | Drop-down    | False   | N/A      | N/A       | N/A                     |
	#      | Relationship with bank                                     | Drop-down    | True    | True     | True      | Auto populated from COB |
	#      | CIF Creation Required?                                     | Check box    | True    | False    | False     | Yes                     |
	#      | Emirate                                                    | Drop-down    | True    | True     | True      | Auto populated from COB |
	#    And I select "IN-INDIA" for "Country of Incorporation / Establishment" field
	#    And I validate the following fields in "Customer Details" panel
	#      | FieldLabel                                                 | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
	#      | Residential Status                                         | Drop-down    | True    | True     | True      | Non-Resident      |
	#      | Justification for opening/maintaining non-resident account | Alphanumeric | True    | True     | True      |                   |
	#      | Emirate                                                    | Drop-down    | False   | N/A      | N/A       | N/A               |
	#    And I assert that "Internal Booking Details" is the first panel
	#    And I validate the following fields in "Internal Booking Details" panel #in the same order. Refer PBI for mockup
	#      | FieldLabel                              | Field Type            | Visible | Editable | Mandatory | Field Defaults To       |
	#      | Request Type                            | Drop-down             | True    | False    | False     | Regular Review          |
	#      | Request Origin                          | Date                  | True    | False    | False     | Auto populated from COB |
	#      | Client Reference                        | Alphanumeric          | True    | True     | False     | Auto populated from COB |
	#      | Booking Country                         | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Consented to Data Sharing Jurisdictions | Multiselect drop-down | True    | True     | False     | Auto populated from COB |
	#      | Confidential                            | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Jurisdiction                            | Multiselect drop-down | True    | False    | False     | Auto populated from COB |
	#      | Target Code                             | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Sector Description                      | Drop-down             | True    | True     | False     | Auto populated from COB |
	#      | UID Originating Branch                  | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Propagate To Target Systems             | Alphanumeric          | True    | False    | False     | Auto populated from COB |
	#    And I validate "Relationships" grid is mandatory
	#    And I validate the following subflows #below Internal Booking Details Panel
	#      #Validate each of the below subflow and check if the data is populated from COB
	#      | Tax Identifier   |
	#      | Trading Entities |
	#      | Products         |
	#      | Relationships    |
	#      | Addresses        |
	#      | Contacts         |
	#      | Documents        |
	#    And I validate the following buttons at the bottom of the scree
	#      | SAVE FOR LATER    |
	#      | SAVE AND COMPLETE |
	#    ########################
	#    #Validate if "Trading Entities" subflow is greyed out
	#    Then I assert that "Trading Entities" subflow is disabled
	#    ########################
	#    #Validate if the user is able to save and complete "Review Request" task without providing all mandatory data
	#    Then I do not fill data for one mandatory field
	#    #Remove data from one of the mandatory fields if all the fields have data pre-populated from COB in the above step
	#    Then I assert that "SAVE AND COMPLETE" button is disabled
	#    Then I click on "SAVE FOR LATER" button
	#    ########################
	#    #Validate if the KYC Maker is able to assign the "Review Request" task to RM and the case is reflected in RM queue
	#    Then I assign the task to RM
	#    #The above action can be performed by clicking the Actions(...) button against the task and choosing "Edit task"
	#    And I assert that "Review Request Details" task is assigned to "RM" team
	#    And I navigate to "Review Request Details" task
	#    #Validate the metadata of the fields/panels in "Review Request" task when logged in as RM
	#    And I assert that "Customer Details" is the first panel
	#    And I validate the following fields in "Customer Details" panel #in the same order. Refer PBI for mockup
	#      | FieldLabel                                                 | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
	#      | ID                                                         | Alphanumeric | True    | False    | False     | Auto populated from COB |
	#      | Client Type                                                | Drop-down    | True    | False    | False     | Auto populated from COB |
	#      | Legal Entity Name                                          | Alphanumeric | True    | False    | False     | Auto populated from COB |
	#      | Legal Entity Type                                          | Drop-down    | True    | False    | False     | Falsen Individual       |
	#      | Entity Type                                                | Drop-down    | True    | False    | False     | Auto populated from COB |
	#      | Legal Entity Category                                      | Drop-down    | True    | False    | False     | Auto populated from COB |
	#      | Country of Incorporation / Establishment                   | Drop-down    | True    | True     | False     | Auto populated from COB |
	#      | Country of Domicile/ Physical Presence                     | Drop-down    | True    | True     | False     | Auto populated from COB |
	#      | Date of Incorporation                                      | Date         | True    | True     | False     | Auto populated from COB |
	#      | Channel & Interface                                        | Drop-down    | True    | True     | True      | Auto populated from COB |
	#      | Purpose of Account/Relationship                            | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#      | Residential Status                                         | Drop-down    | True    | True     | True      | Non-Resident            |
	#      | Justification for opening/maintaining non-resident account | Alphanumeric | True    | True     | True      |                         |
	#      | Customer Tier                                              | Drop-down    | False   | N/A      | N/A       | N/A                     |
	#      | Relationship with bank                                     | Drop-down    | True    | True     | True      | Auto populated from COB |
	#      | CIF Creation Required?                                     | Check box    | True    | False    | False     | Yes                     |
	#      | Emirate                                                    | Drop-down    | False   | N/A      | N/A       | N/A                     |
	#    #Validate Date of Incorporation is not accepting future date
	#    And I assert "Date of Incorporation" does not accept future date
	#    And I select "AE-UNITED ARAB EMIRATES" for "Country of Incorporation / Establishment" field
	#    And I validate the following fields in "Customer Details" panel
	#      | FieldLabel                                                 | Field Type   | Visible | Editable | Mandatory | Field Defaults To |
	#      | Residential Status                                         | Drop-down    | True    | True     | True      | Resident          |
	#      | Emirate                                                    | Drop-down    | True    | True     | True      | Select...         |
	#      | Justification for opening/maintaining non-resident account | Alphanumeric | False   | N/A      | N/A       |                   |
	#    And I validate the following fields in "Internal Booking Details" panel #in the same order. Refer PBI for mockup
	#      | FieldLabel                              | Field Type            | Visible | Editable | Mandatory | Field Defaults To       |
	#      | Request Type                            | Drop-down             | True    | False    | False     | Regular Review          |
	#      | Request Origin                          | Date                  | True    | False    | False     | Auto populated from COB |
	#      | Client Reference                        | Alphanumeric          | True    | True     | False     | Auto populated from COB |
	#      | Booking Country                         | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Consented to Data Sharing Jurisdictions | Multiselect drop-down | True    | True     | False     | Auto populated from COB |
	#      | Confidential                            | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Jurisdiction                            | Multiselect drop-down | True    | False    | False     | Auto populated from COB |
	#      | Target Code                             | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Sector Description                      | Drop-down             | True    | True     | False     | Auto populated from COB |
	#      | UID Originating Branch                  | Drop-down             | True    | True     | True      | Auto populated from COB |
	#      | Propagate To Target Systems             | Alphanumeric          | True    | False    | False     | Auto populated from COB |
	#    And I validate "Relationships" grid is mandatory
	#    And I validate the following subflows #below Internal Booking Details Panel
	#      #Validate each of the below subflow and check if the data is populated from COB
	#      | Tax Identifier   |
	#      | Trading Entities |
	#      | Products         |
	#      | Relationships    |
	#      | Addresses        |
	#      | Contacts         |
	#      | Documents        |
	#    And I validate the following buttons at the bottom of the scree
	#      | SAVE FOR LATER    |
	#      | SAVE AND COMPLETE |
	#    When I complete "ReviewRequest" screen with key "C1"
	#    And I assert that "Validate KYC and Regulatory Data" task is triggered
