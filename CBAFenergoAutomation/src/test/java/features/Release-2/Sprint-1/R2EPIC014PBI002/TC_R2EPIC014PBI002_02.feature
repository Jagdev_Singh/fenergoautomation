#Test Case: TC_R2EPIC014PBI002_02
#PBI: R2EPIC014PBI002
#User Story ID: Corp/PCG-6, FIG-6
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Regular Review

  Scenario: PCG-Entity:#Validate if the document matrix triggered in RR is same as COB
    #Validate documents uploaded during COB are present in Regular Review
    #Validate if the user is able to upload new documents and link/unlink and download the documents uploaded during COB in the following screens
    ##KYC Document Requirements (suggested documents)
    ##RR Case Context - Document Requirements section
    #Precondition: Closed COB case with Client type as Corporte with documents uploaded
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    When I navigate to "Review Request Details" task
    When I complete "ReviewRequest" screen with key "C2"
    When I navigate to "ReviewEditClientData" task
    When I complete "ReviewEditClientData" screen with key "C3"
    When I navigate to "KYCDocumentRequirements" task
    #######################################################
    #Validate if the document matrix triggered in RR is same as COB
    Then I assert document matrix is trigered in matching with the document matrix of COB (compare the matrix between RR and COB)
    #######################################################
    #Validate if the documents uploaded during COB is availabe "KYC Document Requirements" task
    Then I assert the documents are present in "KYC Document Requirements" task (as suggested documents)
    Then I assert the metadata of the documents are matching with the metadata of documents uploaded during COB
    #Validate if the user is able to link the documents
    Then I assert that the user is able to link the document successfully
    #Validate if the user is able to download the documents
    Then I assert that the document is downloaded successfully
    #Validate if the user is able to unlink documents
    Then I assert that the document is unlinked successfully
    #Validate if the user is able to upload documents
    Then I assert that the document is uploaded successfully
    When I complete "KYCDocumentRequirements" screen with key "C3"
    Then I navigate to LE360 screen
    Then I click on "Document Requirements" section
    #######################################################
    #Validate if the document matrix triggered in RR is same as COB
    Then I assert document matrix is trigered in matching with the document matrix of COB (compare the matrix between RR and COB)
    #######################################################
    #Validate if the documents uploaded during COB is availabe in Regular Review - Case Context - Document Requirements section
    Then I assert the documents are present in "KYC Document Requirements" task (as suggested/linked documents)
    Then I assert the documents are present in "KYC Document Requirements" task (as suggested documents)
    Then I assert the metadata of the documents are matching with the metadata of documents uploaded during COB
    #Validate if the user is able to link the documents
    Then I assert that the user is able to link the document successfully
    #Validate if the user is able to download the documents
    Then I assert that the document is downloaded successfully
    #Validate if the user is able to unlink documents
    Then I assert that the document is unlinked successfully
    #Validate if the user is able to upload documents
    Then I assert that the document is uploaded successfully
    Then I click on "Document" section
    #######################################################
    #Validate if the linked documents (in the previous steps) are reflected in Regular Review - Case Context - Documents section
    Then I assert all the linked documents are appearing with status "Received"
