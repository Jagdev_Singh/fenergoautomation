#Test Case: TC_WAC_Corporate_12
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_12

  Scenario: Validate adding multiple associated parites (Individual / Non-Individual) in first layer, Individual AP with UBO relationship (Very high score) and Non-Individual AP with any relationship other than UBO (Medium score) and check system is considering the Very high risk UBO AP for risk calculation.
    #Refer to TC12 in the WAC Corp Data Sheet