#Test Case: TC_WAC_Corporate_07
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Sasmita
#Last Edited by: Sasmita
Feature: TC_WAC_Corporate_07

  Scenario: COB-Derive Overall Risk Rating as Medium-Low and all individual attributes risk rating as Medium-Low
    #Refer to TC07 in the WAC Corp Data Sheet