#Test Case: TC_R2S2EPIC003PBI201_02
#PBI: R2S2EPIC003PBI201
#User Story ID: UBO_002
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2S2EPIC003PBI201_02 

@Automation
Scenario:
Validate When RM/KYC maker does not check the check-box 'Ultimate Benefial owner(UBO)' while associating a party with any Association type 

	Given I login to Fenergo Application with "RM:FI" 
	When I complete "NewRequest" screen with key "FI" 
	And I complete "CaptureNewRequest" with Key "FI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	And I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	#Test-data: Validate 'Ultimate Benifiial owner(UBO)' check-box is available on 'Association details' task screen
	Then I validate the following fields in "Association" Sub Flow 
		|Label|FieldType|Visible|ReadOnly|Mandatory|DefaultsTo|
		|Ultimate Beneficial Owner (UBO)|CheckBox|true|false|NA|NA|
   When I complete "AssociatedPartyShareHolder24UBOUnchecked" task
   	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| No                                   |
	When I click on "SaveAssociationDetails" button 
	Then I validate that added association is displaying with a UBO tag in "Complete AML" task 
   When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociatedPartyShareHolder76Unchecked" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button
	Then I validate that added association is displaying with a UBO tag in "Complete AML" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
    Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	  When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task  
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Then I login to Fenergo Application with "FLoydKYC"
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	#Then I login to Fenergo Application with "FLoydAVP"
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	#Then I login to Fenergo Application with "BusinessUnitHead"
	Then I login to Fenergo Application with "BUH:FI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to 'CapturefabReferences' task screen 
	When I Complete 'CapturefabReferences' task 
	And I assert that the CaseStatus is "Closed" 
	#    # Initiate regular Review workflow
	And I initiate "Regular Review" from action button 
	When I navigate to "CloseAssociatedCasesGrid" task 
	Then I store the "CaseId" from LE360 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task
	And I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid 
	And I remove the existing "RAssociatedParty" from AssociatedPartiesGrid  
	When I complete "EnrichKYC" screen with key "FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	#Test-data: Validate added Association in COB workflow is displaying on Complete AML screen
	
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	#Test-data: Validate 'Ultimate Benifiial owner(UBO)' check-box is available on 'Association details' task screen
	Then I validate the following fields in "Association" Sub Flow 
		|Label|FieldType|Visible|ReadOnly|Mandatory|DefaultsTo|
		|Ultimate Beneficial Owner (UBO)|CheckBox|true|false|NA|NA|
   When I complete "AssociatedPartyShareHolder24UBOUnchecked" task
   	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| No                                   |
	When I click on "SaveAssociationDetails" button 
	Then I validate that added association is not displaying with a UBO tag in "Complete AML" task 
   When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
		When I complete "AssociatedPartyShareHolder76Unchecked" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button
	Then I validate that added association is displaying with a UBO tag in "Complete AML" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	When I Initiate "Fircosoft" by rightclicking for "2" associated party 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
    Then I complete "CompleteAML" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
		Then I complete "KYCDocumentRequirements" task 
	# Verify 'Complete ID&V' task has been triggered
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "Low" 
	Then I login to Fenergo Application with "RM:FI" 
	And I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:FI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
#	Then I login to Fenergo Application with "KYCMaker: FIG" 
#	When I search for the "CaseId" 
#	When I navigate to "CaptureFabReferencesGrid" task 
#	When I complete "CaptureFABReferences" task 
#	And I assert that the CaseStatus is "Closed" 
	
	
	
	
	
	
	
	#    When I navigate to "CaptureRiskCategoryGrid" task
	#    #Select the Risk category as "Medium" and complete "CaptureRiskCategoryGrid" task
	#    When I complete "RiskAssessmentFAB" task with 'medium' risk rating
	#    #Verify "Relationship Manager Review SignOff' task is generated
	#    Then I login to Fenergo Application with "RM:IBG-DNE"
	#    When I search for the "CaseId"
	#    When I navigate to "RelationshipManagerReviewSignOffGrid" task
	#    When I complete "RelationshipManagerReviewSignOffGrid" task
	#    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
	#    And I click on "Submit" button
	#    #Validate the case is referred to "RiskAssessmentFAB" stage
	#    Then I login to Fenergo Application with "KYCMaker: FIG"
	#    When I search for the "CaseId"
	#    When I navigate to "CaptureRiskCategoryGrid" task
	#    When I complete "CaptureRiskCategoryGrid" task with 'low' risk rating
	#    #Verify "Relationship Manager Review SignOff' task is generated
	#    Then I login to Fenergo Application with "RM:IBG-DNE"
	#    When I search for the "CaseId"
	#    When I navigate to "RelationshipManagerReviewSignOffGrid" task
	#    When I complete "RelationshipManagerReviewSignOffGrid" task
	#    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
	#    And I click on "Submit" button
	#    #Verify 'Business Unit Head Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "Business Unit Head (N3)"
	#    When I search for the "CaseId"
	#    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
	#    When I complete "BusinessUnitHeadReviewandSign-Off" task
	#    #Verify 'Capture FAB References' task is generated
	#    Then I login to Fenergo Application with "KYCMaker: FIG"
	#    When I search for the "CaseId"
	#    When I navigate to "CaptureFABReferences" task
	#    When I complete "CaptureFABReferences" task
	#    And I Assert case status as 'Closed'
