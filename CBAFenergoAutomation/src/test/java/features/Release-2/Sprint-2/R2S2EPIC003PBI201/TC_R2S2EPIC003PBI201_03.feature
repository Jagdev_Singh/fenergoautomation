#Test Case: TC_R2S2EPIC003PBI201_03
#PBI: R2S2EPIC003PBI201
#User Story ID: UBO_003
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2S2EPIC003PBI201_03 

@Automation 
Scenario:
Validate if Association Type is selected as  'Shareholder' with 'Shareholding %'  as 25% ownership then user will not be able to proceed further 
without completing screening(error message will appear on AML screen to complete screening) for both COB and RR Workflow.
	Given I login to Fenergo Application with "RM:NBFI" 
	When I complete "NewRequest" screen with key "NBFI" 
	And I complete "CaptureNewRequest" with Key "NBFI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "NBFI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "NBFI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	Then I validate the following fields in "Association" Sub Flow 
		|Label|FieldType|Visible|ReadOnly|Mandatory|DefaultsTo|
		|Ultimate Beneficial Owner (UBO)|CheckBox|true|false|NA|NA|
	When I complete "AssociatedPartyShareHolder25" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
	When I complete "AssociatedPartyShareHolder75" task  
	And I take a screenshot 
	When I click on "SaveAssociationDetails" button 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I validate that added association is displaying with a UBO tag in "Complete AML" task 
	Then I complete "CompleteAML" task 
	And I take a screenshot
    When I Initiate "Fircosoft" by rightclicking
	When I Initiate "Fircosoft" by rightclicking for "2" associated party
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "RM:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	#Then I login to Fenergo Application with "FLoydKYC"
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	#Then I login to Fenergo Application with "FLoydAVP"
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BusinessUnitHead"
	Then I login to Fenergo Application with "BUH:NBFI" 
	When I search for the "CaseId" 
	
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	#    # Initiate regular Review workflow
	And I initiate "Regular Review" from action button 
	Then I store the "CaseId" from LE360 
	When I complete "CloseAssociatedCase" task 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	And I remove the existing "RemoveAssociatedParty" from AssociatedPartiesGrid 
	And I remove the existing "RAssociatedParty" from AssociatedPartiesGrid 
	When I complete "EnrichKYC" screen with key "FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	#Test-data: Validate added Association in COB workflow is displaying on Complete AML screen
		When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	Then I validate the following fields in "Association" Sub Flow 
		|Label|FieldType|Visible|ReadOnly|Mandatory|DefaultsTo|
		|Ultimate Beneficial Owner (UBO)|CheckBox|true|false|NA|NA|
	When I complete "AssociatedPartyShareHolder25" task 
	And I validate following nature of field "Ultimate Beneficial Owner(UBO)" 
		| Is Ultimate Beneficial Owner Checked | 
		| Yes                                   |
	When I click on "SaveAssociationDetails" button 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
	When I complete "AssociatedPartyShareHolder75" task  
	And I take a screenshot 
	When I click on "SaveAssociationDetails" button 
	Then I validate that added association is displaying with a UBO tag in "Complete AML" task 
	When I Initiate "Fircosoft" by rightclicking
	When I Initiate "Fircosoft" by rightclicking for "2" associated party
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
	When I complete "CompleteAML" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	# Verify 'Complete ID&V' task has been triggered
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "Low" 
	Then I login to Fenergo Application with "RM:NBFI" 
	And I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BUH:NBFI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 

	
	
	
	
	
	
	
	
	
	
	#    When I right click on Hologram and select 'Add Association' option
	#    When I navigate to Associated parties screen
	#    When I select legal entity from existing legal entities
	#    When I navigate to 'Association details' task screen
	#    #Test-data: Validate 'Ultimate Benifiial owner(UBO)' check-box is checked and read-only for 'shareholder' on 'Association details' task screen
	#    Then I validate 'Ultimate Benifiial owner(UBO)' check-box is available and editable
	#    When I select Association Type as 'Shareholder', Type of Control as 'Significant Control' and 'Shareholding %' as '25'
	#    Then I validate 'Ultimate Benifiial owner(UBO)' check-box is checked and read-only for 'shareholder
	#    When I save the details
	#    And I see UBO tag is added on the added Association type
	#    When I complete "CaptureHierarchyDetails" task
	#    #Then I login to Fenergo Application with "Onboarding Maker"
	#    Given I login to Fenergo Application with "KYCMaker: FIG"
	#    When I search for the "CaseId"
	#    When I navigate to "KYCDocumentRequirementsGrid" task
	#    When I add a "DocumentUpload" in KYCDocument
	#    Then I complete "KYCDocumentRequirements" task
	#    When I navigate to "CompleteAMLGrid" task
	#    When I click on 'saveandcomplete' button 
	#    #Test-data: Error message should be displayed to complete screening
	#    And I see Error message appeared on 'CompleteAML' task screen to complete screening
	#    When I Initiate "Fircosoft" by rightclicking
	#    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
	#    And I click on "SaveandCompleteforAssessmentScreen1" button
	#    #Test-data:Validate task is displayed as completed after completing screening
	#    When I complete "CompleteID&V" task
	#    And I validate the task is displayed as completed
	#    When I navigate to "CaptureRiskCategoryGrid" task
	#    #Validate Risk category as 'Low'
	#    Then I Select Risk category as 'Low'
	#    And I complete "RiskAssessmentFAB" task
	#    Then I login to Fenergo Application with "RM:IBG-DNE"
	#    When I search for the "CaseId"
	#    When I navigate to "ReviewSignOffGrid" task
	#    When I complete "ReviewSignOff" task
	#    #Then I login to Fenergo Application with "FLoydKYC"
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "ReviewSignOff" task
	#    #Then I login to Fenergo Application with "FLoydAVP"
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
	#    When I complete "ReviewSignOff" task
	#    #Then I login to Fenergo Application with "BusinessUnitHead"
	#    Then I login to Fenergo Application with "BUH:IBG-DNE"
	#    When I search for the "CaseId"
	#    When I navigate to "BHUReviewandSignOffGrid" task
	#    When I complete "ReviewSignOff" task
	#    Then I login to Fenergo Application with "KYCMaker: FIG"
	#    When I navigate to 'CapturefabReferences' task screen
	#    When I Complete 'CapturefabReferences' task
	#    And I assert case has been completed and case status is updated as closed
	#    # Initiate regular Review workflow
	#    When I navigate to 'LE360- LE details' screen
	#    When I Click on 'Actions' button and select 'RegularReview' workflow
	#    # Verify Regular review case has been triggered
	#    Then I see 'RegularReview' Workflow has been triggered
	#    # Verify 'Close Associated Cases' task has been triggered
	#    And I navigated to 'CloseAssociatedCases' task
	#    Then I complete 'CloseAssociatedCases' task
	#    # Verify 'Validate KYC and Regulatory Grid' task has been triggered
	#    When I navigate to "ValidateKYCandRegulatoryGrid" task
	#    Then I complete "ValidateKYCandRegulatoryGrid" task
	#    # Verify 'Review request Details' task has been triggered
	#    When I navigate to "ReviewrequestDetails" task
	#    Then I complete "ReviewrequestDetails" task
	#    # Verify 'Review/edit client data' task has been triggered
	#    When I navigate to "Review/edit client data" task
	#    When I complete "Review/editClientData" task
	#    # Verify 'KYC Document requirement' task has been triggered
	#    When I navigate to "KYCDocumentrequirement" task
	#    Then I complete "KYCDocumentrequirement" task
	#    #Test-data: Validate added Association in COB workflow is displaying on Complete AML screen
	#    When I navigate to "CompleteAML" task workflow 
	#    Then I verify Association added in COB is displaying on Complete AML screen
	#    #Add another association on Complete AML screen
	#    When I right click on Hologram and select 'Add Association' option to add another association
	#    When I navigate to Associated parties screen
	#    When I select legal entity from existing legal entities
	#    When I navigate to 'Association details' task screen
	#    When I select Association Type as 'Shareholder', Type of Control as 'Significant Control' and 'Shareholding %' as '25
	#    When I save the detail
	#    And I Validate added association can be seen as added relationship with a 25% Badge
	#    When I complete "ComplteAML" task
	#    When I click on 'saveandcomplete' button 
	#    #Test-data: Error message should be displayed to complete screening
	#    And I see Error message appeared on 'CompleteAML' task screen to complete screening
	#    When I Initiate "Google" by rightclicking
	#    And I complete "Google" from assessment grid with Key "GoogleScreeningData"
	#    And I click on "SaveandCompleteforAssessmentScreen1" button
	#    #Test-data:Validate task is displayed as completed after completing screening
	#    When I complete "CompleteID&V" task
	#    And I validate the task is displayed as completed    
	#    #Verify 'Complete ID&V' task has been triggered
	#    When I navigate to "CompleteID&VGrid" task
	#    When I complete "CompleteID&V" task
	#    When I navigate to "CaptureRiskCategoryGrid" task
	#    #Select the Risk category as "Medium" and complete "CaptureRiskCategoryGrid" task
	#    When I complete "RiskAssessmentFAB" task with 'medium' risk rating
	#    #Verify "Relationship Manager Review SignOff' task is generated
	#    Then I login to Fenergo Application with "RM:IBG-DNE"
	#    When I search for the "CaseId"
	#    When I navigate to "RelationshipManagerReviewSignOffGrid" task
	#    When I complete "RelationshipManagerReviewSignOffGrid" task
	#    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
	#    And I click on "Submit" button
	#    #Validate the case is referred to "RiskAssessmentFAB" stage
	#    Then I login to Fenergo Application with "KYCMaker: FIG"
	#    When I search for the "CaseId"
	#    When I navigate to "CaptureRiskCategoryGrid" task
	#    When I complete "CaptureRiskCategoryGrid" task with 'low' risk rating
	#    #Verify "Relationship Manager Review SignOff' task is generated
	#    Then I login to Fenergo Application with "RM:IBG-DNE"
	#    When I search for the "CaseId"
	#    When I navigate to "RelationshipManagerReviewSignOffGrid" task
	#    When I complete "RelationshipManagerReviewSignOffGrid" task
	#    #Verify 'CIB R&C KYC Approver - KYC Manager Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    #Verify 'CIB R&C KYC Approver - AVP Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
	#    When I search for the "CaseId"
	#    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
	#    When I complete "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task with key "RefertoRiskAssessment"
	#    And I click on "Submit" button
	#    #Verify 'Business Unit Head Review and Sign-Off' task is generated
	#    Then I login to Fenergo Application with "Business Unit Head (N3)"
	#    When I search for the "CaseId"
	#    When I navigate to "BusinessUnitHeadReviewandSign-Off" task
	#    When I complete "BusinessUnitHeadReviewandSign-Off" task
	#    #Verify 'Capture FAB References' task is generated
	#    Then I login to Fenergo Application with "KYCMaker: FIG"
	#    When I search for the "CaseId"
	#    When I navigate to "CaptureFABReferences" task
	#    When I complete "CaptureFABReferences" task
	#    And I Assert case status as 'Closed'
